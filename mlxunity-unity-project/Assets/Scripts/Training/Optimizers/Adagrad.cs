﻿using SimpleJSON;
using UnityEngine;

public class Adagrad : Optimizer
{
    public float initialAccumulatorValue;
    public float epsilon;

    public Adagrad(
        float learningRate = 0.001f,
        float initialAccumulatorValue = 0.1f,
        float epsilon = 1e-07f
        ) : base(learningRate: learningRate)
    {
        this.name = "adagrad";
        this.initialAccumulatorValue = initialAccumulatorValue;
        this.epsilon = epsilon;
    }

    public override JSONNode ToJSON()
    {
        JSONNode json = base.ToJSON();

        json.Add("initial_accumulator_value", initialAccumulatorValue);
        json.Add("epsilon", epsilon);

        return json;
    }

    public override void LoadFromJSON(JSONNode configJSON)
    {
        base.LoadFromJSON(configJSON);

        initialAccumulatorValue = configJSON["initial_accumulator_value"];
        epsilon = configJSON["epsilon"];
    }

    public override bool Equals(object obj)
    {
        Adagrad other = (Adagrad)obj;
        return base.Equals(obj) &&
            float.Equals(this.initialAccumulatorValue, other.initialAccumulatorValue) &&
            float.Equals(this.epsilon, other.epsilon);
    }

    public override Optimizer Copy()
    {
        return new Adagrad(
            learningRate: learningRate,
            initialAccumulatorValue: initialAccumulatorValue,
            epsilon: epsilon
        );
    }
}
