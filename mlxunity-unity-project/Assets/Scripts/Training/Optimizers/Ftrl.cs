﻿using SimpleJSON;
using UnityEngine;

public class Ftrl : Optimizer
{
    public float learningRatePower;
    public float initialAccumulatorValue;
    public float l1RegularizationStrength;
    public float l2RegularizationStrength;
    public float l2ShrinkageRegularizationStrength;

    public Ftrl(
        float learningRate = 0.001f,
        float learningRatePower = -0.5f,
        float initialAccumulatorValue = 0.1f,
        float l1RegularizationStrength = 0,
        float l2RegularizationStrength = 0,
        float l2ShrinkageRegularizationStrength = 0
        ) : base(learningRate: learningRate)
    {
        this.name = "ftrl";
        this.learningRatePower = learningRatePower;
        this.initialAccumulatorValue = initialAccumulatorValue;
        this.l1RegularizationStrength = l1RegularizationStrength;
        this.l2RegularizationStrength = l2RegularizationStrength;
        this.l2ShrinkageRegularizationStrength = l2ShrinkageRegularizationStrength;
    }

    public override JSONNode ToJSON()
    {
        JSONNode json = base.ToJSON();

        json.Add("learning_rate_power", learningRatePower);
        json.Add("initial_accumulator_value", initialAccumulatorValue);
        json.Add("l1_regularization_strength", l1RegularizationStrength);
        json.Add("l2_regularization_strength", l2RegularizationStrength);
        json.Add("l2_shrinkage_regularization_strength", l2ShrinkageRegularizationStrength);

        return json;
    }

    public override void LoadFromJSON(JSONNode configJSON)
    {
        base.LoadFromJSON(configJSON);

        learningRatePower = configJSON["learning_rate_power"];
        initialAccumulatorValue = configJSON["initial_accumulator_value"];
        l1RegularizationStrength = configJSON["l1_regularization_strength"];
        l2RegularizationStrength = configJSON["l2_regularization_strength"];
        l2ShrinkageRegularizationStrength = configJSON["l2_shrinkage_regularization_strength"];
    }

    public override bool Equals(object obj)
    {
        Ftrl other = (Ftrl)obj;
        return base.Equals(obj) &&
            float.Equals(this.learningRatePower, other.learningRatePower) &&
            float.Equals(this.initialAccumulatorValue, other.initialAccumulatorValue) &&
            float.Equals(this.l1RegularizationStrength, other.l1RegularizationStrength) &&
            float.Equals(this.l2RegularizationStrength, other.l2RegularizationStrength) &&
            float.Equals(this.l2ShrinkageRegularizationStrength, other.l2ShrinkageRegularizationStrength);
    }

    public override Optimizer Copy()
    {
        return new Ftrl(
            learningRate: learningRate,
            learningRatePower: learningRatePower,
            initialAccumulatorValue: initialAccumulatorValue,
            l1RegularizationStrength: l1RegularizationStrength,
            l2RegularizationStrength: l2RegularizationStrength,
            l2ShrinkageRegularizationStrength: l2ShrinkageRegularizationStrength
        );
    }
}
