﻿using SimpleJSON;
using UnityEngine;

public class Adadelta : Optimizer
{
    public float rho;
    public float epsilon;

    public Adadelta(
        float learningRate = 0.001f,
        float rho = 0.95f,
        float epsilon = 1e-07f
        ) : base(learningRate: learningRate)
    {
        this.name = "adadelta";
        this.rho = rho;
        this.epsilon = epsilon;
    }

    public override JSONNode ToJSON()
    {
        JSONNode json = base.ToJSON();

        json.Add("rho", rho);
        json.Add("epsilon", epsilon);

        return json;
    }

    public override void LoadFromJSON(JSONNode configJSON)
    {
        base.LoadFromJSON(configJSON);

        rho = configJSON["rho"];
        epsilon = configJSON["epsilon"];
    }

    public override bool Equals(object obj)
    {
        Adadelta other = (Adadelta)obj;
        return base.Equals(obj) &&
            float.Equals(this.rho, other.rho) &&
            float.Equals(this.epsilon, other.epsilon);
    }

    public override Optimizer Copy()
    {
        return new Adadelta(
            learningRate: learningRate,
            rho: rho,
            epsilon: epsilon
        );
    }
}
