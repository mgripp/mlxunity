﻿using SimpleJSON;
using UnityEngine;

public class RMSprop : Optimizer
{
    public float momentum;
    public float rho;
    public float epsilon;
    public bool centered;

    public RMSprop(
        float learningRate = 0.0001f,
        float momentum = 0,
        float rho = 0.9f,
        float epsilon = 1e-07f,
        bool centered = false
        ) : base(learningRate: learningRate)
    {
        this.name = "rmsprop";
        this.momentum = momentum;
        this.rho = rho;
        this.epsilon = epsilon;
        this.centered = centered;
    }

    public override JSONNode ToJSON()
    {
        JSONNode json = base.ToJSON();

        json.Add("momentum", momentum);
        json.Add("rho", rho);
        json.Add("epsilon", epsilon);
        json.Add("centered", centered);

        return json;
    }

    public override void LoadFromJSON(JSONNode configJSON)
    {
        base.LoadFromJSON(configJSON);

        momentum = configJSON["momentum"];
        rho = configJSON["rho"];
        epsilon = configJSON["epsilon"];
        centered = configJSON["centered"];
    }

    public override bool Equals(object obj)
    {
        RMSprop other = (RMSprop)obj;
        return base.Equals(obj) &&
            float.Equals(this.momentum, other.momentum) &&
            float.Equals(this.rho, other.rho) &&
            float.Equals(this.epsilon, other.epsilon) &&
            this.centered == other.centered;
    }

    public override Optimizer Copy()
    {
        return new RMSprop(
            learningRate: learningRate,
            momentum: momentum,
            rho: rho,
            epsilon: epsilon,
            centered: centered
        );
    }
}
