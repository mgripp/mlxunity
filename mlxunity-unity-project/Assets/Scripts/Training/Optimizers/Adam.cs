﻿using SimpleJSON;
using UnityEngine;

public class Adam : Optimizer
{
    public float beta1;
    public float beta2;
    public float epsilon;
    public bool amsgrad;

    public Adam(
        float learningRate = 0.0001f,
        float beta1 = 0.9f,
        float beta2 = 0.999f,
        float epsilon = 1e-07f,
        bool amsgrad = false
        ) : base(learningRate: learningRate)
    {
        this.name = "adam";
        this.beta1 = beta1;
        this.beta2 = beta2;
        this.epsilon = epsilon;
        this.amsgrad = amsgrad;
    }

    public override JSONNode ToJSON()
    {
        JSONNode json = base.ToJSON();

        json.Add("beta_1", beta1);
        json.Add("beta_2", beta2);
        json.Add("epsilon", epsilon);
        json.Add("amsgrad", amsgrad);

        return json;
    }

    public override void LoadFromJSON(JSONNode configJSON)
    {
        base.LoadFromJSON(configJSON);

        beta1 = configJSON["beta_1"];
        beta2 = configJSON["beta_2"];
        epsilon = configJSON["epsilon"];
        amsgrad = configJSON["amsgrad"];
    }

    public override bool Equals(object obj)
    {
        Adam other = (Adam)obj;
        return base.Equals(obj) &&
            float.Equals(this.beta1, other.beta1) &&
            float.Equals(this.beta2, other.beta2) &&
            float.Equals(this.epsilon, other.epsilon) &&
            this.amsgrad == other.amsgrad;
    }

    public override Optimizer Copy()
    {
        return new Adam(
            learningRate: learningRate,
            beta1: beta1,
            beta2: beta2,
            epsilon: epsilon,
            amsgrad: amsgrad
        );
    }
}
