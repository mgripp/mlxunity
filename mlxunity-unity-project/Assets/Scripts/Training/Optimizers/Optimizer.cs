﻿using SimpleJSON;
using UnityEngine;

public abstract class Optimizer
{
    public float learningRate;
    public string name { get; protected set; }
    public string nameCapital {
        get {
            string capital = name[0].ToString().ToUpper();
            return $"{capital}{name.Substring(1, name.Length - 1)}";
        }
    }

    public Optimizer(float learningRate = 0.0001f)
    {
        this.learningRate = learningRate;
    }

    public virtual JSONNode ToJSON()
    {
        JSONNode json = new JSONObject();
        json.Add("learning_rate", learningRate);

        return json;
    }

    public virtual void LoadFromJSON(JSONNode configJSON)
    {
        learningRate = configJSON["learning_rate"];
    }

    public abstract Optimizer Copy();

    public override string ToString()
    {
        return ToJSON().ToString();
    }

    public override bool Equals(object obj)
    {
        Optimizer other = (Optimizer)obj;
        return float.Equals(this.learningRate, other.learningRate);
    }
}
