﻿using SimpleJSON;
using UnityEngine;

public class SGD : Optimizer
{
    public float momentum;
    public bool nesterov;

    public SGD(
        float learningRate = 0.0001f,
        float momentum = 1e-07f,
        bool nesterov = false
        ) : base(learningRate: learningRate)
    {
        this.name = "sgd";
        this.momentum = momentum;
        this.nesterov = nesterov;
    }

    public override JSONNode ToJSON()
    {
        JSONNode json = base.ToJSON();

        json.Add("momentum", momentum);
        json.Add("nesterov", nesterov);

        return json;
    }

    public override void LoadFromJSON(JSONNode configJSON)
    {
        base.LoadFromJSON(configJSON);

        momentum = configJSON["momentum"];
        nesterov = configJSON["nesterov"];
    }

    public override bool Equals(object obj)
    {
        SGD other = (SGD)obj;
        return base.Equals(obj) &&
            float.Equals(this.momentum, other.momentum) &&
            this.nesterov == other.nesterov;
    }

    public override Optimizer Copy()
    {
        return new SGD(
            learningRate: learningRate,
            momentum: momentum,
            nesterov: nesterov
        );
    }
}
