﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainPanel : TrainStateObservingPanel
{
    [SerializeField] TrainConfigPanel trainConfigPanel;

    void Start() => Initialize();

    public override void OnTrainingStarted() => trainConfigPanel.Deactivate();

    public override void OnTrainingFinished() {}

    public void StartTraining() => Trainer.Instance.StartTraining();

    public void ToggleTrainConfig()
    {
        if (trainConfigPanel.active)
            trainConfigPanel.Deactivate();
        else
            trainConfigPanel.Activate();
    }
}
