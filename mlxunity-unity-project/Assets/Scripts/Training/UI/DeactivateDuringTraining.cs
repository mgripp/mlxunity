﻿public class DeactivateDuringTraining : TrainStateObservingPanel
{
    void Start()
    {
        Initialize();
    }

    public override void OnTrainingStarted()
    {
        gameObject.SetActive(false);
    }

    public override void OnTrainingFinished()
    {
        gameObject.SetActive(true);
    }
}
