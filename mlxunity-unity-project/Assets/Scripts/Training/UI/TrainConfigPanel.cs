﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TrainConfigPanel : MonoBehaviour
{
    public bool active => gameObject.activeSelf;

    [SerializeField] TMP_Dropdown nnPresetDropdown;
    [SerializeField] TMP_Dropdown optimizerDropdown;
    [SerializeField] UnityEngine.UI.Slider learningRateSlider;
    [SerializeField] UnityEngine.UI.Slider epochsSlider;
    [SerializeField] UnityEngine.UI.Slider batchSizeSlider;

    void Start()
    {
        learningRateSlider.SetValueWithoutNotify(TrainConfig.Instance.selectedOptimizer.learningRate);
        epochsSlider.SetValueWithoutNotify(TrainConfig.Instance.epochs);
        batchSizeSlider.SetValueWithoutNotify(TrainConfig.Instance.batchSize);

        List<TMP_Dropdown.OptionData> presetOptions = new List<TMP_Dropdown.OptionData>();
        foreach (Network preset in TrainConfig.Instance.Presets)
        {
            TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData();
            option.text = preset.Name;
            presetOptions.Add(option);
        }
        nnPresetDropdown.AddOptions(presetOptions);
        nnPresetDropdown.value =
            TrainConfig.Instance.Presets.IndexOf(TrainConfig.Instance.ActiveNetwork.PresetNetwork);

        List<TMP_Dropdown.OptionData> optimizerOptions = new List<TMP_Dropdown.OptionData>();
        foreach (Optimizer optimizer in TrainConfig.Instance.Optimizers)
        {
            TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData();
            option.text = optimizer.nameCapital;
            optimizerOptions.Add(option);
        }
        optimizerDropdown.AddOptions(optimizerOptions);
        optimizerDropdown.value = TrainConfig.Instance.GetIndexOfOptimizer(TrainConfig.Instance.selectedOptimizer);
    }

    public void SelectPreset(int index) => TrainConfig.Instance.SelectPreset(index);
    public void SelectOptimizer(int index) => TrainConfig.Instance.SelectOptimizer(index);
    public void ChangeLearningRate(float learningRate) => TrainConfig.Instance.selectedOptimizer.learningRate = learningRate;
    public void ChangeEpochs(float epochs) => TrainConfig.Instance.epochs = (int)epochs;
    public void ChangeBatchSize(float batchSize) => TrainConfig.Instance.batchSize = (int)Mathf.Pow(2, batchSize);
    public void Activate() => gameObject.SetActive(true);
    public void Deactivate() => gameObject.SetActive(false);
}
