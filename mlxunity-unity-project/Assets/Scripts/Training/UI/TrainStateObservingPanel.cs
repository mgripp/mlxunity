﻿using UnityEngine;

public abstract class TrainStateObservingPanel : MonoBehaviour
{
    protected void Initialize()
    {
        Trainer.Instance.OnTrainingStart += OnTrainingStarted;
        Trainer.Instance.OnTrainingFinish += OnTrainingFinished;
    }

    public abstract void OnTrainingStarted();
    public abstract void OnTrainingFinished();
}
