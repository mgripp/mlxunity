﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEditor;


public class Trainer : MonoBehaviour
{
    static public Trainer Instance;

    public event Action OnTrainingStart;
    public event Action OnTrainingFinish;

    TrainConfig config;
    CommandPrompt commandPrompt = new CommandPrompt();
    string pythonScriptPath = "/../../python/train.py";
    string virtualEnvironmentPath = "/../../mlxve/";
    string protocolBufferModelPath;
    string barracudaModelPath = "/Resources/";
    string barracudaModelPathRelative = "Assets/Resources/";
    string modelName = "model";
        
    void Start()
    {
        Instance = Instance ?? this;

        config = new TrainConfig();

        pythonScriptPath = ConvertToAbsoluteWinPath(pythonScriptPath);
        virtualEnvironmentPath = ConvertToAbsoluteWinPath(virtualEnvironmentPath);
        protocolBufferModelPath = ConvertToRelativeWinPath(GeneralConfig.Instance.ProjectPath);
        barracudaModelPath = ConvertToAbsoluteWinPath(barracudaModelPath);
    }

    List<DirectoryInfo> FindProtocolBufferFiles()
    {
        List<DirectoryInfo> dirs = new List<DirectoryInfo>();
        DirectoryInfo info = new DirectoryInfo(protocolBufferModelPath);
        DirectoryInfo[] dirInfo = info.GetDirectories();
        foreach (DirectoryInfo dir in dirInfo)
            if (dir.Name.EndsWith(".pb") && dir.Name.StartsWith(modelName))
                dirs.Add(dir);

        return dirs;
    }

    List<FileInfo> FindBarracudaFiles()
    {
        List<FileInfo> files = new List<FileInfo>();
        DirectoryInfo info = new DirectoryInfo(barracudaModelPath);
        if (info.Exists)
        {
            FileInfo[] fileInfo = info.GetFiles();
            foreach (FileInfo file in fileInfo)
                if (file.Name.EndsWith(".onnx") && file.Name.StartsWith(modelName))
                    files.Add(file);
        }
        return files;
    }

    string ConvertToRelativeWinPath(string path)
    {
        path = path.Replace(@"/", @"\");
        return path;
    }

    string ConvertToAbsoluteWinPath(string path)
    {
        path = Application.dataPath + path;
        path = ConvertToRelativeWinPath(path);
        return path;
    }

    public void StartTraining()
    {
        ConfigLoadSave.Instance.SaveConfig();
        OnTrainingStart?.Invoke();
        StartCoroutine(TrainingRoutine());
    }

    IEnumerator TrainingRoutine()
    {
        // we need to wait for two frames for some reason,
        // otherwise several previous c# commands will happen after the cmd command finished
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        string configPath = ConvertToAbsoluteWinPath(GeneralConfig.Instance.ConfigPath.Replace("Assets", ""));

        // Find old model files and delete them
        // TODO: wait with removing until training of new model was successfull
        List<DirectoryInfo> pbFiles = FindProtocolBufferFiles();
        foreach (DirectoryInfo dir in pbFiles)
        {
            Directory.Delete(dir.FullName, true);
            File.Delete(dir.FullName + ".meta");
        }
        if (File.Exists(config.modelPath))
        {
            File.Delete(config.modelPath);
            File.Delete(config.modelPath + ".meta");
        }

        string modelFileName = $"{modelName}_{DateTime.Now.ToFileTimeUtc()}";
        string path = $"{barracudaModelPath}{ modelFileName}.onnx";
        // TODO: enter ve only if demanded
        string command = "";
        command += $@"{virtualEnvironmentPath}Scripts\activate.bat";
        command += $@" & python {pythonScriptPath}";
        command += $@" -c {configPath}";
        command += $@" -p {protocolBufferModelPath}{modelFileName}.pb";
        command += $@" -b {path}";

        commandPrompt.ExecuteCommand(command);

        // Import newly created barracuda model file and apply to agent
        AssetDatabase.ImportAsset($"{barracudaModelPathRelative}{modelFileName}.onnx");
        TestAgent.Instance.GiveModel(path);
        config.modelPath = path;

        OnTrainingFinish?.Invoke();
    }
}
