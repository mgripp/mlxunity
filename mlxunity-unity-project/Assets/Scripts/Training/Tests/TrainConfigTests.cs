﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using UnityEngine;

namespace Tests
{
    public class TrainConfigTests
    {
        [Test]
        public void TestCopy()
        {
            TrainConfig clone = TrainTestExamples.TrainConfigExample.Copy();

            Assert.AreEqual(TrainTestExamples.TrainConfigExample, clone);
        }

        [Test]
        public void TestEqualsTrueAfterNewlyCreated()
        {
            TrainConfig config1 = new TrainConfig();
            TrainConfig config2 = new TrainConfig();

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsTrueAfterAddingPreset()
        {
            TrainConfig config1 = TrainTestExamples.TrainConfigExample;
            TrainConfig config2 = TrainTestExamples.TrainConfigExample;

            config1.Presets.Add(TrainTestExamples.NetworkExample);
            config2.Presets.Add(TrainTestExamples.NetworkExample);

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsTrueAfterAddingOptimizer()
        {
            TrainConfig config1 = TrainTestExamples.TrainConfigExample;
            TrainConfig config2 = TrainTestExamples.TrainConfigExample;

            config1.AddOptimizer(TrainTestExamples.AdamExample);
            config2.AddOptimizer(TrainTestExamples.AdamExample);

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsTrueAfterChangingActiveNetwork()
        {
            TrainConfig config1 = TrainTestExamples.TrainConfigExample;
            TrainConfig config2 = TrainTestExamples.TrainConfigExample;

            config1.ActiveNetwork.LoadFromPreset(TrainTestExamples.NetworkExample);
            config2.ActiveNetwork.LoadFromPreset(TrainTestExamples.NetworkExample);

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsFalseAfterAddingPreset()
        {
            TrainConfig config1 = TrainTestExamples.TrainConfigExample;
            TrainConfig config2 = TrainTestExamples.TrainConfigExample;

            Network testPreset1 = TrainTestExamples.NetworkExample;

            Network testPreset2 = new Network();
            testPreset2.AddLayer(Network.LayerType.Flatten);
            testPreset2.AddLayer(Network.LayerType.Dense, 64);

            config1.Presets.Add(testPreset1);
            config2.Presets.Add(testPreset2);

            Assert.AreNotEqual(config1, config2);
        }

        [Test]
        public void TestEqualsFalseAfterAddingOptimizer()
        {
            TrainConfig config1 = TrainTestExamples.TrainConfigExample;
            TrainConfig config2 = TrainTestExamples.TrainConfigExample;

            Adam adam = new Adam();

            config1.AddOptimizer(TrainTestExamples.AdamExample);
            config2.AddOptimizer(adam);

            Assert.AreNotEqual(config1, config2);
        }

        [Test]
        public void TestEqualsTrueAfterAddingOptimizersInDifferentOrder()
        {
            TrainConfig config1 = TrainTestExamples.TrainConfigExample;
            TrainConfig config2 = TrainTestExamples.TrainConfigExample;

            config1.AddOptimizer(TrainTestExamples.AdamExample);
            config1.AddOptimizer(TrainTestExamples.AdadeltaExample);

            config2.AddOptimizer(TrainTestExamples.AdadeltaExample);
            config2.AddOptimizer(TrainTestExamples.AdamExample);

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsFalseAfterAddingOptimizersInDifferentOrder()
        {
            TrainConfig config1 = TrainTestExamples.TrainConfigExample;
            TrainConfig config2 = TrainTestExamples.TrainConfigExample;

            config1.AddOptimizer(TrainTestExamples.AdamExample);
            config1.AddOptimizer(TrainTestExamples.AdadeltaExample);

            Adadelta adadelta = TrainTestExamples.AdadeltaExample;
            adadelta.epsilon *= 2f;

            config2.AddOptimizer(adadelta);
            config2.AddOptimizer(TrainTestExamples.AdamExample);

            Assert.AreNotEqual(config1, config2);
        }

        [Test]
        public void TestEqualsFalseAfterChangingActiveNetwork()
        {
            TrainConfig config1 = new TrainConfig();
            TrainConfig config2 = new TrainConfig();

            Network testPreset = new Network();
            testPreset.AddLayer(Network.LayerType.Flatten);
            testPreset.AddLayer(Network.LayerType.Dense, 64);

            config1.ActiveNetwork.LoadFromPreset(TrainTestExamples.NetworkExample);
            config2.ActiveNetwork.LoadFromPreset(testPreset);

            Assert.AreNotEqual(config1, config2);
        }

        [Test]
        public void TestLoadFromJSON()
        {
            Network preset = TrainTestExamples.NetworkExample;
            TrainConfig config = new TrainConfig();
            config.LoadFromJSON(TrainTestExamples.TrainConfigExampleJSON);
            config.ActiveNetwork.LoadFromPreset(preset);

            Assert.AreEqual(TrainTestExamples.TrainConfigExample, config);
        }

        [Test]
        public void TestToJSON() =>
            Assert.AreEqual(TrainTestExamples.TrainConfigExampleString, TrainTestExamples.TrainConfigExample.ToJSON().ToString());

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
