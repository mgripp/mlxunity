﻿using NUnit.Framework;
using SimpleJSON;

namespace Tests
{
    public class AdamTests
    {
        [Test]
        public void TestEqualsTrueAfterNewlyCreated()
        {
            Adam adam1 = new Adam();
            Adam adam2 = new Adam();

            Assert.AreEqual(adam1, adam2);
        }

        [Test]
        public void TestEqualsTrueAfterChange()
        {
            Adam adam1 = new Adam();
            Adam adam2 = new Adam();

            adam1.beta1 = 5f;
            adam2.beta1 = 5f;

            Assert.AreEqual(adam1, adam2);
        }

        [Test]
        public void TestEqualsFalseAfterChange()
        {
            Adam adam1 = new Adam();
            Adam adam2 = new Adam();

            adam2.beta1 = 5f;

            Assert.AreNotEqual(adam1, adam2);
        }

        [Test]
        public void TestCopy()
        {
            Assert.AreEqual(TrainTestExamples.AdamExample, TrainTestExamples.AdamExample.Copy());
        }

        [Test]
        public void TestToJSON()
        {
            Assert.AreEqual(TrainTestExamples.AdamExampleString, TrainTestExamples.AdamExample.ToJSON().ToString());
        }

        [Test]
        public void TestLoadFromJSON()
        {
            Adam adam = new Adam();
            adam.LoadFromJSON(TrainTestExamples.AdamExampleJSON);

            Assert.AreEqual(TrainTestExamples.AdamExample, adam);
        }
    }
}
