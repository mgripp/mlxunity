﻿using SimpleJSON;

public class TrainTestExamples
{
    #region Adam
    public static Adam AdamExample
    {
        get
        {
            return new Adam(
                learningRate: 1f,
                beta1: 2f,
                beta2: 3f,
                epsilon: 4f,
                amsgrad: false
            );
        }
    }
    public static JSONObject AdamExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("beta_1", 2f);
            json.Add("beta_2", 3f);
            json.Add("epsilon", 4f);
            json.Add("amsgrad", false);
            return json;
        }
    }
    public static string AdamExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"beta_1\":2," +
        "\"beta_2\":3," +
        "\"epsilon\":4," +
        "\"amsgrad\":false" +
        "}";
    #endregion

    #region Nadam
    public static Nadam NadamExample
    {
        get
        {
            return new Nadam(
                learningRate: 1f,
                beta1: 2f,
                beta2: 3f,
                epsilon: 4f
            );
        }
    }
    public static JSONObject NadamExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("beta_1", 2f);
            json.Add("beta_2", 3f);
            json.Add("epsilon", 4f);
            return json;
        }
    }
    public static string NadamExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"beta_1\":2," +
        "\"beta_2\":3," +
        "\"epsilon\":4" +
        "}";
    #endregion

    #region Adadelta
    public static Adadelta AdadeltaExample
    {
        get
        {
            return new Adadelta(
                learningRate: 1f,
                rho: 2f,
                epsilon: 4f
            );
        }
    }
    public static JSONObject AdadeltaExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("rho", 2f);
            json.Add("epsilon", 4f);
            return json;
        }
    }
    public static string AdadeltaExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"rho\":2," +
        "\"epsilon\":4" +
        "}";
    #endregion

    #region Adagrad
    public static Adagrad AdagradExample
    {
        get
        {
            return new Adagrad(
                learningRate: 1f,
                initialAccumulatorValue: 2f,
                epsilon: 4f
            );
        }
    }
    public static JSONObject AdagradExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("initial_accumulator_value", 2f);
            json.Add("epsilon", 4f);
            return json;
        }
    }
    public static string AdagradExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"initial_accumulator_value\":2," +
        "\"epsilon\":4" +
        "}";
    #endregion

    #region Adamax
    public static Adamax AdamaxExample
    {
        get
        {
            return new Adamax(
                learningRate: 1f,
                beta1: 2f,
                beta2: 3f,
                epsilon: 4f
            );
        }
    }
    public static JSONObject AdamaxExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("beta_1", 2f);
            json.Add("beta_2", 3f);
            json.Add("epsilon", 4f);
            return json;
        }
    }
    public static string AdamaxExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"beta_1\":2," +
        "\"beta_2\":3," +
        "\"epsilon\":4" +
        "}";
    #endregion

    #region Ftrl
    public static Ftrl FtrlExample
    {
        get
        {
            return new Ftrl(
                learningRate: 1f,
                learningRatePower: 2f,
                initialAccumulatorValue: 3f,
                l1RegularizationStrength: 4f,
                l2RegularizationStrength: 5f,
                l2ShrinkageRegularizationStrength: 6f
            );
        }
    }
    public static JSONObject FtrlExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("learning_rate_power", 2f);
            json.Add("initial_accumulator_value", 3f);
            json.Add("l1_regularization_strength", 4f);
            json.Add("l2_regularization_strength", 5f);
            json.Add("l2_shrinkage_regularization_strength", 6f);
            return json;
        }
    }
    public static string FtrlExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"learning_rate_power\":2," +
        "\"initial_accumulator_value\":3," +
        "\"l1_regularization_strength\":4," +
        "\"l2_regularization_strength\":5," +
        "\"l2_shrinkage_regularization_strength\":6" +
        "}";
    #endregion

    #region RMSprop
    public static RMSprop RMSpropExample
    {
        get
        {
            return new RMSprop(
                learningRate: 1f,
                momentum: 1f,
                rho: 2f,
                centered: true,
                epsilon: 4f
            );
        }
    }
    public static JSONObject RMSpropExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("momentum", 1f);
            json.Add("rho", 2f);
            json.Add("epsilon", 4f);
            json.Add("centered", true);
            return json;
        }
    }
    public static string RMSpropExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"momentum\":1," +
        "\"rho\":2," +
        "\"epsilon\":4," +
        "\"centered\":true" +
        "}";
    #endregion

    #region SGD
    public static SGD SGDExample
    {
        get
        {
            return new SGD(
                learningRate: 1f,
                momentum: 2f,
                nesterov: true
            );
        }
    }
    public static JSONObject SGDExampleJSON
    {
        get
        {
            JSONObject json = new JSONObject();
            json.Add("learning_rate", 1f);
            json.Add("momentum", 2f);
            json.Add("nesterov", true);
            return json;
        }
    }
    public static string SGDExampleString =
        "{" +
        "\"learning_rate\":1," +
        "\"momentum\":2," +
        "\"nesterov\":true" +
        "}";
    #endregion

    public static Network NetworkExample
    {
        get
        {
            Network networkExample = new Network(name: "Stupid", complexity: 0);
            networkExample.AddLayer(Network.LayerType.Flatten);
            networkExample.AddLayer(Network.LayerType.Dense, 128, Network.ActivationType.Softmax);

            return networkExample;
        }
    }
    public static JSONObject NetworkExampleJSON
    {
        get
        {
            JSONObject flattenLayer = new JSONObject();
            flattenLayer.Add("type", "flatten");
            flattenLayer.Add("units", 0);
            flattenLayer.Add("activation", "relu");
            JSONObject denseLayer = new JSONObject();
            denseLayer.Add("type", "dense");
            denseLayer.Add("units", 128);
            denseLayer.Add("activation", "softmax");
            JSONArray layers = new JSONArray();
            layers.Add(flattenLayer);
            layers.Add(denseLayer);
            JSONObject networkExampleJSON = new JSONObject();
            networkExampleJSON.Add("layers", layers);
            return networkExampleJSON;
        }
    }
    public static string NetworkExampleString =
        "{" +
        "\"layers\":[" +
        "{\"type\":\"flatten\",\"units\":0,\"activation\":\"relu\"}," +
        "{\"type\":\"dense\",\"units\":128,\"activation\":\"softmax\"}" +
        "]" +
        "}";
    public static TrainConfig TrainConfigExample {
        get
        {
            TrainConfig trainConfigExample = new TrainConfig();
            trainConfigExample.ActiveNetwork.LoadFromPreset(NetworkExample);
            trainConfigExample.ActiveNetwork.PresetNetwork.PresetFileName = "00_stupid";
            trainConfigExample.batchSize = 16;
            trainConfigExample.epochs = 5;
            trainConfigExample.AddOptimizer(AdamExample);
            trainConfigExample.AddOptimizer(AdadeltaExample);
            trainConfigExample.AddOptimizer(AdagradExample);
            trainConfigExample.AddOptimizer(AdamaxExample);
            trainConfigExample.AddOptimizer(FtrlExample);
            trainConfigExample.AddOptimizer(NadamExample);
            trainConfigExample.AddOptimizer(RMSpropExample);
            trainConfigExample.AddOptimizer(SGDExample);
            trainConfigExample.SelectOptimizer(0);
            return trainConfigExample;
        }
    }
    public static JSONObject TrainConfigExampleJSON
    {
        get
        {
            JSONObject trainConfigExampleJSON = new JSONObject();
            trainConfigExampleJSON.Add("network_preset", "00_stupid");
            trainConfigExampleJSON.Add("batch_size", 16);
            trainConfigExampleJSON.Add("epochs", 5);
            trainConfigExampleJSON.Add("optimizer", "adam");
            JSONObject optimizers = new JSONObject();
            optimizers.Add("adam", AdamExample.ToJSON());
            optimizers.Add("adagrad", AdagradExample.ToJSON());
            optimizers.Add("adadelta", AdadeltaExample.ToJSON());
            optimizers.Add("adamax", AdamaxExample.ToJSON());
            optimizers.Add("ftrl", FtrlExample.ToJSON());
            optimizers.Add("nadam", NadamExample.ToJSON());
            optimizers.Add("rmsprop", RMSpropExample.ToJSON());
            optimizers.Add("sgd", SGDExample.ToJSON());
            trainConfigExampleJSON.Add("optimizers", optimizers);
            trainConfigExampleJSON.Add("network", NetworkExampleJSON);
            return trainConfigExampleJSON;
        }
    }
    public static string TrainConfigExampleString =
        "{" +
        $"\"network\":{NetworkExampleString}," +
        $"\"network_preset\":\"00_stupid\"," +
        $"\"batch_size\":16," +
        $"\"epochs\":5," +
        $"\"model_path\":\"\"," +
        $"\"optimizer\":\"adam\"," +
        $"\"optimizers\":{{" +
        $"\"adam\":{AdamExampleString}," +
        $"\"adadelta\":{AdadeltaExampleString}," +
        $"\"adagrad\":{AdagradExampleString}," +
        $"\"adamax\":{AdamaxExampleString}," +
        $"\"ftrl\":{FtrlExampleString}," +
        $"\"nadam\":{NadamExampleString}," +
        $"\"rmsprop\":{RMSpropExampleString}," +
        $"\"sgd\":{SGDExampleString}" +
        "}}";
}
