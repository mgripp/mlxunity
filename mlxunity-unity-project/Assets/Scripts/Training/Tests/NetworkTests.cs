﻿using NUnit.Framework;

namespace Tests
{
    public class NetworkTests
    {
        [Test]
        public void TestDeepCopy()
        {
            Network clone = TrainTestExamples.NetworkExample.Copy();

            Assert.AreEqual(TrainTestExamples.NetworkExample, clone);
        }

        [Test]
        public void TestEqualsForEqualNetworks()
        {
            Network network1 = new Network();
            Network network2 = new Network();

            network1.AddLayer(Network.LayerType.Flatten);
            network1.AddLayer(Network.LayerType.Dense, 128, Network.ActivationType.Softmax);
            network1.AddLayer(Network.LayerType.Dense, 32, Network.ActivationType.Relu);
            network2.AddLayer(Network.LayerType.Flatten);
            network2.AddLayer(Network.LayerType.Dense, 128, Network.ActivationType.Softmax);
            network2.AddLayer(Network.LayerType.Dense, 32);

            Assert.AreEqual(network1, network2);
        }

        [Test]
        public void TestEqualsForDifferentNetworks()
        {
            Network network1 = new Network();
            Network network2 = new Network();

            network1.AddLayer(Network.LayerType.Flatten);
            network1.AddLayer(Network.LayerType.Dense, 128, Network.ActivationType.Softmax);
            network1.AddLayer(Network.LayerType.Dense, 32, Network.ActivationType.Relu);
            network2.AddLayer(Network.LayerType.Flatten);
            network2.AddLayer(Network.LayerType.Dense, 64, Network.ActivationType.Softmax);
            network2.AddLayer(Network.LayerType.Dense, 32);

            Assert.AreNotEqual(network1, network2);
        }

        [Test]
        public void TestToJSONEmpty()
        {
            Network network = new Network();
            string json = "{\"layers\":[]}";
            Assert.AreEqual(json, network.ToJSON().ToString());
        }

        [Test]
        public void TestToJSONWithAddedLayers()
        {
            Network network = new Network();

            network.AddLayer(Network.LayerType.Flatten, 0, Network.ActivationType.Relu);
            string json = "{\"layers\":[{\"type\":\"flatten\",\"units\":0,\"activation\":\"relu\"}]}";
            Assert.AreEqual(json, network.ToJSON().ToString());

            network.AddLayer(Network.LayerType.Dense, 128, Network.ActivationType.Softmax);
            Assert.AreEqual(TrainTestExamples.NetworkExampleString, network.ToJSON().ToString());
        }

        [Test]
        public void TestToJSON()
        {
            Network network = new Network();
            string json = "{\"layers\":[]}";
            Assert.AreEqual(json, network.ToJSON().ToString());

            network.AddLayer(Network.LayerType.Flatten, 0, Network.ActivationType.Relu);
            json = "{\"layers\":[{\"type\":\"flatten\",\"units\":0,\"activation\":\"relu\"}]}";
            Assert.AreEqual(json, network.ToJSON().ToString());

            network.AddLayer(Network.LayerType.Dense, 128, Network.ActivationType.Softmax);
            Assert.AreEqual(TrainTestExamples.NetworkExampleString, network.ToJSON().ToString());
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
