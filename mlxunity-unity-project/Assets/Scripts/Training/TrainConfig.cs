﻿using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using System.Linq;
using UnityEngine;
using CollectionExtensions;

public class TrainConfig : Configurable
{
    static public TrainConfig Instance;

    public override string ConfigKey => "train";

    public List<Network> Presets { get; private set; } = new List<Network>();
    Dictionary<string, Network> presetFileNameToNetwork =
        new Dictionary<string, Network>();
    public IReadOnlyDictionary<string, Network> PresetFileNameToNetwork => presetFileNameToNetwork;
    public Network ActiveNetwork { get; private set; } = new Network();

    List<Optimizer> optimizers = new List<Optimizer>();
    public IReadOnlyList<Optimizer> Optimizers => optimizers;

    public int batchSize = 32;
    public int epochs = 1;
    public Optimizer selectedOptimizer { get; private set; }
    public string modelPath = "";

    public TrainConfig(bool loadPresets = true) : base()
    {
        Instance = Instance ?? this;

        if (!loadPresets)
            return;

        // load all NN presets
        DirectoryInfo info = new DirectoryInfo(GeneralConfig.Instance.NetworkPresetsPath);
        FileInfo[] fileInfo = info.GetFiles();

        foreach (FileInfo file in fileInfo)
            if (file.FullName.EndsWith(".json"))
                LoadPreset(file.FullName, file.Name);
    }

    public void SelectOptimizer(int index) => selectedOptimizer = optimizers[index];

    void LoadPreset(string path, string fileName)
    {
        JSONNode jsonData;

        // get preset json content
        string jsonString = File.ReadAllText(path);
        jsonData = JSON.Parse(jsonString);

        // name is the filename minus '.json' ending
        string presetFilename = fileName.Substring(0, fileName.Length - 5);

        Network newPreset = new Network(jsonData["name"], jsonData["complexity"], presetFilename);

        newPreset.AddLayersFromJSONArray((JSONArray)jsonData["layers"]);

        AddPreset(newPreset, presetFilename);
    }

    void AddPreset(Network preset, string filename)
    {
        Presets.Add(preset);
        presetFileNameToNetwork[filename] = preset;
    }

    public void SelectPreset(int index) => ActiveNetwork.LoadFromPreset(Presets[index]);

    public int GetIndexOfOptimizer(Optimizer optimizer)
    {
        int index = -1;
        foreach (Optimizer other in optimizers)
        {
            index++;
            if (other == optimizer)
                break;
        }
        return index;
    }

    public TrainConfig Copy()
    {
        TrainConfig clone = new TrainConfig(false);

        clone.ActiveNetwork = ActiveNetwork.Copy();
        clone.batchSize = batchSize;
        clone.epochs = epochs;
        clone.modelPath = modelPath;

        foreach (string filename in PresetFileNameToNetwork.Keys)
            clone.AddPreset(PresetFileNameToNetwork[filename], filename);

        foreach (Optimizer optimizer in optimizers)
            clone.AddOptimizer(optimizer);
        clone.SelectOptimizer(GetIndexOfOptimizer(selectedOptimizer));

        return clone;
    }

    public override void LoadFromJSON(JSONNode configJSON)
    {
        string presetName = configJSON["network_preset"];
        // if preset name ist empty: dont use preset, load network from config instead
        if (PresetFileNameToNetwork.ContainsKey(presetName))
            ActiveNetwork = new Network(PresetFileNameToNetwork[presetName]);
        else
            ActiveNetwork = new Network(configJSON["network"]);

        batchSize = configJSON["batch_size"];
        epochs = configJSON["epochs"];

        modelPath = configJSON["model_path"];
        optimizers = new List<Optimizer>();
        optimizers.Add(new Adam());
        optimizers.Add(new Adamax());
        optimizers.Add(new Nadam());
        optimizers.Add(new Adadelta());
        optimizers.Add(new Adagrad());
        optimizers.Add(new RMSprop());
        optimizers.Add(new Ftrl());
        optimizers.Add(new SGD());
        
        foreach (Optimizer optimizer in optimizers)
        {
            optimizer.LoadFromJSON(configJSON["optimizers"][optimizer.name]);
            if (optimizer.name == configJSON["optimizer"])
                selectedOptimizer = optimizer;
        }

        if (File.Exists(modelPath))
            TestAgent.Instance.GiveModel(modelPath);
    }

    public void AddOptimizer(Optimizer newOptimizer) => optimizers.Add(newOptimizer);

    public override JSONNode ToJSON()
    {
        JSONObject configJSON = new JSONObject();

        configJSON.Add("network", ActiveNetwork.ToJSON());

        string presetName = "None";
        if (ActiveNetwork.PresetNetwork != null)
            presetName = ActiveNetwork.PresetNetwork.PresetFileName;
        configJSON.Add("network_preset", presetName);

        configJSON.Add("batch_size", batchSize);
        configJSON.Add("epochs", epochs);
        configJSON.Add("model_path", modelPath);
        configJSON.Add("optimizer", selectedOptimizer.name);

        JSONObject optimizersJSON = new JSONObject();
        foreach (Optimizer optimizer in optimizers)
            optimizersJSON.Add(optimizer.name, optimizer.ToJSON());
        configJSON.Add("optimizers", optimizersJSON);

        return configJSON;
    }

    public override bool Equals(object obj)
    {
        TrainConfig other = (TrainConfig)obj;

        return (this.ActiveNetwork == null && other.ActiveNetwork == null ||
            !(this.ActiveNetwork == null ^ other.ActiveNetwork == null) &&
            this.ActiveNetwork.Equals(other.ActiveNetwork)) &&
            this.batchSize == other.batchSize &&
            this.epochs == other.epochs &&
            (this.selectedOptimizer == null && other.selectedOptimizer == null ||
            !(this.selectedOptimizer == null ^ other.selectedOptimizer == null) &&
            this.selectedOptimizer.Equals(other.selectedOptimizer)) &&
            this.Presets.SequenceEqual(other.Presets) &&
            this.PresetFileNameToNetwork.SequenceEqual(other.PresetFileNameToNetwork) &&
            this.optimizers.ContentEquals(other.optimizers);
    }
}
