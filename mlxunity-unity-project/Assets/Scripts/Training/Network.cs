﻿using System;
using System.Collections.Generic;
using SimpleJSON;
using System.Linq;
using UnityEngine;

public class Network
{
    public enum LayerType
    {
        Dense,
        Flatten,
    }

    public enum ActivationType
    {
        Relu,
        Softmax,
    }

    class Layer
    {
        public LayerType Type { get; private set; }
        public int Units { get; private set; }
        public ActivationType Activation { get; private set; }

        public Layer(LayerType type, int units = 0, ActivationType activation = ActivationType.Relu)
        {
            Type = type;
            Units = units;
            Activation = activation;
        }

        public JSONObject ToJSON()
        {
            JSONObject layerJSON = new JSONObject();
            layerJSON.Add("type", Type.ToString().ToLower());
            layerJSON.Add("units", Units);
            layerJSON.Add("activation", Activation.ToString().ToLower());

            return layerJSON;
        }

        public override bool Equals(object obj)
        {
            Layer other = (Layer)obj;

            return this.Type == other.Type &&
                this.Units == other.Units &&
                this.Activation == other.Activation;
        }
    }

    List<Layer> layers = new List<Layer>();
    public string Name { get; private set; } = "Unnamed";
    public string PresetFileName = "None";
    public Network PresetNetwork { get; private set; }
    public int Complexity { get; private set; } = 0;

    public Network() { }

    public Network(string name, int complexity, string presetFilename = "None") {
        Name = name;
        Complexity = complexity;
        PresetFileName = presetFilename;
    }

    public Network(Network presetNetwork)
    {
        LoadFromPreset(presetNetwork);
    }

    public void LoadFromPreset(Network presetNetwork)
    {
        ClearLayers();
        PresetNetwork = presetNetwork;
        // copy preset network into this network
        Complexity = PresetNetwork.Complexity;

        foreach (Layer layer in PresetNetwork.layers)
        {
            AddLayer(layer.Type, layer.Units, layer.Activation);
        }
    }

    public Network(JSONNode networkConfig)
    {
        AddLayersFromJSONArray((JSONArray)networkConfig["layers"]);
    }

    public void DetachFromPreset()
    {
        PresetNetwork = null;
    }

    public void AddLayersFromJSONArray(JSONArray jsonArray)
    {
        ClearLayers();
        foreach (JSONNode layer in jsonArray)
        {
            AddLayer(layer["type"], layer["units"], layer["activation"]);
        }
    }

    public void AddLayer(
        LayerType type,
        int units = 0,
        ActivationType activation = ActivationType.Relu
    )
    {
        layers.Add(new Layer(type, units, activation));
    }

    string FirstCharUpper(string s)
    {
        return s.First().ToString().ToUpper() + s.Substring(1);
    }

    public void AddLayer(
        string type,
        int units = 0,
        string activation = "relu"
    )
    {
        type = FirstCharUpper(type);
        activation = activation ?? "relu";
        activation = FirstCharUpper(activation);
        AddLayer(
            (LayerType)Enum.Parse(typeof(LayerType), type),
            units,
            (ActivationType)Enum.Parse(typeof(ActivationType), activation)
        );
    }

    public void ClearLayers()
    {
        layers = new List<Layer>();
    }

    public JSONObject ToJSON()
    {
        JSONObject jsonData = new JSONObject();
        JSONArray jsonLayers = new JSONArray();

        foreach (Layer layer in layers)
        {
            jsonLayers.Add(layer.ToJSON());
        }

        jsonData["layers"] = jsonLayers;

        return jsonData;
    }

    public Network Copy()
    {
        Network clone = new Network(Name, Complexity);
        clone.PresetNetwork = PresetNetwork;

        foreach (Layer layer in layers)
            clone.AddLayer(layer.Type, layer.Units, layer.Activation);

        return clone;
    }

    public override string ToString()
    {
        return ToJSON().ToString();
    }

    public override bool Equals(object obj)
    {
        Network other = (Network)obj;

        return this.Name == other.Name &&
            this.Complexity == other.Complexity &&
            (this.PresetNetwork == null && other.PresetNetwork == null ||
            !(this.PresetNetwork == null ^ other.PresetNetwork == null) &&
            this.PresetNetwork.Equals(other.PresetNetwork)) &&
            this.layers.SequenceEqual(other.layers);
    }
}
