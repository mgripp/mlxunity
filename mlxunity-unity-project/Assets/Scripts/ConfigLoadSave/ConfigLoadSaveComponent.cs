﻿using UnityEngine;

public class ConfigLoadSaveComponent : MonoBehaviour
{
    ConfigLoadSave loadSave;

    private void Start()
    {
        loadSave = new ConfigLoadSave();
        loadSave.LoadConfig();
    }

    private void OnApplicationQuit() => loadSave.SaveConfig();
}
