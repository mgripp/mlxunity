﻿using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    public class ConfigLoadSaveTests
    {
        // TODO: Test with own test dedault config used only for testing (TestExample?)
        [Test]
        public void TestSaveAndLoadDefaultConfig()
        {
            // at this point, the config has been loaded, which msut be the default config
            // test if config stays the same after saving and loading just saved config
            ConfigLoadSave.Instance.SaveConfig();

            GeneralConfig generalConfig = GeneralConfig.Instance.Copy();
            TrainConfig TrainConfig = TrainConfig.Instance.Copy();
            RecordConfig recordConfig = RecordConfig.Instance.Copy();
            ClassesConfig classesConfig = ClassesConfig.Instance.Copy();

            ConfigLoadSave.Instance.LoadConfig();

            Assert.AreEqual(generalConfig, GeneralConfig.Instance);
            Assert.AreEqual(TrainConfig, TrainConfig.Instance);
            Assert.AreEqual(recordConfig, RecordConfig.Instance);
            Assert.AreEqual(classesConfig, ClassesConfig.Instance);
        }

        [Test]
        public void TestOverrideDefaultConfig()
        {
            TrainConfig.Instance.ActiveNetwork.DetachFromPreset();
            TrainConfig.Instance.ActiveNetwork.ClearLayers();
            TrainConfig.Instance.ActiveNetwork.AddLayer("flatten");
            TrainConfig.Instance.ActiveNetwork.AddLayer("Dense", 128);
            TrainConfig.Instance.ActiveNetwork.AddLayer("Dense", 7, "Softmax");

            RecordConfig.Instance.MinMovementDistance = 9f;
            RecordConfig.Instance.MaxTrajectoryVectorMagnitude = 22.5f;
            RecordConfig.Instance.ClassIndex = 4;
            RecordConfig.Instance.TrajectoryCountMin = 31;
            RecordConfig.Instance.TrajectoryCountMax = 68;
            RecordConfig.Instance.NormalizeVectors = false;
            RecordConfig.Instance.ScaleTrajectories = true;

            //TODO: better test when add class is implemented
            ClassesConfig.Instance.ClearClasses();

            GeneralConfig generalConfig = GeneralConfig.Instance.Copy();
            TrainConfig trainConfig = TrainConfig.Instance.Copy();
            RecordConfig recordConfig = RecordConfig.Instance.Copy();
            ClassesConfig classesConfig = ClassesConfig.Instance.Copy();

            ConfigLoadSave.Instance.SaveConfig();

            TrainConfig.Instance.ActiveNetwork.DetachFromPreset();
            TrainConfig.Instance.ActiveNetwork.ClearLayers();
            TrainConfig.Instance.ActiveNetwork.AddLayer("flatten");
            TrainConfig.Instance.ActiveNetwork.AddLayer("Dense", 64);
            TrainConfig.Instance.ActiveNetwork.AddLayer("Dense", 128);
            TrainConfig.Instance.ActiveNetwork.AddLayer("Dense", 4, "Softmax");

            RecordConfig.Instance.MinMovementDistance = 8f;
            RecordConfig.Instance.MaxTrajectoryVectorMagnitude = 12.5f;
            RecordConfig.Instance.ClassIndex = 1;
            RecordConfig.Instance.TrajectoryCountMin = 30;
            RecordConfig.Instance.TrajectoryCountMax = 60;
            RecordConfig.Instance.NormalizeVectors = true;
            RecordConfig.Instance.ScaleTrajectories = false;

            ConfigLoadSave.Instance.LoadConfig();

            Assert.AreEqual(generalConfig, GeneralConfig.Instance);
            Assert.AreEqual(trainConfig, TrainConfig.Instance);
            Assert.AreEqual(recordConfig, RecordConfig.Instance);
            Assert.AreEqual(classesConfig, ClassesConfig.Instance);
        }

        [Test]
        public void TestSaveSelectedPreset()
        {
            TrainConfig.Instance.SelectPreset(3);

            ConfigLoadSave.Instance.SaveConfig();
            ConfigLoadSave.Instance.LoadConfig();

            Assert.AreEqual("04_", TrainConfig.Instance.ActiveNetwork.PresetNetwork.PresetFileName.Substring(0, 3));
        }

        [Test]
        public void TestSaveNoPresetSelected()
        {
            TrainConfig.Instance.ActiveNetwork.DetachFromPreset();

            ConfigLoadSave.Instance.SaveConfig();
            ConfigLoadSave.Instance.LoadConfig();

            Assert.IsNull(TrainConfig.Instance.ActiveNetwork.PresetNetwork);
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
