﻿using UnityEngine;
using System;
using System.IO;
using SimpleJSON;
using System.Collections.Generic;

public class ConfigLoadSave
{
    static public ConfigLoadSave Instance;

    public ConfigLoadSave()
    {
        Instance = Instance ?? this;
    }

    public void SaveConfig()
    {
        JSONObject configJSON = ConfigToJSON();
        string jsonString = configJSON.ToString();

        Debug.Log(string.Format("Save Config to JSON: {0}", jsonString));

        try
        {
            if (!File.Exists(GeneralConfig.Instance.ConfigPath))
                Directory.CreateDirectory(Path.GetDirectoryName(GeneralConfig.Instance.ConfigPath));

            File.WriteAllText(GeneralConfig.Instance.ConfigPath, jsonString);
        }

        catch (System.Exception e)
        {
            Debug.LogWarning(string.Format(
                "Could not save config under {0}: {1}",
                GeneralConfig.Instance.ConfigPath,
                e
            ));
        }
    }

    public void LoadConfig()
    {
        JSONNode jsonData;

        if (!File.Exists(GeneralConfig.Instance.ConfigPath))
        {
            // no config found: load default config
            if (File.Exists(GeneralConfig.Instance.DefaultConfigPath))
            {
                string jsonString = File.ReadAllText(GeneralConfig.Instance.DefaultConfigPath);
                jsonData = JSON.Parse(jsonString);
                Debug.Log($"No config found under '{GeneralConfig.Instance.ConfigPath}', " +
                    $"loading from '{GeneralConfig.Instance.DefaultConfigPath}' instead.");
            } else
            {
                throw new FileNotFoundException(
                    $"Could not load default config, file does not exist: {GeneralConfig.Instance.DefaultConfigPath}"
                );
            }
        }
        else
        {
            Debug.Log($"Loading from '{GeneralConfig.Instance.ConfigPath}'.");
            string jsonString = File.ReadAllText(GeneralConfig.Instance.ConfigPath);
            jsonData = JSON.Parse(jsonString);
        }

        foreach (Configurable config in Configurable.Instances)
            config.LoadFromJSON(jsonData[config.ConfigKey]);
    }

    public JSONObject ConfigToJSON()
    {
        JSONObject configJSON = new JSONObject();
        
        foreach (Configurable config in Configurable.Instances)
            configJSON.Add(config.ConfigKey, config.ToJSON());

        return configJSON;
    }
}
