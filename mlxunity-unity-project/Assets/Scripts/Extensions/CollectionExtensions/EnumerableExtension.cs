﻿using UnityEngine;
using System.Collections.Generic;
using VectorExtensions;
using PrimitiveExtensions;

namespace CollectionExtensions
{
    public static class ListExtension
    {
        // TODO: Replacable by Enumerable.All()? https://stackoverflow.com/questions/22173762/check-if-two-lists-are-equal
        // https://stackoverflow.com/questions/3669970/compare-two-listt-objects-for-equality-ignoring-order
        public static bool ContentEquals<T>(this List<T> list1, List<T> list2)
        {
            if (list1.Count != list2.Count)
                return false;

            foreach (T element1 in list1)
                foreach (T element2 in list2)
                    if (element1.GetType() == element2.GetType() && !element1.Equals(element2))
                        return false;
            return true;
        }

        public static bool SequenceEqualApproximate<T>(this List<Vector2> first, List<Vector2> second, float delta = 1E-05f)
        {
            if (first.Count != second.Count)
                return false;

            for (int i = 0; i < first.Count; i++)
            {
                Vector2 element1 = first[i];
                Vector2 element2 = second[i];
                if (!element1.EqualsApproximate(element2, delta))
                    return false;
            }

            return true;
        }

        public static bool SequenceEqualApproximate<T>(this List<Vector3> first, List<Vector3> second, float delta = 1E-05f)
        {
            if (first.Count != second.Count)
                return false;

            for (int i = 0; i < first.Count; i++)
            {
                Vector3 element1 = first[i];
                Vector3 element2 = second[i];
                if (!element1.EqualsApproximate(element2, delta))
                    return false;
            }

            return true;
        }

        public static bool SequenceEqualApproximate<T>(this List<float> first, List<float> second, float delta = 1E-05f)
        {
            if (first.Count != second.Count)
                return false;

            for (int i = 0; i < first.Count; i++)
            {
                float element1 = first[i];
                float element2 = second[i];
                if (!element1.EqualsApproximate(element2))
                    return false;
            }

            return true;
        }
    }
}
