﻿using UnityEngine;
using SimpleJSON;

namespace SimpleJSONExtenstions
{
    public static class JSONNodeExtension
    {
        public static bool EqualsApproximate(this JSONNode n1, JSONNode n2) => n1.Equals(n2);
    }

    public static class JSONNumberExtension
    {
        public static bool EqualsApproximate(this JSONNumber n1, JSONNumber n2) => Mathf.Approximately(n1, n2);
    }

    public static class JSONArrayExtension
    {
        public static bool EqualsApproximate(this JSONArray a1, JSONArray a2)
        {
            if (a1.Count != a2.Count)
                return false;

            for (int i = 0; i < a1.Count; i++)
            {
                try
                {
                    if (!EqualsApproximate((JSONArray)a1[i], (JSONArray)a2[i]))
                        return false;
                }
                catch
                {
                    if (!JSONNumberExtension.EqualsApproximate((JSONNumber)a1[i], (JSONNumber)a2[i]))
                        return false;
                }
                
            }
            return true;
        }
    }
}
