﻿using UnityEngine;
using System.Collections;

namespace DebugExtensions
{
    public static class DebugExtension
    {
        public static void Log(IEnumerable message)
        {
            try
            {
                Debug.Log($"{message} with length: {((IList)message).Count}");
            }
            catch
            {
                Debug.Log(message);
            }
            foreach (object item in message)
            {
                try
                {
                    Log((IEnumerable)item);
                }
                catch
                {
                    Debug.Log($"\t{item}");
                }
            }
        }
    }
}
