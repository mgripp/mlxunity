﻿using UnityEngine;
using System.Collections.Generic;

namespace VectorExtensions
{
    public static class Vector2Extension
    {
        public static bool EqualsApproximate(this Vector2 v1, Vector2 v2, float delta = 0.00001f) => (v1 - v2).magnitude < delta;
    }
}
