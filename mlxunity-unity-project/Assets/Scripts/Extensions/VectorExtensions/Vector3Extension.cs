﻿using UnityEngine;
     
namespace VectorExtensions
{
    public static class Vector3Extension
    {
        public static bool EqualsApproximate(this Vector3 v1, Vector3 v2, float delta = 0.00001f) => (v1 - v2).magnitude < delta;
    }
}
