﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrimitiveExtensions
{
    public static class FloatExtension
    {
        public static bool EqualsApproximate(this float f1, float f2, float delta = 0.00001f) => Mathf.Abs(f1 - f2) < delta;
    }
}
