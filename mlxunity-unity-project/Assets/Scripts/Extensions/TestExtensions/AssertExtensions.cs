﻿using UnityEngine;
using VectorExtensions;
using SimpleJSONExtenstions;
using SimpleJSON;
using NUnit.Framework;
using System.Collections.Generic;
using CollectionExtensions;

namespace TestExtensions
{
    public static class AssertExtensions
    {
        public static void AreEqual(float[] expected, float[] actual, float delta = 1E-05f)
            => AreEqual(new List<float>(expected), new List<float>(actual), delta);

        public static void AreEqual(List<float> expected, List<float> actual, float delta = 1E-05f)
        {
            if (!expected.SequenceEqualApproximate<float>(actual, delta))
            {
                string message = $"---\nExpected: {expected}\nBut was: {actual}\n---";
                Assert.Fail(message);
            }
        }

        public static void AreEqual(List<Vector2> expected, List<Vector2> actual, float delta = 1E-05f)
        {
            if (!expected.SequenceEqualApproximate<Vector2>(actual, delta))
            {
                string message = $"---\nExpected: {expected}\nBut was: {actual}\n---";
                Assert.Fail(message);
            }
        }

        public static void AreEqual(Vector2 expected, Vector2 actual, float delta = 1E-05f)
        {
            if (!expected.EqualsApproximate(actual, delta))
            {
                string message = $"---\nExpected: {expected}\nBut was: {actual}\n---";
                Assert.Fail(message);
            }
        }

        public static void AreEqual(JSONArray expected, JSONArray actual)
        {
            if (!expected.EqualsApproximate(actual))
            {
                string message = $"---\nExpected: {expected}\nBut was: {actual}\n---";
                Assert.Fail(message);
            }
        }

        public static void AreNotEqual(JSONArray expected, JSONArray actual)
        {
            if (expected.EqualsApproximate(actual))
            {
                string message = $"---\nExpected: not equal {expected}\nBut was: {actual}\n---";
                Assert.Fail(message);
            }
        }
    }
}
