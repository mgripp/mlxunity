﻿using UnityEngine;
using SimpleJSON;
using System.Collections.Generic;

public abstract class Configurable
{
    public static IList<Configurable> Instances => instances.AsReadOnly();

    static List<Configurable> instances = new List<Configurable>();

    public abstract string ConfigKey { get; }

    public Configurable() => instances.Add(this);

    public abstract void LoadFromJSON(JSONNode configJSON);
    public abstract JSONNode ToJSON();

    public override string ToString() => ToJSON().ToString();
}
