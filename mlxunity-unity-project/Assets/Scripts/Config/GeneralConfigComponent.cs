﻿using UnityEngine;

public class GeneralConfigComponent : MonoBehaviour
{
    [SerializeField] string projectPath = "Assets/MlxUnity/default_project.mlx/";
    GeneralConfig generalConfig;
    void Start() => generalConfig = new GeneralConfig(project: projectPath);
}
