﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class GeneralConfig : Configurable
{
    public static GeneralConfig Instance;

    public override string ConfigKey => "general";

    private const string defaultConfigPath = "Assets/MLxUnity/config_default.json";
    public const string DefaultProjectPath = "Assets/MLxUnity/default_project.mlx/";
    public string DefaultConfigPath = defaultConfigPath;
    public string ProjectPath { get; set; }
    public string ConfigPath => $"{ProjectPath}config.json";
    public string NetworkPresetsPath => $"{ProjectPath}/nn_presets/";

    public GeneralConfig(string project = DefaultProjectPath, string defaultPath = defaultConfigPath) : base()
    {
        Instance = Instance ?? this;
        ProjectPath = project;
        DefaultConfigPath = defaultPath;
    }

    public GeneralConfig Copy()
    {
        GeneralConfig clone = new GeneralConfig();
        clone.ProjectPath = this.ProjectPath;
        clone.DefaultConfigPath = this.DefaultConfigPath;
        return clone;
    }

    public override void LoadFromJSON(JSONNode configJSON) { }
    public override JSONNode ToJSON() { return new JSONObject(); }

    public override bool Equals(object obj)
    {
        GeneralConfig other = (GeneralConfig)obj;

        bool equal = (
            this.ProjectPath == other.ProjectPath &&
            this.DefaultConfigPath == other.DefaultConfigPath
        );

        return equal;
    }

    public override string ToString() => $"{base.ToString()}, ProjectPath: {ProjectPath}";
}
