﻿using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    public class GeneralConfigTests
    {
        [Test]
        public void TestCopy()
        {
            GeneralConfig clone = GeneralConfig.Instance.Copy();

            Assert.AreEqual(clone, GeneralConfig.Instance);
        }

        [Test]
        public void TestEqualsTrue()
        {
            GeneralConfig config1 = new GeneralConfig();
            GeneralConfig config2 = new GeneralConfig();

            Assert.AreEqual(config1, config2);

            config1.ProjectPath = "/SomePath/project.mlx";
            config1.DefaultConfigPath = "/SomePath/default.json";
            config2.ProjectPath = "/SomePath/project.mlx";
            config2.DefaultConfigPath = "/SomePath/default.json";

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsFalse()
        {
            GeneralConfig config1 = new GeneralConfig();
            GeneralConfig config2 = new GeneralConfig();

            config1.ProjectPath = "/SomePath/project.mlx";
            config1.DefaultConfigPath = "/SomePath/default.json";
            config2.ProjectPath = "/AnotherPath/project.mlx";
            config2.DefaultConfigPath = "/AnotherPath/default.json";

            Assert.AreNotEqual(config1, config2);
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
