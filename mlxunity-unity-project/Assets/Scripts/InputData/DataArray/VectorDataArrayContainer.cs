﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using SimpleJSON;
using DataTypes;

public class VectorDataArrayContainer : IDataArrayContainer
{
    public float[] Size { get; private set; } = { 0, 0 };

    public int Count => vectorArrays.Count;
    public DataType DataType { get; private set; }
    public int Dimensions => (int)DataType;
    delegate float[] GetSizeDelegate(float[] sizePreviousDimension);
    GetSizeDelegate[] getSizeMethods;

    Dictionary<DataType, Func<IDataArray>> dataArrayClasses = new Dictionary<DataType, Func<IDataArray>>
    {
        { DataType.Vector2, () => { return new Vector2DataArray(); } },
        { DataType.Vector3, () => { return new Vector3DataArray(); } },
    };

    bool normalized = false;
    public bool Normalized => normalized;

    bool scaled = false;
    public bool Scaled => scaled;

    List<IDataArray> vectorArrays = new List<IDataArray>();
    public List<IDataArray> Data => vectorArrays;
    public IReadOnlyList<IDataArray> Vectors => vectorArrays.AsReadOnly();

    public VectorDataArrayContainer() => InitializeBasic();
    public VectorDataArrayContainer(List<IDataArray> vectorArrays, int countMax = -1, bool scale = false, bool normalized = false)
        => InitializeFull(vectorArrays, countMax, scale, normalized);

    void InitializeBasic() => getSizeMethods = new GetSizeDelegate[] { null, getSize2, getSize3 };

    void InitializeFull(List<IDataArray> vectorArrays, int countMax = -1, bool scale = false, bool normalized = false)
    {
        InitializeBasic();

        if (vectorArrays.Count < 1)
        {
            Debug.LogWarning("Empty List of Vector3DataArrays given to VectorX3DataArray constructor, aborting initialization.");
            return;
        }

        if (countMax < 0)
            countMax = vectorArrays[0].Count;

        DataType = vectorArrays[0].DataType;

        SetVectorArrays(vectorArrays);
        if (scale)
            ScaleToLength(countMax);
        else
            SetToLength(countMax);

        this.normalized = normalized;
    }

    public void SetVectorArrays(List<IDataArray> newVectors)
    {
        vectorArrays = newVectors;
        UpdateSize();
    }

    /*
     * Scales all VectorArrays with Vector3DataArray.ScaleToLength.
    */
    public void ScaleToLength(int length)
    {
        scaled = true;
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.ScaleToLength(length);
        UpdateSize();
    }

    /*
     * Will force the trajectory force to be of a certain length.
     * If it is longer than the desired length, it will be cut to that length disgardíng all left over vectorArrays.
     * If it is shorter, it will be padded with zero vectorArrays.
     * In difference to ScaleTrajectory, this can lead to clipping trajectories off.
    */
    public void SetToLength(int length)
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.SetToLength(length);
        UpdateSize();
    }

    /*
     * Normalizes trajectory so that all x and y values are between -1 and 1.
     * This does not update size, since size is only needed for visualization which is only done
     * for un-/denormalized data.
    */
    public void Normalize()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.Normalize();
        normalized = true;
    }

    /*
        * Denormalizes trajectory so that all x and y values are between
        * -NormalizationReference and NormalizationReference.
    */
    public void Denormalize()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.Denormalize();
        normalized = false;
        // Size Informationt might be incorrect after denormalizing, thus we need to update size only after denormalizing
        UpdateSize();
    }

    public float[] Flattened()
    {
        List<float> flat = new List<float>();
        foreach (IDataArray vectorArray in vectorArrays)
            flat.AddRange(vectorArray.Flattened());
        return flat.ToArray();
    }

    public IDataArrayContainer Clone()
    {
        VectorDataArrayContainer clone = new VectorDataArrayContainer();
        clone.normalized = this.normalized;
        clone.scaled = this.scaled;
        clone.Size = this.Size;
        clone.DataType = this.DataType;

        IDataArray[] vectorArraysCopy = new IDataArray[this.Count];
        for (int i = 0; i < vectorArraysCopy.Length; i++)
            vectorArraysCopy[i] = vectorArrays[i].Clone();
        clone.vectorArrays = new List<IDataArray>(vectorArraysCopy);

        return clone;
    }

    public void Rotate(Vector3 degrees)
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.Rotate(degrees);
        UpdateSize();
    }

    public void MirrorX()
    {
        foreach(IDataArray vectorArray in vectorArrays)
            vectorArray.MirrorX();
        UpdateSize();
    }

    public void MirrorY()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.MirrorY();
        UpdateSize();
    }

    public void MirrorZ()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.MirrorZ();
        UpdateSize();
    }

    // TODO: See if shear looks close enough to the original
    public void ShearX()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.ShearX();
        UpdateSize();
    }

    public void ShearY()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.ShearY();
        UpdateSize();
    }

    public void ShearZ()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.ShearZ();
        UpdateSize();
    }

    public void Scale(float scale)
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.Scale(scale);
        UpdateSize();
    }

    public void AddNoise(float amount)
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.AddNoise(amount);
        UpdateSize();
    }

    public void InvertDirection()
    {
        foreach (IDataArray vectorArray in vectorArrays)
            vectorArray.InvertDirection();
    }

    /**
     * Calculates the width and height of the bounding box of a given inputData.
     * The exact calculation happening is determined by the number of dimensions that vectors of this container have.
     */
    void UpdateSize()
    {
        float[] lastSize = null;
        // For every dimnesion there might a method (e.g. two dimensions is calculated with getSize2).
        // We can calculate that size and then add one more dimension to the resulting vector, in which we write
        // the size on the new dimension (e.g. getSize3 will not recalculate x and y dimension but copy it from getSize2's results)
        for (int i = 0; i < Dimensions; i++)
        {
            GetSizeDelegate getSizeMethod = getSizeMethods[i];
            if (getSizeMethod == null)
                continue;
            lastSize = getSizeMethod(lastSize);
        }
        Size = lastSize;
    }

    float[] getSize2(float[] size1 = null)
    {
        Vector2 topLeftCorner = new Vector2();
        Vector2 bottomRightCorner = new Vector2();
        Vector3 currentPosition = new Vector2();

        foreach (IDataArray vectorArray in vectorArrays)
            foreach (object vectorObject in vectorArray.Data)
            {
                // TODO: needs a more flexible
                Vector3 vector;
                try
                {
                    vector = (Vector3)vectorObject;
                }
                catch
                {
                    vector = (Vector2)vectorObject;
                }
                currentPosition += vector;

                if (currentPosition.x < topLeftCorner.x)
                    topLeftCorner = new Vector2(currentPosition.x, topLeftCorner.y);
                if (currentPosition.y < topLeftCorner.y)
                    topLeftCorner = new Vector2(topLeftCorner.x, currentPosition.y);

                if (currentPosition.x > bottomRightCorner.x)
                    bottomRightCorner = new Vector2(currentPosition.x, bottomRightCorner.y);
                if (currentPosition.y > bottomRightCorner.y)
                    bottomRightCorner = new Vector2(bottomRightCorner.x, currentPosition.y);
            }

        Vector2 delta = topLeftCorner - bottomRightCorner;
        return new float[] { Mathf.Abs(delta.x), Mathf.Abs(delta.y) };
    }

    float[] getSize3(float[] size2 = null)
    {
        float min = 0;
        float max = 0;
        float currentPosition = 0;

        foreach (IDataArray vectorArray in vectorArrays)
            foreach (Vector3 vector in vectorArray.Data)
            {
                currentPosition += vector.z;
                if (currentPosition < min)
                    min = currentPosition;
                if (currentPosition > max)
                    max = currentPosition;
            }

        float delta = min - max;
        return new float[] { size2[0], size2[1], Mathf.Abs(delta) };
    }

    public JSONNode ToJSON()
    {
        JSONObject json = new JSONObject();

        JSONArray jsonVectors = new JSONArray();
        foreach (IDataArray vectorArray in vectorArrays)
            jsonVectors.Add(vectorArray.ToJSON());

        json.Add("normalized", normalized);
        json.Add("scaled", scaled);
        json.Add("data_type", DataType.ToString());
        json.Add("data", jsonVectors);

        return json;
    }

    public void LoadFromJSON(JSONNode json)
    {
        JSONArray vectorArraysJSON = json["data"].AsArray;
        DataType dataType = (DataType)Enum.Parse(typeof(DataType), json["data_type"], true);
        List<IDataArray> vectorArrays = new List<IDataArray>();
        foreach (JSONNode vectorArrayJSONNode in vectorArraysJSON)
        {
            JSONArray vectorArrayJSON = (JSONArray)vectorArrayJSONNode;
            IDataArray vectorArray = dataArrayClasses[dataType]();
            vectorArray.LoadFromJSON(vectorArrayJSON, normalized, scaled);
            vectorArrays.Add(vectorArray);
        }

        InitializeFull(vectorArrays, scale: json["scaled"].AsBool, normalized: json["normalized"].AsBool);
    }

    public override bool Equals(object obj)
    {
        VectorDataArrayContainer other = (VectorDataArrayContainer)obj;
        if (this.Count != other.Count)
            return false;
        return this.vectorArrays.SequenceEqual(other.vectorArrays);
    }

    public override string ToString()
    {
        string s = "";

        foreach (IDataArray vector in vectorArrays)
            s += vector.ToString() + ", ";
        if (vectorArrays.Count > 0)
            s = s.Substring(0, s.Length - 2);

        return $"VectorDataArrayContainer: Count: {Count}, Data: [{s}]";
    }
}
