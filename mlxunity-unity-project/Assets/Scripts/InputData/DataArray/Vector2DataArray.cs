﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using SimpleJSON;
using DataTypes;
using CollectionExtensions;

public class Vector2DataArray : IDataArray
{
    static public float NormalizationReference = 1f;

    // https://answers.unity.com/questions/661383/whats-the-most-efficient-way-to-rotate-a-vector2-o.html
    static private Vector2 RotateVector(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public bool deactivateAutoupdateSize { get; set; } = false;

    public int Count => vectors.Count;
    public DataType DataType => DataType.Vector2;
    public int Dimensions => (int)DataType;
    public Type VectorType => typeof(Vector2);

    bool normalized = false;
    public bool Normalized => normalized;

    bool scaled = false;
    public bool Scaled => scaled;

    List<Vector2> vectors = new List<Vector2>();
    public IList Data => vectors;
    public IReadOnlyList<Vector2> Vectors => vectors.AsReadOnly();
    
    // this is the number of vectors actually containing information (without padded zero vectors etc)
    public int ActualCount { get; private set; } = -1;

    public Vector2DataArray() { }

    public Vector2DataArray(
        List<Vector2> vectors,
        int countMax = -1,
        bool normalized = false,
        bool scale = false
    )
    {
        if (countMax < 0)
            countMax = vectors.Count;

        SetVectors(vectors);

        if (scale)
            ScaleToLength(countMax);
        else
            SetToLength(countMax);

        this.normalized = normalized;
    }

    public void SetVectors(List<Vector2> newVectors)
    {
        vectors = newVectors;
        if (IsZeroPadded())
            ActualCount = vectors.Count - ZeroPaddedVectorsCount();
        else
            ActualCount = vectors.Count;
    }

    /*
    * Have the last vectors been padded with zero vectors?
    */
    public bool IsZeroPadded() => vectors.Exists(vector => vector.magnitude == 0);

    /**
     * Counts the number of zero padded vectors at the end trajectory.
     */
    public int ZeroPaddedVectorsCount()
    {
        int count = 0;

        for (int i = vectors.Count - 1; i > -1; i--)
        {
            Vector2 vector = vectors[i];
            if (vector.magnitude > 0)
                // reached first non-padded vector, no further padding to be expected from here on
                break;

            count++;
        }

        return count;
    }

    /*
     * Scales the trajectory segmentation into vectors, so it is of certain length.
     * This will take the current trajectory of the Gesture and split it up into a specified number of vectors.
     * The previous vector count can be arbitrary.
     * The outcoming trajectory will represent the original one as closely as possible with the new vector count.
     * This will in no way change the size or dimensions of the trajectory.
     * It might make slight changes to the orientations of some vectors.
     * In difference to SetTrajectoryToLength, this will never clip the trajectory, but alter it shape slightly.
    */
    public void ScaleToLength(int length)
    {
        scaled = true;

        List<Vector2> trajectoryUnpadded = GetUnpadded();

        // if trajectory already has the desired amount of vectors, we don't need to do anything
        if (trajectoryUnpadded.Count == length)
            return;

        // this variable will hold the final scaled trajectory
        List<Vector2> scaledVectors = new List<Vector2>();

        // This is the realtive length of the new trajectory's vectors,
        // multiplied with the current vector you get the actual segment.
        // Most likely all segments will be of the same absolut length from the beginning, being Recorder.minMovementDistance.
        float newSegmentLength = (float)trajectoryUnpadded.Count / (float)length;

        // previous absolute position on the trajectory
        // (needed to create segments, which are drawn from positionPrevious to positionCurrent)
        Vector2 positionPrevious = new Vector2();

        // current absolute position on the trajectory
        Vector2 positionCurrent = new Vector2();

        float relativePositionPrevious = 0;
        float relativePositionFractionPrevious = 0;
        int relativePositionIntPartPrevious = 0;

        // We need to find out what the vectors for each segment are,
        // being as many vectors as vectorsCount defines.
        for (int i = 1; i <= length; i++)
        {
            // relative position on the trajectory with integer part and fraction
            float relativePosition = i * newSegmentLength;
            int relativePositionIntPart = (int)relativePosition;
            float relativePositionFraction = relativePosition - (float)relativePositionIntPart;

            // length of the new segment (relative measure) that we still need to cover this round
            float LengthToCover = relativePosition - relativePositionPrevious;

            // Firstly, finish off the current old segment's fraction if we are entering a new old segment
            if (relativePositionIntPart > relativePositionIntPartPrevious)
            {
                // this is how much of the old segment remains
                float segmentRemainder = (1f - relativePositionFractionPrevious);
                // add this remainder to the position, so now the current position is at the end of this old segment
                positionCurrent += trajectoryUnpadded[relativePositionIntPartPrevious] * segmentRemainder;
                // we just covered a relative length of segmentRemainder
                LengthToCover -= segmentRemainder;
                // we entered the next old segment and so the relative position increments by 1
                relativePositionIntPartPrevious++;
            }

            // get int part and fraction of the length to cover
            int LengthToCoverIntPart = (int)LengthToCover;
            float LengthToCoverFraction = LengthToCover - (float)LengthToCoverIntPart;

            // We now try to find out where the end of our new segment is,
            // which is at the absolute position belonging to our already known relative position.
            // We also know the beginning of it: positionPrevious

            // iterate through all segments of the old trajectory till we covered the integer part
            for (int j = 0; j < LengthToCoverIntPart; j++)
                positionCurrent += vectors[relativePositionIntPartPrevious + j];

            // add the remaining fraction, but only if there is a fraction
            // (this might cause calls outside the trajectory array's bounds)
            try
            {
                if (LengthToCoverFraction > 0)
                    positionCurrent += vectors[relativePositionIntPart] * LengthToCoverFraction;
            }
            catch { }
            
            // positionCurrent nows holds the endpoint of our new segment, while positionPrevious has the startpoint
            // the new segment runs from the previous to the current position
            Vector2 newSegment = (positionCurrent - positionPrevious);

            // add the new segment to the new trajectory
            scaledVectors.Add(newSegment);

            // save current position as previous and new startpoint for next segment
            positionPrevious = positionCurrent;

            // save relative position of this round for the next as reference
            relativePositionPrevious = relativePosition;
            relativePositionFractionPrevious = relativePositionFraction;
            relativePositionIntPartPrevious = relativePositionIntPart;
        }

        SetVectors(scaledVectors);
    }

    /*
     * Will force the trajectory force to be of a certain length.
     * If it is longer than the desired length, it will be cut to that length disgardíng all left over vectors.
     * If it is shorter, it will be padded with zero vectors.
     * In difference to ScaleTrajectory, this can lead to clipping trajectories off.
    */
    public void SetToLength(int length)
    {
        // if trajectory already has the desired amount of vectors, we don't need to do anything
        if (vectors.Count == length)
            return;

        List<Vector2> newVectors = new List<Vector2>();
        int trajectoryLength = vectors.Count;

        if (trajectoryLength > length)
        {
            // desired length is shorter: keep only the vectors within the desired length, disgard all beyond that
            newVectors = vectors.GetRange(0, length);
        }
        else
        {
            // desired length is longer: keep all vectors and pad zero vectors until desired length is reached
            newVectors = vectors;
            for (int i = trajectoryLength; i < length; i++)
            {
                newVectors.Add(new Vector2());
            }
        }

        SetVectors(newVectors);
    }

    public void ClampVectors(float coordinateValue = 1f, List<Vector2> input = null)
    {
        if (input == null)
            input = this.vectors;

        List<Vector2> newVectors = new List<Vector2>();
        foreach (Vector2 vector in input)
            newVectors.Add(
                new Vector2(
                    Mathf.Clamp(vector.x, -coordinateValue, coordinateValue),
                    Mathf.Clamp(vector.y, -coordinateValue, coordinateValue)
                )
            );
        SetVectors(newVectors);
    }

    // TODO: test
    public List<Vector2> GetUnpadded()
    {
        // only unpad trajectory if vectorCount makes sense to be used to find zero vectors
        if (ActualCount > 0 && ActualCount < vectors.Count)
            return vectors.GetRange(0, ActualCount - 1);

        return vectors;
    }

    /*
     * Normalizes trajectory so that all x and y values are between -1 and 1.
    */
    public void Normalize()
    {
        List<Vector2> vectorsNormalized = new List<Vector2>();

        foreach (Vector2 vector in vectors)
            vectorsNormalized.Add(vector / NormalizationReference);

        ClampVectors(1f, input: vectorsNormalized);
        normalized = true;
    }

    /*
        * Denormalizes trajectory so that all x and y values are between
        * -NormalizationReference and NormalizationReference.
    */
    public void Denormalize()
    {
        List<Vector2> vectorsDenormalized = new List<Vector2>();

        foreach (Vector2 vector in vectors)
            vectorsDenormalized.Add(vector * NormalizationReference);

        normalized = false;
        vectors = vectorsDenormalized;
    }

    public float[] Flattened()
    {
        float[] flat = new float[vectors.Count * 2];

        int index = 0;
        foreach (Vector2 vector in vectors)
        {
            flat[index] = vector.x;
            index++;
            flat[index] = vector.y;
            index++;
        }

        Debug.Log(flat.Length);
        return flat;
    }

    public IDataArray Clone()
    {
        Vector2DataArray clone = new Vector2DataArray();
        clone.ActualCount = this.ActualCount;
        clone.normalized = this.normalized;
        clone.scaled = this.scaled;

        Vector2[] vectorsCopy = new Vector2[this.Count];
        this.vectors.CopyTo(vectorsCopy);
        clone.vectors = new List<Vector2>(vectorsCopy);

        return clone;
    }

    public void Rotate(Vector3 degrees)
    {
        List<Vector2> newVectors = new List<Vector2>();
        foreach (Vector2 vector in vectors)
            newVectors.Add(RotateVector(vector, degrees.z));

        vectors = newVectors;
    }

    public void MirrorX()
    {
        List<Vector2> newVectors = new List<Vector2>();
        foreach (Vector2 vector in vectors)
            newVectors.Add(new Vector2(vector.x * -1f, vector.y));

        vectors = newVectors;
    }

    public void MirrorY()
    {
        List<Vector2> newVectors = new List<Vector2>();
        foreach (Vector2 vector in vectors)
            newVectors.Add(new Vector2(vector.x, vector.y * -1f));

        vectors = newVectors;
    }

    public void MirrorZ() => throw new NotImplementedException();

    public void ShearX()
    {
        List<Vector2> newVectors = new List<Vector2>();
        foreach (Vector2 vector in vectors)
            newVectors.Add(new Vector2(vector.x + vector.y, vector.y));

        vectors = newVectors;
    }

    public void ShearY()
    {
        List<Vector2> newVectors = new List<Vector2>();
        foreach (Vector2 vector in vectors)
            newVectors.Add(new Vector2(vector.x, vector.y + vector.x));

        vectors = newVectors;
    }

    public void ShearZ() => throw new NotImplementedException();

    public void Scale(float scale)
    {
        List<Vector2> newVectors = new List<Vector2>();
        foreach (Vector2 vector in vectors)
            newVectors.Add(vector * scale);

        vectors = newVectors;
    }

    public void AddNoise(float amount)
    {
        List<Vector2> newVectors = new List<Vector2>();
        float amountHalf = amount * .5f;
        foreach (Vector2 vector in vectors)
            newVectors.Add(new Vector2(
                vector.x + UnityEngine.Random.Range(-amountHalf, amountHalf),
                vector.y + UnityEngine.Random.Range(-amountHalf, amountHalf)
            ) * vector.magnitude);

        vectors = newVectors;
    }

    public void InvertDirection()
    {
        List<Vector2> newVectors = new List<Vector2>();
        for (int i = Count - 1; i > -1; i--)
            newVectors.Add(-vectors[i]);

        vectors = newVectors;
    }

    public JSONNode ToJSON()
    {
        JSONArray json = new JSONArray();

        foreach (Vector2 vector in vectors)
        {
            JSONArray vectorJSON = new JSONArray();
            vectorJSON.Add(vector.x);
            vectorJSON.Add(vector.y);

            json.Add(vectorJSON);
        }
        return json;
    }

    public void LoadFromJSON(JSONNode json, bool normalized, bool scaled)
    {
        this.normalized = normalized;
        this.scaled = scaled;

        JSONArray vectorsJSON = json.AsArray;
        List<Vector2> vectors = new List<Vector2>();
        foreach (JSONNode vectorJSONNode in vectorsJSON)
        {
            JSONArray vectorJSON = (JSONArray)vectorJSONNode;
            Vector2 vector = new Vector2(vectorJSON[0], vectorJSON[1]);
            vectors.Add(vector);
        }

        SetVectors(vectors);
    }

    public override bool Equals(object obj)
    {
        Vector2DataArray other = (Vector2DataArray)obj;
        if (this.Count != other.Count)
            return false;
        return this.vectors.SequenceEqualApproximate<Vector2>(other.vectors);
    }

    public override string ToString()
    {
        string s = "";

        foreach (Vector2 vector in vectors)
            s += vector.ToString() + ", ";
        if (vectors.Count > 0)
            s = s.Substring(0, s.Length - 2);

        return $"Vector2DataArray: Count: {Count}, Data: [{s}]";
    }
}
