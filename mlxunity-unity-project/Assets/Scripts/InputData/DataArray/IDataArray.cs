﻿using System.Collections;
using SimpleJSON;
using UnityEngine;

public interface IDataArray
{
    int Count { get; }
    DataTypes.DataType DataType { get; }
    int Dimensions { get; }
    bool Normalized { get; }
    bool Scaled { get; }
    IList Data { get; }

    void Normalize();
    void Denormalize();
    void ScaleToLength(int length);
    void SetToLength(int length);
    float[] Flattened();
    IDataArray Clone();
    JSONNode ToJSON();
    void LoadFromJSON(JSONNode json, bool normalized, bool scaled);

    void MirrorX();
    void MirrorY();
    void MirrorZ();
    void ShearX();
    void ShearY();
    void ShearZ();
    void Scale(float scale);
    void AddNoise(float amount);
    void InvertDirection();
    void Rotate(Vector3 degrees);
}
