﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataTypes
{
    // int value is dimension count of that data type
    public enum DataType : int
    {
        None = 0,
        Vector1 = 1,
        Vector2 = 2,
        Vector3 = 3,
    }
}
