﻿using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public interface IDataArrayContainer
{
    int Count { get; }
    DataTypes.DataType DataType { get; }
    int Dimensions { get; }
    float[] Size { get; }
    bool Normalized { get; }
    bool Scaled { get; }
    List<IDataArray> Data { get; }

    void Normalize();
    void Denormalize();
    void ScaleToLength(int length);
    void SetToLength(int length);
    float[] Flattened();
    IDataArrayContainer Clone();
    JSONNode ToJSON();
    void LoadFromJSON(JSONNode json);

    void MirrorX();
    void MirrorY();
    void MirrorZ();
    void ShearX();
    void ShearY();
    void ShearZ();
    void Scale(float scale);
    void AddNoise(float amount);
    void InvertDirection();
    void Rotate(Vector3 degrees);
}
