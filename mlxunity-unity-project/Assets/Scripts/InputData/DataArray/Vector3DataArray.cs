﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using DataTypes;
using CollectionExtensions;

public class Vector3DataArray : IDataArray
{
    static public float NormalizationReference = 1f;
    static private Vector3 RotateVector(Vector3 vector, Vector3 degrees) => Quaternion.Euler(degrees) * vector;

    public int Count => vectors.Count;
    public DataType DataType => DataType.Vector3;
    public int Dimensions => (int)DataType;
    public Type VectorType => typeof(Vector3);

    bool normalized = false;
    public bool Normalized => normalized;

    bool scaled = false;
    public bool Scaled => scaled;

    List<Vector3> vectors = new List<Vector3>();
    public IList Data => vectors;
    public IReadOnlyList<Vector3> Vectors => vectors.AsReadOnly();
    
    // this is the number of vectors actually containing information (without padded zero vectors etc)
    public int ActualCount { get; private set; } = -1;

    public Vector3DataArray() { }
    public Vector3DataArray(List<Vector3> vectors) => SetVectors(vectors);

    public Vector3DataArray(
        List<Vector3> vectors,
        int countMax = -1,
        bool scale = false
    )
    {
        if (countMax < 0)
            countMax = vectors.Count;

        SetVectors(vectors);

        if (scale)
            ScaleToLength(countMax);
        else
            SetToLength(countMax);
    }

    public void SetVectors(List<Vector3> newVectors)
    {
        vectors = newVectors;
        if (IsZeroPadded())
            ActualCount = vectors.Count - ZeroPaddedVectorsCount();
        else
            ActualCount = vectors.Count;
    }

    /*
    * Have the last vectors been padded with zero vectors?
    */
    public bool IsZeroPadded() => vectors.Exists(vector => vector.magnitude == 0);

    /**
     * Counts the number of zero padded vectors at the end trajectory.
     */
    public int ZeroPaddedVectorsCount()
    {
        int count = 0;

        for (int i = vectors.Count - 1; i > -1; i--)
        {
            Vector3 vector = vectors[i];
            if (vector.magnitude > 0)
                // reached first non-padded vector, no further padding to be expected from here on
                break;

            count++;
        }

        return count;
    }

    /*
     * Scales the trajectory segmentation into vectors, so it is of certain length.
     * This will take the current trajectory of the Gesture and split it up into a specified number of vectors.
     * The previous vector count can be arbitrary.
     * The outcoming trajectory will represent the original one as closely as possible with the new vector count.
     * This will in no way change the size or dimensions of the trajectory.
     * It might make slight changes to the orientations of some vectors.
     * In difference to SetTrajectoryToLength, this will never clip the trajectory, but alter it shape slightly.
    */
    public void ScaleToLength(int length)
    {
        scaled = true;

        List<Vector3> trajectoryUnpadded = GetUnpadded();

        // if trajectory already has the desired amount of vectors, we don't need to do anything
        if (trajectoryUnpadded.Count == length)
            return;

        // this variable will hold the final scaled trajectory
        List<Vector3> scaledVectors = new List<Vector3>();

        // This is the realtive length of the new trajectory's vectors,
        // multiplied with the current vector you get the actual segment.
        // Most likely all segments will be of the same absolut length from the beginning, being Recorder.minMovementDistance.
        float newSegmentLength = (float)trajectoryUnpadded.Count / (float)length;

        // previous absolute position on the trajectory
        // (needed to create segments, which are drawn from positionPrevious to positionCurrent)
        Vector3 positionPrevious = new Vector3();

        // current absolute position on the trajectory
        Vector3 positionCurrent = new Vector3();

        float relativePositionPrevious = 0;
        float relativePositionFractionPrevious = 0;
        int relativePositionIntPartPrevious = 0;

        // We need to find out what the vectors for each segment are,
        // being as many vectors as vectorsCount defines.
        for (int i = 1; i <= length; i++)
        {
            // relative position on the trajectory with integer part and fraction
            float relativePosition = i * newSegmentLength;
            int relativePositionIntPart = (int)relativePosition;
            float relativePositionFraction = relativePosition - (float)relativePositionIntPart;

            // length of the new segment (relative measure) that we still need to cover this round
            float LengthToCover = relativePosition - relativePositionPrevious;

            // Firstly, finish off the current old segment's fraction if we are entering a new old segment
            if (relativePositionIntPart > relativePositionIntPartPrevious)
            {
                // this is how much of the old segment remains
                float segmentRemainder = (1f - relativePositionFractionPrevious);
                // add this remainder to the position, so now the current position is at the end of this old segment
                positionCurrent += trajectoryUnpadded[relativePositionIntPartPrevious] * segmentRemainder;
                // we just covered a relative length of segmentRemainder
                LengthToCover -= segmentRemainder;
                // we entered the next old segment and so the relative position increments by 1
                relativePositionIntPartPrevious++;
            }

            // get int part and fraction of the length to cover
            int LengthToCoverIntPart = (int)LengthToCover;
            float LengthToCoverFraction = LengthToCover - (float)LengthToCoverIntPart;

            // We now try to find out where the end of our new segment is,
            // which is at the absolute position belonging to our already known relative position.
            // We also know the beginning of it: positionPrevious

            // iterate through all segments of the old trajectory till we covered the integer part
            for (int j = 0; j < LengthToCoverIntPart; j++)
                positionCurrent += vectors[relativePositionIntPartPrevious + j];

            try
            {
                if (LengthToCoverFraction > 0)
                    positionCurrent += vectors[relativePositionIntPart] * LengthToCoverFraction;
            }
            catch { }

            // positionCurrent nows holds the endpoint of our new segment, while positionPrevious has the startpoint
            // the new segment runs from the previous to the current position
            Vector3 newSegment = (positionCurrent - positionPrevious);

            // add the new segment to the new trajectory
            scaledVectors.Add(newSegment);

            // save current position as previous and new startpoint for next segment
            positionPrevious = positionCurrent;

            // save relative position of this round for the next as reference
            relativePositionPrevious = relativePosition;
            relativePositionFractionPrevious = relativePositionFraction;
            relativePositionIntPartPrevious = relativePositionIntPart;
        }

        SetVectors(scaledVectors);
    }

    /*
     * Will force the trajectory force to be of a certain length.
     * If it is longer than the desired length, it will be cut to that length disgardíng all left over vectors.
     * If it is shorter, it will be padded with zero vectors.
     * In difference to ScaleTrajectory, this can lead to clipping trajectories off.
    */
    public void SetToLength(int length)
    {
        // if trajectory already has the desired amount of vectors, we don't need to do anything
        if (vectors.Count == length)
            return;

        List<Vector3> newVectors = new List<Vector3>();
        int trajectoryLength = vectors.Count;

        if (trajectoryLength > length)
        {
            // desired length is shorter: keep only the vectors within the desired length, disgard all beyond that
            newVectors = vectors.GetRange(0, length);
        }
        else
        {
            // desired length is longer: keep all vectors and pad zero vectors until desired length is reached
            newVectors = vectors;
            for (int i = trajectoryLength; i < length; i++)
            {
                newVectors.Add(new Vector3());
            }
        }

        SetVectors(newVectors);
    }

    public void ClampVectors(float coordinateValue = 1f, List<Vector3> input = null)
    {
        if (input == null)
            input = this.vectors;

        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in input)
            newVectors.Add(
                new Vector3(
                    Mathf.Clamp(vector.x, -coordinateValue, coordinateValue),
                    Mathf.Clamp(vector.y, -coordinateValue, coordinateValue),
                    Mathf.Clamp(vector.z, -coordinateValue, coordinateValue)
                )
            );
        SetVectors(newVectors);
    }

    // TODO: test
    public List<Vector3> GetUnpadded()
    {
        // only unpad trajectory if vectorCount makes sense to be used to find zero vectors
        if (ActualCount > 0 && ActualCount < vectors.Count)
            return vectors.GetRange(0, ActualCount - 1);

        return vectors;
    }

    /*
     * Normalizes trajectory so that all x and y values are between -1 and 1.
    */
    public void Normalize()
    {
        List<Vector3> vectorsNormalized = new List<Vector3>();

        foreach (Vector3 vector in vectors)
            vectorsNormalized.Add(vector / NormalizationReference);

        ClampVectors(1f, input: vectorsNormalized);
        normalized = true;
    }

    /*
        * Denormalizes trajectory so that all x and y values are between
        * -NormalizationReference and NormalizationReference.
    */
    public void Denormalize()
    {
        List<Vector3> vectorsDenormalized = new List<Vector3>();

        foreach (Vector3 vector in vectors)
            vectorsDenormalized.Add(vector * NormalizationReference);

        normalized = false;
        vectors = vectorsDenormalized;
    }

    public float[] Flattened()
    {
        float[] flat = new float[vectors.Count * 3];

        int index = 0;
        foreach (Vector3 vector in vectors)
        {
            flat[index] = vector.x;
            index++;
            flat[index] = vector.y;
            index++;
            flat[index] = vector.z;
            index++;
        }

        return flat;
    }

    public IDataArray Clone()
    {
        Vector3DataArray clone = new Vector3DataArray();
        clone.ActualCount = this.ActualCount;
        clone.normalized = this.normalized;
        clone.scaled = this.scaled;

        Vector3[] vectorsCopy = new Vector3[this.Count];
        this.vectors.CopyTo(vectorsCopy);
        clone.vectors = new List<Vector3>(vectorsCopy);

        return clone;
    }

    public void Rotate(Vector3 degrees)
    {
        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in vectors)
            newVectors.Add(RotateVector(vector, degrees));

        vectors = newVectors;
    }

    public void MirrorX()
    {
        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in vectors)
            newVectors.Add(new Vector3(vector.x * -1f, vector.y, vector.z));

        vectors = newVectors;
    }

    public void MirrorY() => throw new NotImplementedException();

    public void MirrorZ()
    {
        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in vectors)
            newVectors.Add(new Vector3(vector.x, vector.y, vector.z * -1f));

        vectors = newVectors;
    }

    // TODO: See if shear looks close enough to the original
    public void ShearX()
    {
        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in vectors)
            newVectors.Add(new Vector3(vector.x + vector.y, vector.y, vector.z));

        vectors = newVectors;
    }

    public void ShearY()
    {
        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in vectors)
            newVectors.Add(new Vector3(vector.x, vector.y + vector.x, vector.z));

        vectors = newVectors;
    }

    public void ShearZ()
    {
        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in vectors)
            newVectors.Add(new Vector3(vector.x, vector.y, vector.z + vector.y));

        vectors = newVectors;
    }

    public void Scale(float scale)
    {
        List<Vector3> newVectors = new List<Vector3>();
        foreach (Vector3 vector in vectors)
            newVectors.Add(vector * scale);

        vectors = newVectors;
    }

    public void AddNoise(float amount)
    {
        List<Vector3> newVectors = new List<Vector3>();
        float amountHalf = amount * .5f;
        foreach (Vector3 vector in vectors)
            newVectors.Add(new Vector3(
                vector.x + UnityEngine.Random.Range(-amountHalf, amountHalf),
                vector.y + UnityEngine.Random.Range(-amountHalf, amountHalf),
                vector.z + UnityEngine.Random.Range(-amountHalf, amountHalf)
            ) * vector.magnitude);

        vectors = newVectors;
    }

    public void InvertDirection()
    {
        List<Vector3> newVectors = new List<Vector3>();
        for (int i = Count - 1; i > -1; i--)
            newVectors.Add(-vectors[i]);

        vectors = newVectors;
    }

    public JSONNode ToJSON()
    {
        JSONArray json = new JSONArray();

        foreach (Vector3 vector in vectors)
        {
            JSONArray vectorJSON = new JSONArray();
            vectorJSON.Add(vector.x);
            vectorJSON.Add(vector.y);
            vectorJSON.Add(vector.z);

            json.Add(vectorJSON);
        }

        return json;
    }

    public void LoadFromJSON(JSONNode json, bool normalized, bool scaled)
    {
        this.normalized = normalized;
        this.scaled = scaled;

        JSONArray vectorsJSON = json.AsArray;
        List<Vector3> vectors = new List<Vector3>();
        foreach (JSONNode vectorJSONNode in vectorsJSON)
        {
            JSONArray vectorJSON = (JSONArray)vectorJSONNode;
            Vector3 vector = new Vector3(vectorJSON[0], vectorJSON[1], vectorJSON[2]);
            vectors.Add(vector);
        }

        SetVectors(vectors);
    }

    public override bool Equals(object obj)
    {
        Vector3DataArray other = (Vector3DataArray)obj;
        if (this.Count != other.Count)
            return false;
        return this.vectors.SequenceEqualApproximate<Vector3>(other.vectors);
    }

    public override string ToString()
    {
        string s = "";

        foreach (Vector3 vector in vectors)
            s += vector.ToString() + ", ";
        if (vectors.Count > 0)
            s = s.Substring(0, s.Length - 2);

        return $"Vector3DataArray: Count: {Count}, Data: [{s}]";
    }
}
