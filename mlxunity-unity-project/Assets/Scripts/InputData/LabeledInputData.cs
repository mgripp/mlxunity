﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public abstract class LabeledInputData
{
    public IDataArrayContainer inputData;
    // TODO: Why protected and not private?
    protected internal IClass labelClass;
    public IClass LabelClass
    {
        get { return labelClass; }
        set { labelClass.CopyFrom(value); }
    }
    public int ClassIndex => labelClass.Index;
    public string ClassName => labelClass.Name;

    public bool Scaled => inputData.Scaled;
    public bool Normalized => inputData.Normalized;

    protected List<LabeledInputData> augmentations = null;
    public IReadOnlyList<LabeledInputData> Augmentations {
        get {
            if (augmentations == null)
                return null;
            return augmentations.AsReadOnly();
        }
    }
    public int AugmentationCount
    {
        get
        {
            if (augmentations != null)
                return augmentations.Count;
            return 0;
        }
    }

    protected AugmentationTree augmentationTree;

    /*
     * Augments this gesture and adds all produced augmentations to respective list "augmentations" in this object".
     * Also augments all created augmentations crossing different augmentations techniques. These are also added to "augmentations".
     */
    virtual public void Augment() => augmentations = augmentationTree.Augment(this);

    public LabeledInputData() => Initialize();
    protected void Initialize() { labelClass = new Class(); }

    public LabeledInputData(JSONObject json)
    {
        Initialize();
        LoadFromJSON(json);
    }

    public void SetClass(int classIndex) => LabelClass = ClassesConfig.Instance.Classes[classIndex];
    public void RemoveAugmentation(LabeledInputData augmentation) => augmentations.Remove(augmentation);
    public void RemoveAugmentation(int index) => RemoveAugmentation(augmentations[index]);

    /*
     * Normalizes inputData so that all x and y values are between -1 and 1.
    */
    public void Normalize()
    {
        inputData.Normalize();

        if (augmentations == null)
            return;
        foreach (LabeledInputData augmentation in augmentations)
            augmentation.Normalize();
    }

    /*
        * Denormalizes inputData so that all x and y values are between values defined in
        * implementing class's inputData-class.
    */
    public void Denormalize()
    {
        inputData.Denormalize();

        if (augmentations == null)
            return;
        foreach (LabeledInputData augmentation in augmentations)
            augmentation.Denormalize();
    }

    public JSONObject ToJSON()
    {
        JSONObject gestureJSON = new JSONObject();
        gestureJSON.Add("class", ClassIndex);
        gestureJSON.Add("name", ClassName);

        // Add augmentations if present
        if (augmentations != null)
        {
            JSONArray augmentationsJSON = new JSONArray();
            foreach (IAugmentation augmentation in augmentations)
                augmentationsJSON.Add(augmentation.ToJSON());
            gestureJSON.Add("augmentations", augmentationsJSON);
        }

        gestureJSON.Add("input_data", inputData.ToJSON());

        return gestureJSON;
    }

    public void LoadFromJSON(JSONNode json)
    {
        Initialize();
        SetClass(json["class"].AsInt);
        inputData.LoadFromJSON(json["input_data"]);

        // get augmentations of present
        if (json.HasKey("augmentations"))
        {
            JSONArray augmentationsJSON = json["augmentations"].AsArray;
            augmentations = new List<LabeledInputData>();
            foreach (JSONObject augmentationJSON in augmentationsJSON)
            {
                LabeledInputData augmentation = GetAugmentation();
                augmentation.LoadFromJSON(augmentationJSON);
                augmentations.Add(augmentation);
            }
        }
    }

    virtual public LabeledInputData Clone()
    {
        LabeledInputData clone = (LabeledInputData)this.MemberwiseClone();

        clone.inputData = this.inputData.Clone();

        if (this.augmentations != null)
        {
            clone.augmentations = new List<LabeledInputData>();

            foreach (LabeledInputData augmentation in this.augmentations)
                clone.augmentations.Add(augmentation.Clone());
        }

        return clone;
    }

    public override bool Equals(object obj)
    {
        LabeledInputData other = (LabeledInputData)obj;

        bool equal = other != null &&
            this.inputData.Equals(other.inputData) &&
            this.ClassIndex == other.ClassIndex &&
            this.Normalized == other.Normalized &&
            this.Scaled == other.Scaled;

        if (!equal)
            return false;

        // both have augmentations or both have none
        // the exact content of the augmentations does not have to be equal since they might be based on randomness
        // also you can assume equal augmentation (or at least similar) with same inputDatas (will be checked below)
        equal = (this.augmentations == null && other.augmentations == null) ||
            ((this.augmentations != null && other.augmentations != null));

        return equal;
    }

    abstract public LabeledInputData GetAugmentation();
    public override string ToString() => ToJSON().ToString();
}
