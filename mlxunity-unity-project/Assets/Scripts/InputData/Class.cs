﻿using SimpleJSON;

public class Class : IClass
{
    public int Index { get; private set; } = 0;
    public string Name { get; private set; } = "Undefined";
    public string RecordPath { get; private set; } = null;
    public Class inverse = null;

    public Class() { }

    public Class(int index, string name, string recordPath)
    {
        Index = index;
        Name = name;
        this.RecordPath = recordPath;
    }

    public void CopyFrom(IClass other)
    {
        Class otherClass = (Class)other;
        this.Index = otherClass.Index;
        this.Name = otherClass.Name;
        this.RecordPath = otherClass.RecordPath;
        this.inverse = otherClass.inverse;
    }

    public JSONNode ToJSON()
    {
        JSONNode jsonClass = new JSONObject();
        jsonClass["index"] = Index;
        jsonClass["name"] = Name;
        if (inverse == null)
            jsonClass["inverse"] = null;
        else
            jsonClass["inverse"] = inverse?.Name;
        jsonClass["input_json_path"] = RecordPath;

        return jsonClass;
    }

    public override string ToString() => ToJSON().ToString();

    public override bool Equals(object obj)
    {
        Class other = (Class)obj;
        return this.Name == other.Name && (this.inverse == null && other.inverse == null || this.inverse.Name == other.inverse.Name);
    }
}
