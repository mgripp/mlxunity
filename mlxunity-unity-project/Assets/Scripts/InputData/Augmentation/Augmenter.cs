﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Augmenter
{
    protected string name = "None";

    List<Augmenter> followers = new List<Augmenter>();

    public Augmenter() { }
    public Augmenter(List<Augmenter> followers) => this.followers = followers;
    public Augmenter(Augmenter follower) => this.followers = new List<Augmenter>(new Augmenter[] { follower });

    virtual public List<LabeledInputData> Process(IAugmentation augmentation)
    {
        // Repeat normalization if augmentation has been normalized before augmenting it
        // since it might have become denormalized by this Augmenter.
        if (augmentation.Normalized)
            augmentation.Normalize();
        augmentation.LastAugmentation = name;
        LabeledInputData augmentationLabeledInputdata = (LabeledInputData)augmentation;
        List<LabeledInputData> augmentations = new List<LabeledInputData>(new LabeledInputData[] { augmentationLabeledInputdata });

        foreach (Augmenter augmenter in followers)
        {
            IAugmentation clone = augmentation.Clone();
            augmentations.AddRange(augmenter.Process(clone));
        }

        return augmentations;
    }
}
