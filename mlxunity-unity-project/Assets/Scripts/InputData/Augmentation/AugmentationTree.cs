﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: make own class or other separated solution for mousegesture
public class AugmentationTree
{
    protected List<Augmenter> rootLayer = new List<Augmenter>();

    public List<LabeledInputData> Augment(LabeledInputData inputData)
    {
        LabeledInputData augmentation = inputData.GetAugmentation();
        List<LabeledInputData> augmentations = new List<LabeledInputData>();

        foreach (Augmenter augmenter in rootLayer)
            augmentations.AddRange(augmenter.Process((IAugmentation)augmentation));

        return augmentations;
    }
}
