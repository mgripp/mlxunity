﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShearVertically2 : Augmenter
{
    public ShearVertically2(List<Augmenter> followers) : base(followers) => Initialize();
    public ShearVertically2(Augmenter follower) : base(follower) => Initialize();
    public ShearVertically2() : base() => Initialize();

    void Initialize() => this.name = "Shear Vertically2";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).ShearVertically();
        return base.Process(augmentation);
    }
}
