﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShearHorizontally2 : Augmenter
{
    public ShearHorizontally2(List<Augmenter> followers) : base(followers) => Initialize();
    public ShearHorizontally2(Augmenter follower) : base(follower) => Initialize();
    public ShearHorizontally2() : base() => Initialize();

    void Initialize() => this.name = "Shear Horizontally";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).ShearHorizontally();
        return base.Process(augmentation);
    }
}
