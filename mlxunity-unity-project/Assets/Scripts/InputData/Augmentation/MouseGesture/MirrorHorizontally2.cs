﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MirrorHorizontally2 : Augmenter
{
    public MirrorHorizontally2(List<Augmenter> followers) : base(followers) => Initialize();
    public MirrorHorizontally2(Augmenter follower) : base(follower) => Initialize();
    public MirrorHorizontally2() : base() => Initialize();

    void Initialize() => this.name = "Mirror Horizontally";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).MirrorHorizontally();
        return base.Process(augmentation);
    }
}
