﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class InvertDirection2 : Augmenter
{
    public InvertDirection2(List<Augmenter> followers) : base(followers) => Initialize();
    public InvertDirection2(Augmenter follower) : base(follower) => Initialize();
    public InvertDirection2() : base() => Initialize();

    void Initialize() => this.name = "Invert Direction";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).InvertDirection();
        return base.Process(augmentation);
    }
}
