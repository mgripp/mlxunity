﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAugmentationTree : AugmentationTree
{
    public MouseAugmentationTree()
    {
        AddNoise2 noise = new AddNoise2(1f);
        InvertDirection2 invertDirection = new InvertDirection2(noise);

        ShearHorizontally2 shearH = new ShearHorizontally2(new List<Augmenter> {
            noise,
            invertDirection,
        });
        ShearVertically2 shearV = new ShearVertically2(new List<Augmenter> {
            noise,
            invertDirection,
        });

        Scale2 scaleBigger = new Scale2(2f, new List<Augmenter> {
            shearV,
            shearH,
            noise,
            invertDirection,
        });
        Scale2 scaleSmaller = new Scale2(.5f, new List<Augmenter> {
            shearV,
            shearH,
            noise,
            invertDirection,
        });

        MirrorHorizontally2 mirrorH = new MirrorHorizontally2(new List<Augmenter> {
            shearV,
            shearH,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        });
        MirrorVertically2 mirrorV = new MirrorVertically2(new List<Augmenter> {
            shearV,
            shearH,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        });

        rootLayer.Add(noise);
        rootLayer.Add(invertDirection);
        rootLayer.Add(shearH);
        rootLayer.Add(shearV);
        rootLayer.Add(mirrorH);
        rootLayer.Add(mirrorV);
        rootLayer.Add(scaleBigger);
        rootLayer.Add(scaleSmaller);
        rootLayer.Add(new Rotate2(90f, new List<Augmenter> {
            mirrorH,
            mirrorV,
            shearV,
            shearH,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        }));
        rootLayer.Add(new Rotate2(180f, new List<Augmenter> {
            mirrorH,
            mirrorV,
            shearV,
            shearH,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        }));
        rootLayer.Add(new Rotate2(270f, new List<Augmenter> {
            mirrorH,
            mirrorV,
            shearV,
            shearH,
            scaleBigger,
            scaleSmaller,
            invertDirection,
            noise,
            invertDirection,
        }));
    }
}
