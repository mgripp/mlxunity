﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale2 : Augmenter
{
    float scale = 0;

    public Scale2(float scale, List<Augmenter> followers) : base(followers) => Initialize(scale);
    public Scale2(float scale, Augmenter follower) : base(follower) => Initialize(scale);
    public Scale2(float scale) : base() => Initialize(scale);

    void Initialize(float scale)
    {
        this.name = "Scale";
        this.scale = scale;
    }

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).Scale(scale);
        return base.Process(augmentation);
    }
}
