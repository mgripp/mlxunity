﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MirrorVertically2 : Augmenter
{
    public MirrorVertically2(List<Augmenter> followers) : base(followers) => Initialize();
    public MirrorVertically2(Augmenter follower) : base(follower) => Initialize();
    public MirrorVertically2() : base() => Initialize();

    void Initialize() => this.name = "Mirror Vertically";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).MirrorVertically();
        return base.Process(augmentation);
    }
}
