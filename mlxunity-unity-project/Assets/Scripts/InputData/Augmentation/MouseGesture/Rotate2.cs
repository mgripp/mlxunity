﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate2 : Augmenter
{
    float degrees = 0;

    public Rotate2(float degrees, List<Augmenter> followers) : base(followers) => Initialize(degrees);
    public Rotate2(float degrees, Augmenter follower) : base(follower) => Initialize(degrees);
    public Rotate2(float degrees) : base() => Initialize(degrees);

    void Initialize(float degrees)
    {
        this.name = "Rotate";
        this.degrees = degrees;
    }

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).Rotate(Vector3.forward * degrees);
        return base.Process(augmentation);
    }
}
