﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddNoise2 : Augmenter
{
    float amount = 0;

    public AddNoise2(List<Augmenter> followers, float amount) : base(followers) => Initialize(amount);
    public AddNoise2(Augmenter follower, float amount) : base(follower) => Initialize(amount);
    public AddNoise2(float amount) : base() => Initialize(amount);

    void Initialize(float amount)
    {
        this.name = "Add Noise";
        this.amount = amount;
    }

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((MouseGestureAugmentation)augmentation).AddNoise(amount);
        return base.Process(augmentation);
    }
}
