﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class InvertDirection3 : Augmenter
{
    public InvertDirection3(List<Augmenter> followers) : base(followers) => Initialize();
    public InvertDirection3(Augmenter follower) : base(follower) => Initialize();
    public InvertDirection3() : base() => Initialize();

    void Initialize() => this.name = "Invert Direction";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).InvertDirection();
        return base.Process(augmentation);
    }
}
