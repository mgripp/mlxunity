﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate3 : Augmenter
{
    Vector3 degrees = Vector3.zero;

    public Rotate3(Vector3 degrees, List<Augmenter> followers) : base(followers) => Initialize(degrees);
    public Rotate3(Vector3 degrees, Augmenter follower) : base(follower) => Initialize(degrees);
    public Rotate3(Vector3 degrees) : base() => Initialize(degrees);

    void Initialize(Vector3 degrees)
    {
        this.name = "Rotate";
        this.degrees = degrees;
    }

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).Rotate(degrees);
        return base.Process(augmentation);
    }
}
