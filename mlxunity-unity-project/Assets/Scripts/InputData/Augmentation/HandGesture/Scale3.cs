﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale3 : Augmenter
{
    float scale = 0;

    public Scale3(float scale, List<Augmenter> followers) : base(followers) => Initialize(scale);
    public Scale3(float scale, Augmenter follower) : base(follower) => Initialize(scale);
    public Scale3(float scale) : base() => Initialize(scale);

    void Initialize(float scale)
    {
        this.name = "Scale";
        this.scale = scale;
    }

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).Scale(scale);
        return base.Process(augmentation);
    }
}
