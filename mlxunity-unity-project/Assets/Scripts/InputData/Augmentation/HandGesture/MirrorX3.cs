﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MirrorX3 : Augmenter
{
    public MirrorX3(List<Augmenter> followers) : base(followers) => Initialize();
    public MirrorX3(Augmenter follower) : base(follower) => Initialize();
    public MirrorX3() : base() => Initialize();

    void Initialize() => this.name = "Mirror X";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).MirrorX();
        return base.Process(augmentation);
    }
}
