﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShearY3 : Augmenter
{
    public ShearY3(List<Augmenter> followers) : base(followers) => Initialize();
    public ShearY3(Augmenter follower) : base(follower) => Initialize();
    public ShearY3() : base() => Initialize();

    void Initialize() => this.name = "Shear Y";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).ShearY();
        return base.Process(augmentation);
    }
}
