﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShearZ3 : Augmenter
{
    public ShearZ3(List<Augmenter> followers) : base(followers) => Initialize();
    public ShearZ3(Augmenter follower) : base(follower) => Initialize();
    public ShearZ3() : base() => Initialize();

    void Initialize() => this.name = "Shear Z";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).ShearZ();
        return base.Process(augmentation);
    }
}
