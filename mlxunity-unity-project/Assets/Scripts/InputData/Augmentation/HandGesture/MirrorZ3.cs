﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MirrorZ3 : Augmenter
{
    public MirrorZ3(List<Augmenter> followers) : base(followers) => Initialize();
    public MirrorZ3(Augmenter follower) : base(follower) => Initialize();
    public MirrorZ3() : base() => Initialize();

    void Initialize() => this.name = "Mirror Z";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).MirrorZ();
        return base.Process(augmentation);
    }
}
