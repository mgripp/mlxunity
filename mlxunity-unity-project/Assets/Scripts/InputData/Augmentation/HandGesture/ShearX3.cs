﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShearX3 : Augmenter
{
    public ShearX3(List<Augmenter> followers) : base(followers) => Initialize();
    public ShearX3(Augmenter follower) : base(follower) => Initialize();
    public ShearX3() : base() => Initialize();

    void Initialize() => this.name = "Shear X";

    public override List<LabeledInputData> Process(IAugmentation augmentation)
    {
        ((HandGestureAugmentation)augmentation).ShearX();
        return base.Process(augmentation);
    }
}
