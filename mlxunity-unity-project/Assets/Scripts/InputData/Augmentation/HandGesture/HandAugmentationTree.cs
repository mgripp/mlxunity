﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandAugmentationTree : AugmentationTree
{
    public HandAugmentationTree()
    {
        AddNoise3 noise = new AddNoise3(2f);
        InvertDirection3 invertDirection = new InvertDirection3(noise);

        ShearX3 shearX = new ShearX3(new List<Augmenter> {
            noise,
            invertDirection,
        });
        ShearY3 shearY = new ShearY3(new List<Augmenter> {
            noise,
            invertDirection,
        });
        ShearZ3 shearZ = new ShearZ3(new List<Augmenter> {
            noise,
            invertDirection,
        });

        Scale3 scaleBigger = new Scale3(2f, new List<Augmenter> {
            shearY,
            shearX,
            noise,
            invertDirection,
        });
        Scale3 scaleSmaller = new Scale3(.5f, new List<Augmenter> {
            shearY,
            shearX,
            noise,
            invertDirection,
        });

        MirrorX3 mirrorX = new MirrorX3(new List<Augmenter> {
            shearY,
            shearX,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        });
        MirrorZ3 mirrorZ = new MirrorZ3(new List<Augmenter> {
            shearY,
            shearX,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        });

        rootLayer.Add(noise);
        rootLayer.Add(invertDirection);
        rootLayer.Add(shearX);
        rootLayer.Add(shearY);
        rootLayer.Add(mirrorX);
        rootLayer.Add(mirrorZ);
        rootLayer.Add(scaleBigger);
        rootLayer.Add(scaleSmaller);
        rootLayer.Add(new Rotate3(Vector3.up * 90f, new List<Augmenter> {
            mirrorX,
            mirrorZ,
            shearY,
            shearX,
            shearZ,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        }));
        rootLayer.Add(new Rotate3(Vector3.up * 180f, new List<Augmenter> {
            mirrorX,
            mirrorZ,
            shearY,
            shearX,
            shearZ,
            scaleBigger,
            scaleSmaller,
            noise,
            invertDirection,
        }));
        rootLayer.Add(new Rotate3(Vector3.up * 370f, new List<Augmenter> {
            mirrorX,
            mirrorZ,
            shearY,
            shearX,
            shearZ,
            scaleBigger,
            scaleSmaller,
            invertDirection,
            noise,
            invertDirection,
        }));
    }
}
