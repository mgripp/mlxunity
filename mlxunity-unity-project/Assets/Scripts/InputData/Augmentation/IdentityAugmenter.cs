﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdentityAugmenter : Augmenter
{
    public IdentityAugmenter(List<Augmenter> followers) : base(followers) => Initialize();
    public IdentityAugmenter(Augmenter follower) : base(follower) => Initialize();
    public IdentityAugmenter() : base() => Initialize();
    void Initialize() => this.name = "Identity";
    public override List<LabeledInputData> Process(IAugmentation augmentation) => base.Process(augmentation);
}
