﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public interface IAugmentation
{
    IClass LabelClass { get; }
    int ClassIndex { get; }
    string ClassName { get; }
    bool Normalized { get; }
    string LastAugmentation { set; get; }
    void Normalize();
    IAugmentation Clone();
    JSONObject ToJSON();
    void LoadFromJSON(JSONNode json);
    void ToggleInverted();
}
