﻿using System;

public class RelativeClass : IClass
{
    Class original;
    Class reference
    {
        get
        {
            if (Inverted && original.inverse != null)
                return original.inverse;
            return original;
        }
    }
    public bool Inverted { get; private set; } = false;

    public RelativeClass(Class original) => this.original = original;

    public int Index => reference.Index;
    public string Name => reference.Name;
    public string RecordPath => reference.RecordPath;
    public void ToggleInverted() => Inverted = !Inverted;
    public void CopyFrom(IClass other) => throw new NotImplementedException();
}
