﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    public class GestureTest
    {
        [Test]
        public void TestTrajectoryLengthIsWithinBounds()
        {
            MouseGesture gesture = new MouseGesture(new List<Vector2> { new Vector2(), new Vector2(), new Vector2() });
            foreach (IDataArray vectorArray in gesture.Trajectory.Data)
                Assert.AreEqual(3, vectorArray.Count);
        }

        [Test]
        public void TestGetAugmentation()
        {
            MouseGesture gesture = TestExamples.gestureExample1;
            LabeledInputData augmentation = gesture.GetAugmentation();
            Assert.AreEqual(gesture, augmentation);
        }

        [Test]
        public void TestClone()
        {
            MouseGesture clone = TestExamples.gestureExample1.Clone();
            Assert.AreEqual(TestExamples.gestureExample1, clone);
        }

        [Test]
        public void TestLoadFromJSONViaConstructor()
        {
            Assert.AreEqual(TestExamples.gestureExample1, new MouseGesture(TestExamples.gestureJSONExample1));
            Assert.AreEqual(TestExamples.gestureExample2, new MouseGesture(TestExamples.gestureJSONExample2));
        }

        [Test]
        public void TestLoadFromJSON()
        {
            MouseGesture gesture = new MouseGesture();
            gesture.LoadFromJSON(TestExamples.gestureJSONNodeExample1);
            Assert.AreEqual(TestExamples.gestureExample1, gesture);

            gesture = new MouseGesture();
            gesture.LoadFromJSON(TestExamples.gestureJSONNodeExample2);
            Assert.AreEqual(TestExamples.gestureExample2, gesture);
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }   
}
