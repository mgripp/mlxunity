﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using TestExtensions;
using SimpleJSON;
using UnityEngine.TestTools;

namespace Tests
{
    public class Vector2DataArrayTests
    {
        static public bool ValidateNormalized(Vector2DataArray array)
        {
            foreach (Vector2 vector in array.Vectors)
                if (vector.x < -1f || vector.x > 1f || vector.y < -1f || vector.y > 1f)
                    return false;
            return true;
        }

        [Test]
        public void TestEquals()
        {
            Vector2DataArray array1 = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(0, 1f),
                    new Vector2(1f, 0),
                    new Vector2(1f, 1f),
                    new Vector2(1.2f, -2.2f),
                    new Vector2(10.99f, 1.8f),
                }
            );

            Vector2DataArray array1Clone = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(0, 1f),
                    new Vector2(1f, 0),
                    new Vector2(1f, 1f),
                    new Vector2(1.2f, -2.2f),
                    new Vector2(10.99f, 1.8f),
                }
            );

            Vector2DataArray array1Close = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(0, 1f - Mathf.Epsilon),
                    new Vector2(1f + Mathf.Epsilon, 0),
                    new Vector2(1f, 1f - Mathf.Epsilon),
                    new Vector2(1.2f - Mathf.Epsilon, -2.2f),
                    new Vector2(10.99f + Mathf.Epsilon, 1.8f)
                }
            );

            Vector2DataArray array2 = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1f, 0),
                    new Vector2(3.5f, 2f),
                    new Vector2(6.993f, 1.029f),
                    new Vector2(0, 1f),
                    new Vector2(),
                }
            );

            Vector2DataArray array3 = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(0, 1f),
                    new Vector2(1f, 0),
                    new Vector2(1f, 1f),
                }
            );

            Vector2DataArray array4 = new Vector2DataArray(
                vectors: new List<Vector2>()
            );

            // Are equal to themselves
            Assert.AreEqual(array1, array1);
            Assert.AreEqual(array1Clone, array1Clone);
            Assert.AreEqual(array1Close, array1Close);
            Assert.AreEqual(array2, array2);
            Assert.AreEqual(array3, array3);
            Assert.AreEqual(array4, array4);

            // Equal to each other?
            Assert.AreEqual(array1, array1Clone);
            Assert.AreEqual(array1, array1Close);
            Assert.AreNotEqual(array1, array2);
            Assert.AreNotEqual(array1, array3);
            Assert.AreNotEqual(array1, array4);
        }

        [Test]
        public void TestClone()
        {
            Assert.AreEqual(TestExamples.vector2DataArrayExample1, TestExamples.vector2DataArrayExample1.Clone());
            Assert.AreEqual(TestExamples.vector2DataArrayExample2, TestExamples.vector2DataArrayExample2.Clone());
        }

        [Test]
        public void TestValidateNormalized()
        {
            Vector2DataArray array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1List);
            Assert.IsFalse(ValidateNormalized(array));

            array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1NormalizedList);
            Assert.IsTrue(ValidateNormalized(array));
        }

        [Test]
        public void TestClampVectors()
        {
            Vector2DataArray array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1List);
            Vector2DataArray expected = new Vector2DataArray(vectors: new List<Vector2>
                {
                    new Vector2(-1f, 1f),
                    new Vector2(-1f, 1f),
                    new Vector2(1f, 0),
                    new Vector2(0, 1f),
                    new Vector2(1f, -1f),
                    new Vector2(0f, 1f),
                }
            );
            array.ClampVectors(1f);
            Assert.AreEqual(expected, array);
        }

        [Test]
        public void TestNormalize()
        {
            Vector2DataArray array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1List);
            Vector2DataArray arrayNormalized = new Vector2DataArray(vectors: TestExamples.trajectoryExample1NormalizedList);
            array.Normalize();

            Assert.IsTrue(array.Normalized);
            Assert.IsTrue(ValidateNormalized(array));
            Assert.AreEqual(arrayNormalized, array);
        }

        [Test]
        public void TestDenormalize()
        {
            Vector2DataArray arrayNormalized = new Vector2DataArray(vectors: TestExamples.trajectoryExample1NormalizedList);
            Vector2DataArray array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1List);

            // The coordinates of value 20 will be cropped to Vector2DataArray.NormalizationReference (10)
            array.ClampVectors(Vector2DataArray.NormalizationReference);

            arrayNormalized.Denormalize();

            Assert.IsFalse(arrayNormalized.Normalized);
            Assert.IsFalse(ValidateNormalized(arrayNormalized));
            Assert.AreEqual(array, arrayNormalized);
        }

        [Test]
        public void TestIsZeroPadded()
        {
            Vector2DataArray array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1List);
            Assert.IsFalse(array.IsZeroPadded());

            array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1NormalizedList);
            Assert.IsFalse(array.IsZeroPadded());

            array = new Vector2DataArray(vectors: TestExamples.trajectoryExample1ZeroPaddedList);
            Assert.IsTrue(array.IsZeroPadded());
        }

        [Test]
        public void TestZeroPaddedVectorsCount()
        {
            Vector2DataArray array = new Vector2DataArray(vectors: new List<Vector2> { Vector2.zero }, 3);
            Assert.AreEqual(3, array.ZeroPaddedVectorsCount());

            array = new Vector2DataArray(vectors: new List<Vector2> { Vector2.zero });
            Assert.AreEqual(1, array.ZeroPaddedVectorsCount());

            array = new Vector2DataArray(vectors: new List<Vector2> { Vector2.one, Vector2.zero });
            Assert.AreEqual(1, array.ZeroPaddedVectorsCount());

            array = new Vector2DataArray(vectors: new List<Vector2> { Vector2.zero, Vector2.zero });
            Assert.AreEqual(2, array.ZeroPaddedVectorsCount());

            array = new Vector2DataArray(vectors: new List<Vector2> { Vector2.one, Vector2.zero, Vector2.zero });
            Assert.AreEqual(2, array.ZeroPaddedVectorsCount());

            array = new Vector2DataArray(vectors: new List<Vector2> { Vector2.one, Vector2.zero, Vector2.one, Vector2.zero });
            Assert.AreEqual(1, array.ZeroPaddedVectorsCount());
        }

        [Test]
        public void TestScaleToLength()
        {
            Vector2DataArray array = new Vector2DataArray(vectors: new List<Vector2> { Vector2.one });
            Assert.IsFalse(array.Scaled);
            Assert.AreEqual(1, array.ActualCount);

            //Try different trajectories for correct scaling

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    Vector2.one,
                    Vector2.one,
                    Vector2.one
                }
            );
            Vector2DataArray expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    Vector2.one * 1.5f,
                    Vector2.one * 1.5f
                }
            );
            array.ScaleToLength(2);

            Assert.IsTrue(array.Scaled);
            Assert.AreEqual(expected, array);
            Assert.IsFalse(array.IsZeroPadded());
            Assert.AreEqual(2, array.ActualCount);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    Vector2.one
                }
            );
            expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    Vector2.one / 3,
                    Vector2.one / 3,
                    Vector2.one / 3
                }
            );
            array.ScaleToLength(3);
            Assert.IsTrue(array.Scaled);
            Assert.AreEqual(expected, array);
            Assert.IsFalse(array.IsZeroPadded());
            Assert.AreEqual(3, array.ActualCount);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, 0),
                new Vector2(0, -1),
                new Vector2(-1, -1)
                }
            );
            expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, -0.5f),
                    new Vector2(-1, -1.5f),
                }
            );

            array.ScaleToLength(2);
            Assert.AreEqual(expected, array);
            Assert.AreEqual(false, array.IsZeroPadded());
            Assert.AreEqual(2, array.ActualCount);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, 0),
                    new Vector2(0, -1),
                    new Vector2(1, 0),
                    new Vector2(-1, -1),
                    new Vector2(1, 0),
                    new Vector2(0, -1),
                    new Vector2(1, 0),
                    new Vector2(-1, -1)
                }
            );
            expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1f, -2f),
                    new Vector2(1f, -2f),
                }
            );

            array.ScaleToLength(2);
            Assert.AreEqual(expected, array);
            Assert.AreEqual(false, array.IsZeroPadded());
            // sizes will not be equal since parts of the original trajectory will be lost in scaling
            Assert.AreEqual(2, array.ActualCount);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, 0),
                new Vector2(0, -1),
                new Vector2(-1, -1)
                }
            );
            expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(0, -2)
                }
            );
            array.ScaleToLength(1);
            Assert.AreEqual(expected, array);
            Assert.AreEqual(false, array.IsZeroPadded());
            // sizes will not be equal since parts of the original trajectory will be lost in scaling
            Assert.AreEqual(1, array.ActualCount);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, 0),
                    new Vector2(0, -1),
                    new Vector2(-1, -1)
                }
            );
            expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(0.75f, 0),
                    new Vector2(0.25f, -0.5f),
                    new Vector2(-0.25f, -0.75f),
                    new Vector2(-0.75f, -0.75f)
                }
            );

            array.ScaleToLength(4);
            Assert.AreEqual(expected, array);
            Assert.AreEqual(false, array.IsZeroPadded());
            Assert.AreEqual(4, array.ActualCount);
        }

        [Test]
        public void TestSetToLength()
        {
            Vector2DataArray array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, 0),
                    new Vector2(0, -1),
                    new Vector2(-1, -1)
                }
            );
            Vector2DataArray expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, 0),
                    new Vector2(0, -1)
                }
            );

            array.SetToLength(2);
            Assert.AreEqual(expected, array);
            Assert.IsFalse(array.IsZeroPadded());
            Assert.AreEqual(2, array.ActualCount);

            array.SetToLength(1);

            expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1, 0)
                }
            );

            Assert.AreEqual(expected, array);
            Assert.AreEqual(false, array.IsZeroPadded());
            Assert.AreEqual(1, array.ActualCount);

            array.SetToLength(5);
            expected = new Vector2DataArray(
                vectors: new List<Vector2> {
                new Vector2(1, 0),
                    Vector2.zero,
                    Vector2.zero,
                    Vector2.zero,
                    Vector2.zero
                }
            );

            Assert.AreEqual(expected, array);
            Assert.AreEqual(true, array.IsZeroPadded());
            Assert.AreEqual(1, array.ActualCount);
        }

        [Test]
        public void TestConstructor()
        {
            Vector2DataArray array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1.1f, 1.2f),
                    new Vector2(1.3f, 1.4f)
                },
                scale: true
            );
            Assert.IsFalse(array.Normalized);
            Assert.IsTrue(array.Scaled);
            Assert.AreEqual(2, array.Vectors.Count);
            Assert.AreEqual(2, array.ActualCount);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1.1f, 1.2f),
                    new Vector2(1.3f, 1.4f),
                    new Vector2()
                },
                scale: false
            );
            Assert.IsFalse(array.Normalized);
            Assert.IsFalse(array.Scaled);
            Assert.AreEqual(3, array.Vectors.Count);
            Assert.AreEqual(2, array.ActualCount);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                    new Vector2(1.1f, 1.2f),
                    new Vector2(1.3f, 1.4f),
                },
                countMax: 3,
                scale: false
            );
            Assert.IsFalse(array.Normalized);
            Assert.IsFalse(array.Scaled);
            Assert.AreEqual(3, array.Vectors.Count);
            Assert.AreEqual(2, array.ActualCount);
        }

        [Test]
        public void TestToJSON()
        {
            AssertExtensions.AreEqual((JSONArray)TestExamples.vector2DataArrayJSONNodeExample2.AsArray[0],
                (JSONArray)TestExamples.vector2DataArrayExample2.ToJSON());
            AssertExtensions.AreNotEqual((JSONArray)TestExamples.vector2DataArrayJSONNodeExample1.AsArray[0],
                (JSONArray)TestExamples.vector2DataArrayExample2.ToJSON());
        }

        [Test]
        public void TestLoadFromJSON()
        {
            Vector2DataArray vectorArray = new Vector2DataArray();
            vectorArray.LoadFromJSON(TestExamples.vector2DataArrayJSONNodeExample1.AsArray[0], false, false);
            Assert.AreEqual(TestExamples.vector2DataArrayExample1, vectorArray);
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
