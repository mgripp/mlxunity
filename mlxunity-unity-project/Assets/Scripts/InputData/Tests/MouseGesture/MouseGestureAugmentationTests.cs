﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TestExtensions;
using DebugExtensions;
using NUnit.Framework;

namespace Tests
{
    public class GestureAugmentationTests
    {
        [Test]
        public void TestToggleInverted()
        {
            MouseGesture gesture;
            MouseGestureAugmentation augmentation;

            gesture = new MouseGesture(
                new List<Vector2> { new Vector2() },
                classIndex: 0
            );

            augmentation = (MouseGestureAugmentation)gesture.GetAugmentation();
            Assert.AreEqual("Circle CCW", gesture.ClassName);
            Assert.AreEqual("Circle CCW", augmentation.ClassName);
            augmentation.ToggleInverted();
            Assert.AreEqual("Circle CCW", gesture.ClassName);
            Assert.AreEqual("Circle CW", augmentation.ClassName);
        }

        [Test]
        public void TestRotate()
        {
            MouseGesture gesture;
            MouseGestureAugmentation rotated;
            MouseGestureAugmentation rotatedMinus90;
            Vector2DataArray expected;

            gesture = new MouseGesture(
                new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() }
            );

            // Rotate 90°
            rotated = (MouseGestureAugmentation)gesture.GetAugmentation();
            float[] rotatedSize = new float[] { rotated.Size[1], rotated.Size[0] };
            rotated.Rotate(Vector3.forward * 90f);
            expected = new Vector2DataArray(
                new List<Vector2> {
                    new Vector2(0, 1f),
                    new Vector2(2f, 1.5f),
                    new Vector2()
                }
            );
            Assert.AreEqual(expected, rotated.Trajectory.Data[0]);
            AssertExtensions.AreEqual(rotatedSize, rotated.Size);

            // Rotate -90°
            rotatedMinus90 = (MouseGestureAugmentation)gesture.GetAugmentation();
            rotatedSize = new float[] { rotatedMinus90.Size[1], rotatedMinus90.Size[0] };
            rotatedMinus90.Rotate(Vector3.forward * - 90f);

            expected = new Vector2DataArray(
                new List<Vector2> {
                    new Vector2(0, -1f),
                    new Vector2(-2f, -1.5f),
                    new Vector2()
                }
            );
            Assert.AreEqual(expected, rotatedMinus90.Trajectory.Data[0]);
            Assert.AreEqual(rotatedSize, rotatedMinus90.Size);

            // Rotate 270°
            rotated = (MouseGestureAugmentation)gesture.GetAugmentation();
            rotatedSize = new float[] { rotated.Size[1], rotated.Size[0] };
            rotated.Rotate(Vector3.forward * 270f);

            Assert.AreEqual(rotatedMinus90.Trajectory.Data[0], rotated.Trajectory.Data[0]);
            Assert.AreEqual(rotatedSize, rotatedMinus90.Size);
        }

        // TODO: Finish when Augmenters are implemented
        //[Test]
        //public void TestMirrorVertically()
        //{
        //    Gesture gesture;
        //    Gesture mirrored;

        //    gesture = new Gesture(
        //        new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() },
        //        trajectoryCountMin: 1,
        //        trajectoryCountMax: 3,
        //        classIndex: ClassesConfig.Instance.ClassNameToClass["Circle CW"].Index,
        //        normalized: false,
        //        scaled: false,
        //        augmented: false,
        //        augmentations: null
        //    );

        //    mirrored = gesture.MirrorVertically();

        //    Assert.AreEqual(gesture.Size, mirrored.Size);

        //    Assert.IsTrue(mirrored.Augmented);
        //    Assert.IsNull(mirrored.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.MirrorVertically }, mirrored.AugmentationHistory);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(mirrored.Trajectory, new List<Vector2> {
        //        new Vector2(-1f, 0),
        //        new Vector2(-1.5f, -2f),
        //        new Vector2()
        //    }));

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Circle CCW"].Index);
        //    mirrored = gesture.MirrorVertically();
        //    Assert.AreEqual("Circle CW", mirrored.ClassName);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Spiral CW"].Index);
        //    mirrored = gesture.MirrorVertically();
        //    Assert.AreEqual("Spiral CCW", mirrored.ClassName);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Spiral CCW"].Index);
        //    mirrored = gesture.MirrorVertically();
        //    Assert.AreEqual("Spiral CW", mirrored.ClassName);
        //}

        //[Test]
        //public void TestMirrorHorizontally()
        //{
        //    Gesture gesture;
        //    Gesture mirrored;

        //    gesture = new Gesture(
        //        new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() },
        //        trajectoryCountMin: 1,
        //        trajectoryCountMax: 3,
        //        classIndex: ClassesConfig.Instance.ClassNameToClass["Circle CW"].Index,
        //        normalized: false,
        //        scaled: false,
        //        augmented: false,
        //        augmentations: null
        //    );

        //    mirrored = gesture.MirrorHorizontally();

        //    Assert.IsTrue(mirrored.Augmented);
        //    Assert.IsNull(mirrored.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.MirrorHorizontally }, mirrored.AugmentationHistory);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(mirrored.Trajectory, new List<Vector2> {
        //        new Vector2(1f, 0),
        //        new Vector2(1.5f, 2f),
        //        new Vector2()
        //    }));
        //    Assert.AreEqual(gesture.Size, mirrored.Size);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Circle CCW"].Index);
        //    mirrored = gesture.MirrorHorizontally();
        //    Assert.AreEqual("Circle CW", mirrored.ClassName);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Spiral CW"].Index);
        //    mirrored = gesture.MirrorHorizontally();
        //    Assert.AreEqual("Spiral CCW", mirrored.ClassName);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Spiral CCW"].Index);
        //    mirrored = gesture.MirrorHorizontally();
        //    Assert.AreEqual("Spiral CW", mirrored.ClassName);
        //}

        //[Test]
        //public void TestShearHorizontally()
        //{
        //    Gesture gesture;
        //    Gesture sheared;

        //    gesture = new Gesture(
        //        new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() },
        //        trajectoryCountMin: 1,
        //        trajectoryCountMax: 3,
        //        classIndex: 0,
        //        normalized: false,
        //        scaled: false,
        //        augmented: false,
        //        augmentations: null
        //    );

        //    sheared = gesture.ShearHorizontally();

        //    Assert.IsTrue(sheared.Augmented);
        //    Assert.IsNull(sheared.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.ShearHorizontally }, sheared.AugmentationHistory);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(sheared.Trajectory, new List<Vector2> {
        //        new Vector2(1f, 0),
        //        new Vector2(-0.5f, -2f),
        //        new Vector2()
        //    }));
        //}

        //[Test]
        //public void TestShearVertically()
        //{
        //    Gesture gesture;
        //    Gesture sheared;

        //    gesture = new Gesture(
        //        new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() },
        //        trajectoryCountMin: 1,
        //        trajectoryCountMax: 3,
        //        classIndex: 0,
        //        normalized: false,
        //        scaled: false,
        //        augmented: false,
        //        augmentations: null
        //    );

        //    sheared = gesture.ShearVertically();

        //    Assert.IsTrue(sheared.Augmented);
        //    Assert.IsNull(sheared.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.ShearVertically }, sheared.AugmentationHistory);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(sheared.Trajectory, new List<Vector2> {
        //        new Vector2(1f, 1f),
        //        new Vector2(1.5f, -0.5f),
        //        new Vector2()
        //    }));
        //}

        //[Test]
        //public void TestScale()
        //{
        //    Gesture gesture;
        //    Gesture scaled;

        //    gesture = new Gesture(
        //        new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() },
        //        trajectoryCountMin: 1,
        //        trajectoryCountMax: 3,
        //        classIndex: 0,
        //        normalized: false,
        //        scaled: false,
        //        augmented: false,
        //        augmentations: null
        //    );

        //    scaled = gesture.Scale(2f);

        //    Assert.IsTrue(scaled.Augmented);
        //    Assert.IsNull(scaled.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.Scale }, scaled.AugmentationHistory);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(scaled.Trajectory, new List<Vector2> {
        //        new Vector2(2f, 0),
        //        new Vector2(3f, -4f),
        //        new Vector2()
        //    }));
        //    Assert.AreEqual(2f * gesture.Size, scaled.Size);

        //    scaled = gesture.Scale(0.5f);

        //    Assert.IsTrue(scaled.Augmented);
        //    Assert.IsNull(scaled.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.Scale }, scaled.AugmentationHistory);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(scaled.Trajectory, new List<Vector2> {
        //        new Vector2(0.5f, 0),
        //        new Vector2(0.75f, -1f),
        //        new Vector2()
        //    }));
        //    Assert.AreEqual(0.5f * gesture.Size, scaled.Size);
        //}

        //[Test]
        //public void TestAddNoise()
        //{
        //    Gesture gesture;
        //    Gesture noised;

        //    gesture = new Gesture(
        //        new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() },
        //        trajectoryCountMin: 1,
        //        trajectoryCountMax: 3,
        //        classIndex: 0,
        //        normalized: false,
        //        scaled: false,
        //        augmented: false,
        //        augmentations: null
        //    );

        //    noised = gesture.AddNoise(0);

        //    Assert.IsTrue(noised.Augmented);
        //    Assert.IsNull(noised.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.AddNoise }, noised.AugmentationHistory);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(noised.Trajectory, gesture.Trajectory));

        //    noised = gesture.AddNoise(1f);

        //    Assert.IsTrue(noised.Augmented);
        //    Assert.IsNull(noised.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.AddNoise }, noised.AugmentationHistory);

        //    for (int i = 0; i < noised.Trajectory.Count; i++)
        //    {
        //        Vector2 vector = gesture.Trajectory[i];
        //        Vector2 noisedVector = noised.Trajectory[i];
        //        float diffX = Mathf.Abs(noisedVector.x - vector.x);
        //        float diffY = Mathf.Abs(noisedVector.y - vector.y);

        //        Assert.LessOrEqual(diffX, 0.5f);
        //        Assert.LessOrEqual(diffY, 0.5f);
        //        Assert.Greater(diffX, 0);
        //        Assert.Greater(diffY, 0);
        //    }
        //}

        //[Test]
        //public void TestInvertDirection()
        //{
        //    Gesture gesture;
        //    Gesture inverted;

        //    gesture = new Gesture(
        //        new List<Vector2> { new Vector2(1f, 0f), new Vector2(1.5f, -2f), new Vector2() },
        //        trajectoryCountMin: 1,
        //        trajectoryCountMax: 3,
        //        classIndex: ClassesConfig.Instance.ClassNameToClass["Circle CW"].Index,
        //        normalized: false,
        //        scaled: false,
        //        augmented: false,
        //        augmentations: null
        //    );

        //    inverted = gesture.InvertDirection();

        //    Assert.IsTrue(inverted.Augmented);
        //    Assert.IsNull(inverted.augmentations);
        //    Assert.AreEqual(new List<Gesture.AugmentationType> { Gesture.AugmentationType.InvertDirection }, inverted.AugmentationHistory);
        //    Assert.AreEqual("Circle CCW", inverted.ClassName);
        //    Assert.IsTrue(Gesture.AreTrajectoriesEqual(inverted.Trajectory, new List<Vector2> {
        //        new Vector2(),
        //        new Vector2(-1.5f, 2f),
        //        new Vector2(-1f, 0)
        //    }));
        //    Assert.AreEqual(gesture.Size, inverted.Size);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Circle CCW"].Index);
        //    inverted = gesture.InvertDirection();
        //    Assert.AreEqual("Circle CW", inverted.ClassName);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Spiral CW"].Index);
        //    inverted = gesture.InvertDirection();
        //    Assert.AreEqual("Spiral CCW", inverted.ClassName);

        //    gesture.SetClassIndex(ClassesConfig.Instance.ClassNameToClass["Spiral CCW"].Index);
        //    inverted = gesture.InvertDirection();
        //    Assert.AreEqual("Spiral CW", inverted.ClassName);
        //}

        [Test]
        public void TestToJSON()
        {
            MouseGesture gesture;
            MouseGestureAugmentation augmentation;

            gesture = TestExamples.gestureExample1;
            augmentation = (MouseGestureAugmentation)gesture.GetAugmentation();

            Assert.AreEqual(TestExamples.gestureJSONExample1AsGesture, new MouseGesture(json: augmentation.ToJSON()));
        }

        [Test]
        public void TestToJSONAfterClassInversion()
        {
            MouseGesture gesture0 = new MouseGesture(
                new List<Vector2> { new Vector2() },
                classIndex: 0
            );
            MouseGesture gesture1 = new MouseGesture(
                new List<Vector2> { new Vector2() },
                classIndex: 1
            );
            MouseGestureAugmentation augmentation = (MouseGestureAugmentation)gesture0.GetAugmentation();
            Debug.Log(augmentation.ClassIndex);
            augmentation.ToggleInverted();
            Debug.Log(augmentation.ClassIndex);
            Debug.Log(augmentation);


            Assert.AreEqual(gesture1, new MouseGesture(json: augmentation.ToJSON()));
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
