﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Tests
{
    public class ClassConfigTests
    {
        [Test]
        public void TestSetClasses()
        {
            ClassesConfig.ClassStruct[] classNames = new ClassesConfig.ClassStruct[] {
                new ClassesConfig.ClassStruct("BLA"),
                new ClassesConfig.ClassStruct(""),
                new ClassesConfig.ClassStruct("bla"),
                new ClassesConfig.ClassStruct("Bla"),
                new ClassesConfig.ClassStruct("Bla Blub")
            };
            string[] fileNames = new string[] {
                "record_bla.json",
                "record_.json",
                "record_bla.json",
                "record_bla.json",
                "record_bla_blub.json",
            };

            List<ClassesConfig.ClassStruct> classNamesSorted = classNames.ToList().OrderBy(o => o.name).ToList();
            List<string> fileNamesSorted = fileNames.ToList<string>();
            fileNamesSorted.Sort();

            ClassesConfig.Instance.SetClasses(classNames);

            for (int i = 0; i < ClassesConfig.Instance.ClassCount; i++)
            {
                Assert.AreEqual(classNamesSorted[i].name, ClassesConfig.Instance.Classes[i].Name);
                Assert.AreEqual(GeneralConfig.Instance.ProjectPath + fileNamesSorted[i], ClassesConfig.Instance.Classes[i].RecordPath);
            }
        }

        [Test]
        public void TestToJSON()
        {
            ClassesConfig classConfig = new ClassesConfig();
            classConfig.SetClasses(new ClassesConfig.ClassStruct[0]);
            string json = "[]";
            Assert.AreEqual(json, classConfig.ToJSON().ToString());

            classConfig.ClearClasses();
            classConfig.SetClasses(new ClassesConfig.ClassStruct[] {
                new ClassesConfig.ClassStruct("Some Class"),
                new ClassesConfig.ClassStruct("Some Other Class"),
            });
            json = "[" +
                '{' + $"\"index\":0,\"name\":\"Some Class\",\"inverse\":null,\"input_json_path\":\"{GeneralConfig.Instance.ProjectPath}record_some_class.json\"" + "}," +
                '{' + $"\"index\":1,\"name\":\"Some Other Class\",\"inverse\":null,\"input_json_path\":\"{GeneralConfig.Instance.ProjectPath}record_some_other_class.json\"" + "}" +
                "]";
            Assert.AreEqual(json, classConfig.ToJSON().ToString());
        }

        [Test]
        public void TestEquals()
        {
            ClassesConfig classConfig1 = new ClassesConfig();
            classConfig1.SetClasses(new ClassesConfig.ClassStruct[] {
                new ClassesConfig.ClassStruct("bla"),
                new ClassesConfig.ClassStruct("blub"),
            });

            ClassesConfig classConfig2 = new ClassesConfig();
            classConfig2.SetClasses(new ClassesConfig.ClassStruct[] {
                new ClassesConfig.ClassStruct("bla"),
                new ClassesConfig.ClassStruct("blub"),
            });

            Assert.AreEqual(classConfig1, classConfig2);

            classConfig2.ClearClasses();
            classConfig2.SetClasses(new ClassesConfig.ClassStruct[] {
                new ClassesConfig.ClassStruct("bla"),
                new ClassesConfig.ClassStruct("bla"),
            });
            Assert.AreNotEqual(classConfig1, classConfig2);
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
