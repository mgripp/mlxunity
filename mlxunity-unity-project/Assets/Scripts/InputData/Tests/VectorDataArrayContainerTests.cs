﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using TestExtensions;
using DebugExtensions;

namespace Tests
{
    public class VectorDataArrayContainerTests
    {
        [Test]
        public void TestUpdateSize2()
        {
            Vector2DataArray array = new Vector2DataArray(
                vectors: new List<Vector2> {
                new Vector2(),
                }
            );
            VectorDataArrayContainer container = new VectorDataArrayContainer(
                vectorArrays: new List<IDataArray> { array }
                );

            Assert.AreEqual(new float[] { 0, 0 }, container.Size);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                new Vector2(),
                new Vector2(-1f, 0)
                }
            );
            container = new VectorDataArrayContainer(
                vectorArrays: new List<IDataArray> { array }
                );

            Assert.AreEqual(new float[] { 1f, 0 }, container.Size);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                new Vector2(),
                new Vector2(1f, 1f),
                new Vector2(1f, 0),
                new Vector2(0, 0.5f),
                new Vector2(-2.5f, -0.5f)
                }
            );
            container = new VectorDataArrayContainer(
                vectorArrays: new List<IDataArray> { array }
                );

            Assert.AreEqual(new float[] { 2.5f, 1.5f }, container.Size);

            array = new Vector2DataArray(
                vectors: new List<Vector2> {
                new Vector2(1, 0),
                new Vector2(0, -1),
                new Vector2(1, 0),
                new Vector2(-1, -1),
                new Vector2(1, 0),
                new Vector2(0, -1),
                new Vector2(1, 0),
                new Vector2(-1, -1)
                }
            );
            container = new VectorDataArrayContainer(
                vectorArrays: new List<IDataArray> { array }
                );

            Assert.AreEqual(new float[] { 3f, 4f }, container.Size);
        }

        [Test]
        public void TestEqualsTrue()
        {
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1, TestExamples.vector2DataArrayContainerExample1);
        }

        [Test]
        public void TestToJSON()
        {
            VectorDataArrayContainer container = new VectorDataArrayContainer();
            container.LoadFromJSON(TestExamples.vector2DataArrayContainerExample1.ToJSON());
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1, container);

            container = new VectorDataArrayContainer();
            container.LoadFromJSON(TestExamples.vector2DataArrayContainerExample2.ToJSON());
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample2, container);
        }

        [Test]
        public void TestLoadFromJSON()
        {
            VectorDataArrayContainer container = new VectorDataArrayContainer();
            container.LoadFromJSON(TestExamples.vector2DataArrayContainerJSONNodeExample1);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1, container);
            AssertExtensions.AreEqual(TestExamples.vector2DataArrayContainerExample1.Size, container.Size);

            container = new VectorDataArrayContainer();
            container.LoadFromJSON(TestExamples.vector2DataArrayContainerJSONNodeExample2);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample2, container);
            AssertExtensions.AreEqual(TestExamples.vector2DataArrayContainerExample2.Size, container.Size);
        }
    }
}
