﻿using NUnit.Framework;

namespace Tests
{
    public class ClassTests
    {
        [Test]
        public void TestToJSON()
        {
            Class c = new Class(0, "bla", "./bla.json");

            string json = "{" +
                "\"index\":0," +
                "\"name\":\"bla\"," +
                "\"inverse\":null," +
                "\"input_json_path\":\"./bla.json\"" +
                "}";

            Assert.AreEqual(json, c.ToJSON().ToString());
        }

        [Test]
        public void TestToJSONWithInverse()
        {
            Class c = new Class(0, "bla", "./bla.json");
            Class inverseC = new Class(0, "blob", "./blob.json");
            c.inverse = inverseC;

            string json = "{" +
                "\"index\":0," +
                "\"name\":\"bla\"," +
                "\"inverse\":\"blob\"," +
                "\"input_json_path\":\"./bla.json\"" +
                "}";

            Assert.AreEqual(json, c.ToJSON().ToString());
        }

        [Test]
        public void TestEquals()
        {
            Class c1 = new Class(0, "bla", "./bla.json");
            Class c2 = new Class(1, "bla", "./blabla.json");

            Assert.AreEqual(c1, c2);

            c2 = new Class(0, "blub", "./bla.json");
            Assert.AreNotEqual(c1, c2);
        }
    }
}
