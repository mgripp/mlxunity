﻿using System.Collections.Generic;
using System.Linq;
using SimpleJSON;

public class ClassesConfig : Configurable
{
    static public ClassesConfig Instance;

    static string ClassNameToJSONPath(string gestureName)
        => $"{GeneralConfig.Instance.ProjectPath}record_{NameToFilename(gestureName)}.json";

    static string NameToFilename(string name)
    {
        string filename = name.ToLower();
        filename = filename.Replace(' ', '_');
        return filename;
    }

    public override string ConfigKey => "classes";

    public Class[] Classes { get; private set; }
    public int ClassCount => Classes.Length;

    Dictionary<string, Class> classNameToClass = new Dictionary<string, Class>();
    public IReadOnlyDictionary<string, Class> ClassNameToClass => classNameToClass;

    public ClassesConfig() : base() => Instance = Instance ?? this;

    public struct ClassStruct
    {
        public string name;
        public string inverseName;

        public ClassStruct(string name, string inverseName = null)
        {
            this.name = name;
            this.inverseName = inverseName;
        }
    }

    public void SetClasses(ClassStruct[] classNames)
    {
        // sort class names since name-index relationship needs to be consistent
        ClassStruct[] classNamesSorted = classNames.ToList().OrderBy(o => o.name).ToArray();

        // save sorted names to classes
        Classes = new Class[classNames.Length];
        for (int i = 0; i < classNamesSorted.Length; i++)
        {
            ClassStruct current = classNamesSorted[i];
            Class newClass = new Class(i, current.name, ClassNameToJSONPath(current.name));
            Classes[i] = newClass;
            classNameToClass[current.name] = newClass;
        }

        // get inverse classes
        for (int i = 0; i < ClassCount; i ++)
        {
            ClassStruct current = classNames[i];
            if (current.inverseName != null)
                Classes[i].inverse = classNameToClass[current.inverseName];
        }
    }

    public void ClearClasses()
    {
        Classes = new Class[0];
        classNameToClass = new Dictionary<string, Class>();
    }

    public override string ToString() => base.ToString();

    public override void LoadFromJSON(JSONNode configJSON)
    {
        int classesCount = configJSON.Count;
        ClassStruct[] classNames = new ClassStruct[classesCount];

        for (int i = 0; i < classesCount; i++)
            classNames[i] = new ClassStruct(configJSON[i]["name"], configJSON[i]["inverse"]);

        SetClasses(classNames);
    }

    override public JSONNode ToJSON()
    {
        JSONArray jsonClasses = new JSONArray();

        foreach (Class c in Classes)
        {
            jsonClasses.Add(c.ToJSON());
        }

        return jsonClasses;
    }

    public ClassesConfig Copy()
    {
        ClassesConfig clone = new ClassesConfig();

        ClassStruct[] classnames = new ClassStruct[ClassCount];
        for (int i = 0; i < ClassCount; i++)
        {
            Class current = Classes[i];
            string inverseName = null;
            if (current.inverse != null)
                inverseName = current.inverse.Name;
            classnames[i] = new ClassStruct(current.Name, inverseName);
        }
        clone.SetClasses(classnames);

        return clone;
    }

    public override bool Equals(object obj)
    {
        ClassesConfig other = (ClassesConfig)obj;

        bool equal = (
            this.Classes.SequenceEqual(other.Classes) &&
            this.ClassNameToClass.SequenceEqual(other.ClassNameToClass)
        );

        return equal;
    }
}
