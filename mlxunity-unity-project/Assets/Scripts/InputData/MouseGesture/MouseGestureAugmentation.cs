﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseGestureAugmentation : MouseGesture, IAugmentation
{
    public enum AugmentationType
    {
        None,
        Rotate,
        ShearHorizontally,
        ShearVertically,
        Scale,
        AddNoise,
        MirrorHorizontally,
        MirrorVertically,
        InvertDirection,
    }

    public enum DrawDirection
    {
        CW,
        CCW,
        None,
    }

    public RelativeClass RelativeClass => (RelativeClass)labelClass;

    List<AugmentationType> augmentationHistory = new List<AugmentationType>();
    public IReadOnlyList<AugmentationType> AugmentationHistory => augmentationHistory.AsReadOnly();
    DrawDirection direction = DrawDirection.None;
    public DrawDirection Direction => direction;

    string lastAugmentation = "None";
    public string LastAugmentation
    {
        get => lastAugmentation;
        set => lastAugmentation = value;
    }

    MouseGesture original;
    // TODO: do not hide original fields
    new public bool Normalized => original.Normalized;
    new public bool Scaled => original.Scaled;

    public MouseGestureAugmentation(
        MouseGestureAugmentation original
    ) : base(
        inputData: original.inputData.Clone()
    )
    {
        AugmentationType[] copy = new AugmentationType[original.augmentationHistory.Count];
        original.augmentationHistory.CopyTo(copy);
        augmentationHistory = new List<AugmentationType>(copy);
        SetOriginal(original);
    } 

    public MouseGestureAugmentation(
        MouseGesture original,
        IDataArrayContainer inputData
    ) : base(
        inputData: inputData
    ) => SetOriginal(original);

    public MouseGestureAugmentation(
        MouseGesture original
    ) : base(
        inputData: original.inputData.Clone()
    ) => SetOriginal(original);

    void SetOriginal(MouseGesture original)
    {
        labelClass = new RelativeClass((Class)original.labelClass);
        this.original = original;
    }

    public void AddAugmentationType(AugmentationType newAugmentationType)
    {
        if (newAugmentationType != AugmentationType.None)
            augmentationHistory.Add(newAugmentationType);
    }

    public void ToggleInverted() => RelativeClass.ToggleInverted();

    public void Rotate(Vector3 degrees)
    {
        Trajectory.Rotate(degrees);
        AddAugmentationType(AugmentationType.Rotate);
    }

    public void MirrorVertically()
    {
        Trajectory.MirrorX();
        AddAugmentationType(AugmentationType.MirrorVertically);
        ToggleInverted();
    }

    public void MirrorHorizontally()
    {
        Trajectory.MirrorY();
        AddAugmentationType(AugmentationType.MirrorHorizontally);
        ToggleInverted();
    }

    public void ShearHorizontally()
    {
        Trajectory.ShearX();
        AddAugmentationType(AugmentationType.ShearHorizontally);
    }

    public void ShearVertically()
    {
        Trajectory.ShearY();
        AddAugmentationType(AugmentationType.ShearVertically);
    }

    public void Scale(float scale)
    {
        Trajectory.Scale(scale);
        AddAugmentationType(AugmentationType.Scale);
    }

    public void AddNoise(float amount)
    {
        Trajectory.AddNoise(amount);
        AddAugmentationType(AugmentationType.AddNoise);
    }

    public void InvertDirection()
    {
        Trajectory.InvertDirection();
        AddAugmentationType(AugmentationType.InvertDirection);
        ToggleInverted();
    }

    public new IAugmentation Clone() => new MouseGestureAugmentation(this.original);

    public override bool Equals(object obj)
    {
        MouseGestureAugmentation other = (MouseGestureAugmentation)obj;
        bool equal = base.Equals(obj) &&
            this.original == other.original;

        return equal;
    }
}
