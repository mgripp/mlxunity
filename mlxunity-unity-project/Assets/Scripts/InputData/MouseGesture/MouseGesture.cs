﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

// TODO still needed? Doesnt add much to base class
public class MouseGesture : LabeledInputData
{
    public VectorDataArrayContainer Trajectory => (VectorDataArrayContainer)inputData;
    public float[] Size => Trajectory.Size;

    public MouseGesture() : base()
    {
        inputData = new VectorDataArrayContainer();
        augmentationTree = new MouseAugmentationTree();
    }
    protected MouseGesture(IDataArrayContainer inputData)
    {
        this.inputData = inputData;
        augmentationTree = new MouseAugmentationTree();
    }

    public MouseGesture(string jsonString)
    {
        inputData = new VectorDataArrayContainer();
        augmentationTree = new MouseAugmentationTree();
        LoadFromJSON(JSON.Parse(jsonString));
    }

    public MouseGesture(JSONNode json)
    {
        inputData = new VectorDataArrayContainer();
        augmentationTree = new MouseAugmentationTree();
        LoadFromJSON(json);
    }

    public MouseGesture(
        List<Vector2> input,
        int countMax = -1,
        bool scale = false,
        int classIndex = 0
    ) : base()
    {
        inputData = new VectorDataArrayContainer(
            vectorArrays: new List<IDataArray> { new Vector2DataArray(vectors: input) },
            countMax: countMax,
            scale: scale
        );
        Initialize();
        augmentationTree = new MouseAugmentationTree();
        SetClass(classIndex);
    }

    public MouseGesture(
        VectorDataArrayContainer input,
        int classIndex = 0
    ) : base()
    {
        inputData = input;
        Initialize();
        augmentationTree = new MouseAugmentationTree();
        SetClass(classIndex);
    }

    public MouseGesture(
        Vector2DataArray input,
        int classIndex = 0
    ) : base()
    {
        inputData = new VectorDataArrayContainer(
            vectorArrays: new List<IDataArray> { input }
        );
        Initialize();
        augmentationTree = new MouseAugmentationTree();
        SetClass(classIndex);
    }

    // TODO: still needed?
    public bool TrajectoryLengthIsWithinBounds(int min, int max) => inputData.Count >= min && inputData.Count <= max;

    override public LabeledInputData GetAugmentation() => new MouseGestureAugmentation(original: this);
    new public MouseGesture Clone() => (MouseGesture)base.Clone();
}
