﻿public interface IClass
{
    int Index { get; }
    string Name { get; }
    string RecordPath { get; }
    void CopyFrom(IClass other);
}
