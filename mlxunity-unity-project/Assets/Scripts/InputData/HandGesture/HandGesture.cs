﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class HandGesture : LabeledInputData
{
    public VectorDataArrayContainer Trajectories => (VectorDataArrayContainer)inputData;
    public float[] Size => Trajectories.Size;

    public HandGesture() : base() { inputData = new VectorDataArrayContainer(); }
    protected HandGesture(IDataArrayContainer inputData) { this.inputData = inputData; }

    public HandGesture(string jsonString)
    {
        inputData = new VectorDataArrayContainer();
        LoadFromJSON(JSON.Parse(jsonString));
        augmentationTree = new HandAugmentationTree();
    }

    public HandGesture(JSONNode json)
    {
        inputData = new VectorDataArrayContainer();
        LoadFromJSON(json);
        augmentationTree = new HandAugmentationTree();
    }

    public HandGesture(
        List<IDataArray> inputData,
        int countMax = -1,
        bool scale = false,
        int classIndex = 0
    ) : base()
    {
        this.inputData = new VectorDataArrayContainer(
            vectorArrays: inputData,
            countMax: countMax,
            scale: scale
        );
        Initialize();
        SetClass(classIndex);
        augmentationTree = new HandAugmentationTree();
    }

    public HandGesture(
        VectorDataArrayContainer inputData,
        int classIndex = 0
    ) : base()
    {
        this.inputData = inputData;
        Initialize();
        SetClass(classIndex);
        augmentationTree = new HandAugmentationTree();
    }

    override public LabeledInputData GetAugmentation() => new HandGestureAugmentation(original: this);
    new public HandGesture Clone() => (HandGesture)base.Clone();

    public override bool Equals(object obj)
    {
        HandGesture other = (HandGesture)obj;

        bool equal = other != null &&
            this.ClassIndex == other.ClassIndex &&
            this.Normalized == other.Normalized &&
            this.Scaled == other.Scaled;

        if (!equal)
            return false;

        // both have augmentations or both have none
        // the exact content of the augmentations does not have to be equal since they might be based on randomness
        // also you can assume equal augmentation (or at least similar) with same inputDatas (will be checked below)
        equal = (this.augmentations == null && other.augmentations == null) ||
            ((this.augmentations != null && other.augmentations != null));

        return equal;
    }
}
