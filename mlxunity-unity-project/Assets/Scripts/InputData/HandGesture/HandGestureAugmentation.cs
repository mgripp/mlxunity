﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandGestureAugmentation : HandGesture, IAugmentation
{
    public enum AugmentationType
    {
        None,
        Rotate,
        ShearX,
        ShearY,
        ShearZ,
        Scale,
        AddNoise,
        MirrorX,
        MirrorZ,
        InvertDirection,
    }

    public enum DrawDirection
    {
        CW,
        CCW,
        None,
    }

    public RelativeClass RelativeClass => (RelativeClass)labelClass;

    public List<AugmentationType> augmentationHistory = new List<AugmentationType>();
    public IReadOnlyList<AugmentationType> AugmentationHistory => augmentationHistory.AsReadOnly();
    DrawDirection direction = DrawDirection.None;
    public DrawDirection Direction => direction;

    string lastAugmentation = "None";
    public string LastAugmentation
    {
        get => lastAugmentation;
        set => lastAugmentation = value;
    }

    HandGesture original;
    // TODO: do not hide original fields
    new public bool Normalized => original.Normalized;
    new public bool Scaled => original.Scaled;

    public HandGestureAugmentation(
        HandGestureAugmentation original
    ) : base(
        inputData: original.inputData.Clone()
    )
    {
        AugmentationType[] copy = new AugmentationType[original.augmentationHistory.Count];
        original.augmentationHistory.CopyTo(copy);
        augmentationHistory = new List<AugmentationType>(copy);
        SetOriginal(original);
    }

    public HandGestureAugmentation(
        HandGesture original,
        IDataArrayContainer inputData
    ) : base(
        inputData: inputData
    ) => SetOriginal(original);

    public HandGestureAugmentation(
        HandGesture original
    ) : base(
        inputData: original.inputData.Clone()
    ) => SetOriginal(original);

    void SetOriginal(HandGesture original)
    {
        labelClass = new RelativeClass((Class)original.labelClass);
        this.original = original;
    }

    public void AddAugmentationType(AugmentationType newAugmentationType)
    {
        if (newAugmentationType != AugmentationType.None)
            augmentationHistory.Add(newAugmentationType);
    }

    public void ToggleInverted() => RelativeClass.ToggleInverted();

    public void Rotate(Vector3 degrees)
    {
        Trajectories.Rotate(degrees);
        AddAugmentationType(AugmentationType.Rotate);
    }

    public void MirrorX()
    {
        Trajectories.MirrorX();
        AddAugmentationType(AugmentationType.MirrorX);
        ToggleInverted();
    }

    public void MirrorZ()
    {
        Trajectories.MirrorZ();
        AddAugmentationType(AugmentationType.MirrorZ);
        ToggleInverted();
    }

    public void ShearX()
    {
        Trajectories.ShearX();
        AddAugmentationType(AugmentationType.ShearX);
    }

    public void ShearY()
    {
        Trajectories.ShearY();
        AddAugmentationType(AugmentationType.ShearY);
    }

    public void ShearZ()
    {
        Trajectories.ShearZ();
        AddAugmentationType(AugmentationType.ShearZ);
    }

    public void Scale(float scale)
    {
        Trajectories.Scale(scale);
        AddAugmentationType(AugmentationType.Scale);
    }

    public void AddNoise(float amount)
    {
        Trajectories.AddNoise(amount);
        AddAugmentationType(AugmentationType.AddNoise);
    }

    public void InvertDirection()
    {
        Trajectories.InvertDirection();
        AddAugmentationType(AugmentationType.InvertDirection);
        ToggleInverted();
    }

    public new IAugmentation Clone() => new HandGestureAugmentation(this.original);

    public override bool Equals(object obj)
    {
        HandGestureAugmentation other = (HandGestureAugmentation)obj;
        bool equal = base.Equals(obj) &&
            this.original == other.original;

        return equal;
    }
}
