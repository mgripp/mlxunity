﻿using UnityEngine;
using UnityEditor;
using Unity.Barracuda;
using System.IO;

// TODO: separate for hand and mouse gesture
public class TestAgent : MonoBehaviour
{
    public static TestAgent Instance;
    public float[] gestureClassPredictions { get; private set; }
    public int gestureClassIndex { get; private set; }
    public string gestureClassName { get; private set; }

    protected Model model;
    protected IWorker worker;
    protected LabeledInputData currentData;

    private void Start() => Initialize();
    protected void Initialize() => Instance = Instance ?? this;
    public void GiveModel(string path)
    {
        string name = Path.GetFileNameWithoutExtension(path);
        NNModel modelAsset = (NNModel)Resources.Load(name);
        if (modelAsset == null)
        {
            Debug.LogWarning($"Could not load model {path}");
            return;
        }
        model = ModelLoader.Load(modelAsset);
        worker = WorkerFactory.CreateWorker(WorkerFactory.Type.CSharpBurst, model);
        Debug.Log($"Loaded model from {path}");
    }
    public void GiveModel(FileInfo fileInfo) => GiveModel(fileInfo.FullName);

    virtual public Class ProcessGesture(LabeledInputData data)
    {
        if (worker == null)
        {
            Debug.LogWarning("Could not process gesture since no model is currently loaded.");
            return null;
        }

        float[] inputArray = data.inputData.Flattened();
        Tensor input = new Tensor(1, inputArray.Length, inputArray);
        Tensor output = worker.Execute(input).PeekOutput();
        var max_val = Mathf.Max(output.ToReadOnlyArray());
        var arr = output.ToReadOnlyArray();
        var index = System.Array.IndexOf(arr, max_val);
        print("-----------------------------");

        // TODO: add class name + number
        foreach (float f in output.ToReadOnlyArray())
            print(f);

        return ClassesConfig.Instance.Classes[index];
    }
}
