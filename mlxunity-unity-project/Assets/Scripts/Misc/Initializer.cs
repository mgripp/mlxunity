﻿using UnityEngine;

public class Initializer : MonoBehaviour
{
    [SerializeField] ConfigLoadSaveComponent configLoadSave;
    [SerializeField] GeneralConfigComponent generalConfig;
    [SerializeField] InputDataComponent inputDataConfig;
    [SerializeField] TestAgent testAgent;
    [SerializeField] RecorderComponent recorder;
    [SerializeField] Trainer trainer;
    [SerializeField] MainPanel ui;

    /**
     * This script will activate all components in the correct order.
     * For them to be correctly initialized, they need to be actiaved in that specific order.
     * */
    void Start()
    {
        generalConfig.enabled = true;
        inputDataConfig.enabled = true;
        testAgent.enabled = true;
        recorder.enabled = true;
        trainer.enabled = true;
        configLoadSave.enabled = true;
        ui.enabled = true;

        foreach(DeactivateDuringTraining deactivator in FindObjectsOfType<DeactivateDuringTraining>())
        {
            deactivator.enabled = true;
        }
    }
}
