﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using NUnit.Framework;

namespace Tests
{
    public class TestExamples
    {
        static public List<Vector2> vectorsExample1 => new List<Vector2> {
            new Vector2(1.1f, 1.2f),
            new Vector2(1.3f, 1.4f),
        };

        static public List<Vector2> vectorsExample2 => new List<Vector2> {
            new Vector2(0.1f, 0.2f),
            new Vector2(0.3f, 0.4f),
            new Vector2(0f, 0f),
        };

        static public Vector2DataArray vector2DataArrayExample1 => new Vector2DataArray(
            vectors: vectorsExample1,
            scale: true,
            normalized: false
        );

        static public Vector2DataArray vector2DataArrayExample2 => new Vector2DataArray(
            vectors: vectorsExample2,
            scale: false,
            normalized: true
        );

        static public string vector2DataArrayJSONExample1 => "[[[1.1,1.2],[1.3,1.4]]]";
        static public string vector2DataArrayJSONExample2 => "[[[0.1,0.2],[0.3,0.4],[0,0]]]";
        static public JSONNode vector2DataArrayJSONNodeExample1 => JSON.Parse(vector2DataArrayJSONExample1);
        static public JSONNode vector2DataArrayJSONNodeExample2 => JSON.Parse(vector2DataArrayJSONExample2);    

        static public float[][] trajectoryExample1Array => new float[][]
        {
                new float[]{ -5f, 10f },
                new float[]{ -10f, 5f },
                new float[]{ 5f, 0f },
                new float[]{ 0f, 5f },
                new float[]{ 4.5f, -8.4f },
                new float[]{ 0, 20f },
        };
        static public List<Vector2> trajectoryExample1List => new List<Vector2>
            {
                new Vector2(-5f, 10f),
                new Vector2(-10f, 5f),
                new Vector2(5f, 0),
                new Vector2(0, 5f),
                new Vector2(4.5f, -8.4f),
                new Vector2(0f, 20f),
            };
        static public Vector2 trajecoryExample1Size => new Vector2(15f, 31.6f);
        static public List<Vector2> trajectoryExample1NormalizedList => new List<Vector2>
            {
                new Vector2(-0.5f, 1f),
                new Vector2(-1f, 0.5f),
                new Vector2(0.5f, 0),
                new Vector2(0, 0.5f),
                new Vector2(0.45f, -0.84f),
                new Vector2(0, 1f)
            };
        static public List<Vector2> trajectoryExample1ZeroPaddedList => new List<Vector2>
            {
                new Vector2(0, 10f),
                new Vector2(10f, 0),
                new Vector2(0, -10f),
                new Vector2(-10f, 0),
                new Vector2(-1f, 6.8f),
                new Vector2(-10f, 20f),
                new Vector2(0, 0),
                new Vector2(0, 0),
            };

        static public VectorDataArrayContainer vector2DataArrayContainerExample1 => new VectorDataArrayContainer(
            vectorArrays: new List<IDataArray> { vector2DataArrayExample1 },
            countMax: -1,
            scale: true,
            normalized: false
        );

        static public VectorDataArrayContainer vector2DataArrayContainerExample2 => new VectorDataArrayContainer(
            vectorArrays: new List<IDataArray> { vector2DataArrayExample2 },
            countMax: -1,
            scale: false,
            normalized: true
        );

        static public string vector2DataArrayContainerJSONExample1
            => $"{{\"normalized\":false,\"scaled\":true,\"data_type\":\"Vector2\",\"data\":{vector2DataArrayJSONExample1}}}";
        static public string vector2DataArrayContainerJSONExample2
            => $"{{\"normalized\":true,\"scaled\":false,\"data_type\":\"Vector2\",\"data\":{vector2DataArrayJSONExample2}}}";
        static public JSONNode vector2DataArrayContainerJSONNodeExample1
            => JSON.Parse(vector2DataArrayContainerJSONExample1);
        static public JSONNode vector2DataArrayContainerJSONNodeExample2
            => JSON.Parse(vector2DataArrayContainerJSONExample2);

        // Gestures
        static public MouseGesture gestureExample1 => new MouseGesture(
            input: vector2DataArrayContainerExample1,
            classIndex: 0
        );
        static public MouseGesture gestureExample2 => new MouseGesture(
            input: vector2DataArrayContainerExample2,
            classIndex: 6
        );

        // Gestures as JSON
        static public string gestureJSONExample1 => $"{{\"class\":0,\"name\":\"Circle CCW\",\"input_data\":{vector2DataArrayContainerJSONExample1}}}";
        static public string gestureJSONExample2 => $"{{\"class\":6,\"name\":\"Swipe\",\"augmentation_types\":[\"Rotate\"],\"input_data\":{vector2DataArrayContainerJSONExample2}}}";
        // this one is used for testing whether jsons without augmentation attributes can still be loaded
        static public string gestureJSONExample3 => $"{{\"class\":6,\"name\":\"Swipe\",\"input_data\":{vector2DataArrayContainerJSONExample1}}}";
        static public string gestureJSONExample1Augmentations => $"{{\"class\":6,\"name\":\"Swipe\",\"augmentations\":[{gestureJSONExample2}],\"input_data\":{vector2DataArrayContainerJSONExample1}}}";
        
        static public JSONNode gestureJSONNodeExample1 => JSON.Parse(gestureJSONExample1);
        static public JSONNode gestureJSONNodeExample2 => JSON.Parse(gestureJSONExample2);

        static public MouseGesture gestureJSONExample1AsGesture => new MouseGesture(gestureJSONExample1);
        static public MouseGesture gestureJSONExample2AsGesture => new MouseGesture(gestureJSONExample2);
        static public MouseGesture gestureJSONExample3AsGesture => new MouseGesture(gestureJSONExample3);
        static public MouseGesture gestureJSONExample1AugmentationsAsGesture => new MouseGesture(gestureJSONExample1Augmentations);
    }
}
