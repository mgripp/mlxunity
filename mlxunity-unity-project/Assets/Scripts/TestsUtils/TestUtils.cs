﻿using NUnit.Framework;
using System.IO;
using SimpleJSON;

namespace Tests
{
    // TODO: put all functionality to respective config tests

    public class TestUtils
    {
        static ConfigLoadSave configLoadSave;

        static GeneralConfig generalConfig;

        static  TrainConfig TrainConfig;

        static RecordConfig recorderConfig;
        static MouseRecorder recorder;
        static RecordingsLoadSave recordingsLoadSave;

        static ClassesConfig classesConfig;

        const string TestProjectPath = "Assets/MLxUnity/test_project.mlx/";
        static public string TestDefaultConfigPath => $"{TestProjectPath}config_default.json";
        static public string TestRecordPath => $"{TestProjectPath}test.json";

        static public void Initialize<InputDataType>()
            where InputDataType : LabeledInputData
        {
            Vector2DataArray.NormalizationReference = 10f;

            Assert.IsNull(GeneralConfig.Instance);
            Assert.IsNull(TrainConfig.Instance);
            Assert.IsNull(Recorder.Instance);
            Assert.IsNull(RecordConfig.Instance);
            Assert.IsNull(RecordingsLoadSave.Instance);
            Assert.IsNull(ClassesConfig.Instance);
            Assert.IsNull(ConfigLoadSave.Instance);

            generalConfig = new GeneralConfig(defaultPath: TestDefaultConfigPath);
            generalConfig.ProjectPath = TestProjectPath;
            Assert.IsNotNull(GeneralConfig.Instance);
            Assert.IsFalse(File.Exists(TestProjectPath));

            TrainConfig = new TrainConfig();
            Assert.IsNotNull(TrainConfig.Instance);

            recorder = new MouseRecorder();
            recorderConfig = new RecordConfig<InputDataType>();
            recordingsLoadSave = new RecordingsLoadSave();
            Assert.IsNotNull(Recorder.Instance);
            Assert.IsNotNull(RecordConfig.Instance);
            Assert.IsNotNull(RecordingsLoadSave.Instance);

            classesConfig = new ClassesConfig();
            Assert.IsNotNull(ClassesConfig.Instance);

            configLoadSave = new ConfigLoadSave();
            Assert.IsNotNull(ConfigLoadSave.Instance);
            configLoadSave.LoadConfig();
        }

        static public void CleanUp()
        {
            if (GeneralConfig.Instance.ProjectPath == TestProjectPath && File.Exists(GeneralConfig.Instance.ConfigPath))
                File.Delete(GeneralConfig.Instance.ConfigPath);
            if (File.Exists(TestRecordPath))
                File.Delete(TestRecordPath);

            configLoadSave = null;
            generalConfig = null;
            TrainConfig = null;
            recorderConfig = null;
            recorder = null;
            recordingsLoadSave = null;
            classesConfig = null;

            GeneralConfig.Instance = null;
            TrainConfig.Instance = null;
            Recorder.Instance = null;
            RecordConfig.Instance = null;
            RecordingsLoadSave.Instance = null;
            ClassesConfig.Instance = null;
            ConfigLoadSave.Instance = null;
        }

        [Test]
        static public void TestGestureExampleConsistency()
        {
            Initialize<MouseGesture>();
            Assert.AreEqual(TestExamples.gestureJSONExample1AsGesture, TestExamples.gestureExample1);
            Assert.AreEqual(TestExamples.gestureJSONExample2AsGesture, TestExamples.gestureExample2);
            Assert.AreEqual(TestExamples.gestureExample1.Trajectory, TestExamples.vector2DataArrayContainerExample1);
            Assert.AreEqual(TestExamples.gestureExample2.Trajectory, TestExamples.vector2DataArrayContainerExample2);

            Assert.IsFalse(TestExamples.gestureExample1.Normalized);
            Assert.IsTrue(TestExamples.gestureExample2.Normalized);
            Assert.IsTrue(TestExamples.gestureExample1.Scaled);
            Assert.IsFalse(TestExamples.gestureExample2.Scaled);
        }

        [Test]
        public void TestInitialize() => Initialize<MouseGesture>();

        [Test]
        public void TestCleanup()
        {
            Initialize<MouseGesture>();
            CleanUp();
            Initialize<MouseGesture>();
        }

        [TearDown]
        public void AfterTest() => TestUtils.CleanUp();
    }
}
