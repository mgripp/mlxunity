﻿using System.Diagnostics;

public class CommandPrompt
{
    public void ExecuteCommand(string command)
    {
        UnityEngine.Debug.Log($"Executing command: {command}");
        command = @"/k " + @command;
        ProcessStartInfo processInfo = new ProcessStartInfo("cmd.exe", @command);
        processInfo.CreateNoWindow = true;
        Process process = Process.Start(processInfo);
        process.WaitForExit();
        process.Close();
    }
}
