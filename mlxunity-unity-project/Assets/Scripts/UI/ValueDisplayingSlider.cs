﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ValueDisplayingSlider : Slider
{
    public TMP_Text displayText;
    public bool base2Values = false;
    public string format = "00.0";
    public int roundDigits = 1;

    protected override void Set(float input, bool sendCallback = true)
    {
        base.Set(input, sendCallback);
        Display(value);
    }

    public override void SetValueWithoutNotify(float input)
    {
        if (base2Values)
            input = Mathf.Log(input) / Mathf.Log(2);
        base.SetValueWithoutNotify(input);
    }

    void Display(float displayValue)
    {
        if (base2Values)
            displayValue = Mathf.Pow(2, Mathf.Round(displayValue));

        string formatUsed = format;
        if (wholeNumbers)
            formatUsed = "";
        float rounder = Mathf.Pow(10f, roundDigits);
        displayText.text = (Mathf.Round(displayValue * rounder) / rounder).ToString(formatUsed);
    }
}
