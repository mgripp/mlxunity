﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainPanel : MonoBehaviour
{
    static public MainPanel Instance;
    
    [SerializeField] MonoBehaviour[] childPanels;

    private void Start()
    {
        Instance = Instance ?? this;

        foreach (MonoBehaviour panel in childPanels)
            panel.enabled = true;
    }

    /*
     * Taken from this video tutorial by Code Monkeys:
     * https://www.youtube.com/watch?time_continue=269&v=ptmum1FXiLE&feature=emb_logo
    */
    public bool MouseIsOverUI()
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = Input.mousePosition;

        List<RaycastResult> raycastResultList = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, raycastResultList);
        for (int i = 0; i < raycastResultList.Count; i++)
        {
            if (raycastResultList[i].gameObject.GetComponent<GUIClickThrough>() != null)
            {
                raycastResultList.RemoveAt(i);
                i--;
            }
        }

        return raycastResultList.Count > 0;
    }
}
