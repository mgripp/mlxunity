﻿using UnityEditor;

[CustomEditor(typeof(ValueDisplayingSlider))]
public class ValueDisplayingSliderEditor : UnityEditor.UI.SliderEditor
{
    SerializedProperty displayText;
    SerializedProperty base2Values;
    SerializedProperty format;
    SerializedProperty roundDigits;

    protected override void OnEnable()
    {
        base.OnEnable();
        displayText = serializedObject.FindProperty("displayText");
        base2Values = serializedObject.FindProperty("base2Values");
        format = serializedObject.FindProperty("format");
        roundDigits = serializedObject.FindProperty("roundDigits");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();
        EditorGUILayout.PropertyField(displayText);
        EditorGUILayout.PropertyField(base2Values);
        EditorGUILayout.PropertyField(format);
        EditorGUILayout.PropertyField(roundDigits);
        serializedObject.ApplyModifiedProperties();
    }
}
