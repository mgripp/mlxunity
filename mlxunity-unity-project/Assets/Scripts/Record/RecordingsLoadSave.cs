﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class RecordingsLoadSave
{
    static public RecordingsLoadSave Instance;

    public RecordingsLoadSave() => Instance = Instance ?? this;

    public List<LabeledInputData> LoadFromJSON(int classIndex)
    {
        JSONNode jsonData;
        return LoadFromJSON(out jsonData, classIndex);
    }

    public List<LabeledInputData> LoadFromJSON(out JSONNode jsonData, string path)
    {
        string jsonText = "[]";

        // create file of non-existent
        if (!File.Exists(path))
            InitializeJSON(path);
        // read file if existent
        else
            jsonText = File.ReadAllText(path);

        // Length of 3 is an estimated minimum length the file should have to be valid.
        // This length will be below that of the file is empty or has been non-existent.
        if (File.ReadAllText(path).Length < 3)
        {
            InitializeJSON(path);
            jsonData = new JSONArray();
            return new List<LabeledInputData>();
        }
        else
            jsonData = JSON.Parse(jsonText);

        List<LabeledInputData> gestures = new List<LabeledInputData>();

        foreach (JSONObject json in jsonData)
        {
            LabeledInputData gesture = (LabeledInputData) Activator.CreateInstance(RecordConfig.Instance.InputDataType);
            gesture.LoadFromJSON(json);
            gestures.Add(gesture);
            Debug.Log($"Loaded gesture: {gesture}");
        }

        return gestures;
    }

    public List<LabeledInputData> LoadFromJSON(out JSONNode jsonData, int classIndex)
    {
        string path = ClassesConfig.Instance.Classes[classIndex].RecordPath;
        return LoadFromJSON(out jsonData, path);
    }

    public void InitializeJSON(string path)
    {
        try
        {
            if (!File.Exists(path))
                Directory.CreateDirectory(Path.GetDirectoryName(path));

            File.WriteAllText(path, "[]");
        }
        catch (System.Exception e)
        {
            Debug.LogWarning(string.Format(
                "Could not access gesture json file {0}: {1}",
                path,
                e
            ));
        }
    }

    void InitializeJSON(int classIndex)
    {
        string path = ClassesConfig.Instance.Classes[classIndex].RecordPath;
        InitializeJSON(path);
    }

    public void OverwriteJSON(List<LabeledInputData> gestures, int classIndex)
    {
        InitializeJSON(classIndex);
        WriteJSON(gestures, classIndex);
    }

    public void OverwriteJSON(List<LabeledInputData> gestures, string path)
    {
        InitializeJSON(path);
        WriteJSON(gestures, path);
    }

    public void WriteJSON(List<LabeledInputData> newGestures, string path)
    {
        JSONArray jsonData = new JSONArray();

        foreach (LabeledInputData gesture in newGestures)
        {
            JSONObject gestureJSON = gesture.ToJSON();
            jsonData.Add(gestureJSON);
        }

        string jsonString = jsonData.ToString();
        File.WriteAllText(path, jsonString);
        Debug.Log($"Wrote to json {path}: {jsonString}");
    }

    public void WriteJSON(List<LabeledInputData> newGestures, int classIndex)
    {
        string path = ClassesConfig.Instance.Classes[classIndex].RecordPath;
        WriteJSON(newGestures, path);
    }
}
