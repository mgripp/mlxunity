﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Recorder
{
    static public Recorder Instance;

    public bool Recording { get; private set; } = false;

    public Recorder() => Instance = Instance ?? this;

    public bool ToogleRecording()
    {
        if (Recording)
            StopRecording();
        else
            StartRecording();
        return Recording;
    }

    virtual protected void StartRecording()
    {
        ConfigLoadSave.Instance.SaveConfig();
        Recording = true;
    }

    virtual protected void StopRecording() => Recording = false;
    abstract public void StopDrawingGesture();

    protected void SaveGesture<InputDataType>(LabeledInputData gesture)
         where InputDataType : LabeledInputData
    {
        if (RecordConfig.Instance.NormalizeVectors)
            gesture.Normalize();

        RecordPanel.Instance?.ShowGestureName(TestAgent.Instance.ProcessGesture(gesture)?.Name);
        gesture?.Augment();
        RecordingsContainer.Instance.Add(gesture);
    }
}
