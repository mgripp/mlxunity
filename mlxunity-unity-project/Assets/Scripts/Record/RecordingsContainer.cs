﻿using System.Collections.Generic;
using UnityEngine;

/**
 * This is a singleton, that keeps the currently worked with Gestures. This will either be a loaded set of Gestures (from JSON)
 * or a set of recorded Gestures from this sesssion. It can also contain a set of Gestures from serveral of these sources.
 */
// TODO: unit tests
public class RecordingsContainer
{
    static public RecordingsContainer Instance;

    public List<LabeledInputData> gestures = new List<LabeledInputData>();
    public int Count => gestures.Count;
    public int TotalCount => totalCount;

    int totalCount = 0;

    public RecordingsContainer()
    {
        Instance = Instance ?? this;
    }

    public void AugmentGestures()
    {
        foreach (LabeledInputData gesture in gestures)
        {
            gesture.Augment();
        }
    }

    public void NormalizeGestures()
    {
        foreach (LabeledInputData gesture in gestures)
            gesture.Normalize();
    }

    public void Add(LabeledInputData gesture)
    {
        gestures.Add(gesture);
        totalCount += gesture.AugmentationCount + 1;
        RecordPanel.Instance.AddGesture(gesture);
    }

    public void AddRange(List<LabeledInputData> gesturesRange)
    {
        foreach (LabeledInputData gesture in gesturesRange)
            Add(gesture);
    }

    public void Drop(LabeledInputData gesture)
    {
        gestures.Remove(gesture);
        totalCount -= gesture.AugmentationCount + 1;
        RecordPanel.Instance.RemoveGesture(gesture);
    }

    public void Clear()
    {
        gestures = new List<LabeledInputData>();
        totalCount = 0;
        RecordPanel.Instance.RemoveAllGestures();
    }

    public void LoadGestures(int classIndex)
    {
        List<LabeledInputData> gestureRange = RecordingsLoadSave.Instance.LoadFromJSON(classIndex);
        AddRange(gestureRange);

        if (gestureRange.Count < 1)
            // TODO: info message for user when no gestures where found for that index
            Debug.Log(string.Format("No stored gestures for class #{0} \"{1}\"",
                classIndex,
                ClassesConfig.Instance.Classes[classIndex].Name)
            );
    }

    public void SaveGestures()
    {
        // sort gestures according to their class
        List<LabeledInputData>[] gesturesSorted = new List<LabeledInputData>[ClassesConfig.Instance.ClassCount];
        for (int i = 0; i < gesturesSorted.Length; i++)
            gesturesSorted[i] = new List<LabeledInputData>();

        foreach (LabeledInputData gesture in gestures)
            gesturesSorted[gesture.ClassIndex].Add(gesture);

        // save all gestures to their respective class JSON
        foreach (List<LabeledInputData> currentGestures in gesturesSorted)
        {
            // skip empty lists
            if (currentGestures.Count < 1)
                continue;

            // get current class index from any gesture in the list
            int currentClassIndex = currentGestures[0].ClassIndex;

            RecordingsLoadSave.Instance.OverwriteJSON(currentGestures, currentClassIndex);

            Debug.Log(string.Format("Exported {0} gestures to {1}",
                currentGestures.Count,
                GeneralConfig.Instance.ProjectPath
            ));
        }
    }
}
