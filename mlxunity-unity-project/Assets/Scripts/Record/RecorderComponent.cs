﻿using UnityEngine;

public abstract class RecorderComponent : MonoBehaviour
{
    protected Recorder recorder;
    protected RecordingsContainer recordingsContainer;
    protected RecordingsLoadSave recordingsLoadSave;
    protected bool gestureActive = false;

    public void StartDrawingGesture() => gestureActive = true;

    virtual public void StopDrawingGesture()
    {
        gestureActive = false;
        recorder.StopDrawingGesture();
    }
}
