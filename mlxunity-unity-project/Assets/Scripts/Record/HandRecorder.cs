﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandRecorder : Recorder
{
    List<List<Vector3>> trajectories;

    public HandRecorder() : base() => trajectories = new List<List<Vector3>>();

    public void AddMovement(int joint, Vector3 movement)
    {
        int diff = joint + 1 - trajectories.Count;
        for (int i = 0; i < diff; i++)
            trajectories.Add(new List<Vector3>());
        trajectories[joint].Add(movement);
    }

    override public void StopDrawingGesture()
    {
        if (trajectories.Count < 1)
            return;

        if (trajectories[0].Count > RecordConfig<HandGesture>.Instance.TrajectoryCountMin)
        {
            List<IDataArray> vectorArrays = new List<IDataArray>();
            foreach (List<Vector3> trajectory in trajectories)
                vectorArrays.Add(new Vector3DataArray(trajectory));

            HandGesture gesture = new HandGesture(
                inputData: vectorArrays,
                countMax: RecordConfig<HandGesture>.Instance.TrajectoryCountMax,
                scale: RecordConfig<HandGesture>.Instance.ScaleTrajectories,
                classIndex: RecordConfig<HandGesture>.Instance.ClassIndex
            );
            if (Recording)
                SaveGesture<HandGesture>(gesture);
            trajectories = new List<List<Vector3>>();
        }
    }
}
