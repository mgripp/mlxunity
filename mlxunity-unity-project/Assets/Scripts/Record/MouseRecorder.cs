﻿using UnityEngine;
using System.Collections.Generic;

public class MouseRecorder : Recorder
{
    List<Vector2> trajectory;

    public MouseRecorder() : base() => trajectory = new List<Vector2>();
    public void AddMovement(Vector2 movement) => trajectory.Add(movement);

    override protected void StopRecording()
    {
        base.StopRecording();
        LineDrawer.mainDrawer.Clear();
    }

    override public void StopDrawingGesture()
    {
        LineDrawer.mainDrawer.FadeOutIndipendently(.3f);
        if (trajectory.Count > RecordConfig.Instance.TrajectoryCountMin)
        {
            MouseGesture gesture = new MouseGesture(
                input: trajectory,
                countMax: RecordConfig.Instance.TrajectoryCountMax,
                scale: RecordConfig.Instance.ScaleTrajectories,
                classIndex: RecordConfig.Instance.ClassIndex
            );
            if (Recording)
                SaveGesture<MouseGesture>(gesture);
            trajectory = new List<Vector2>();
        }
    }
}
