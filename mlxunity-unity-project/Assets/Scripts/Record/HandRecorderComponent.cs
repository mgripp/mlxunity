﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class HandRecorderComponent : RecorderComponent
{
    [SerializeField] GameObject jointVisualizer;
    [SerializeField] Transform visualizerTransform;
    [SerializeField] float visualizationScale = .1f;

    RecordConfig<HandGesture> config;
    List<GameObject> jointVisualizers = new List<GameObject>();
    Controller controller;
    Vector3[] jointPositionsBefore = null;

    void Start()
    {
        config = new RecordConfig<HandGesture>();
        recorder = new HandRecorder();
        recordingsContainer = new RecordingsContainer();
        recordingsLoadSave = new RecordingsLoadSave();
        controller = new Controller();
    }

    void Update()
    {
        if (Input.GetButtonDown("StartRecord"))
            RecordPanel.Instance.ToggleRecord();

        Hand hand;
        Vector3[] jointPositions;
        List<Joint> joints = GetJointPositions(out hand, out jointPositions);

        // Do not process if no hand has been detected
        if (joints.Count < 1)
            return;

        Vector3[] visualizedPositions;
        VisualizeHand(joints, hand, out visualizedPositions);

        // if ctrl is pressed, draw/record hand movement
        if (Input.GetButtonDown("Record"))
            StartDrawingGesture();
        else if (Input.GetButtonUp("Record"))
            StopDrawingGesture();

        // watch joints movement and draw/record it when recording is active
        if (jointPositionsBefore != null)
            for (int i = 0; i < jointPositions.Length; i++)
            {
                Vector3 position = jointPositions[i];
                Vector3 movement = (position - jointPositionsBefore[i]);

                if (gestureActive && movement.magnitude > RecordConfig.Instance.MinMovementDistance)
                {
                    movement = Quaternion.Euler(90f, 180f, 0) * movement;
                    ((HandRecorder)recorder).AddMovement(i, movement);
                    ((LineDrawerArray)LineDrawer.mainDrawer).DrawLines(visualizedPositions);
                }
            }

        jointPositionsBefore = jointPositions;
    }

    public override void StopDrawingGesture()
    {
        ((LineDrawerArray)LineDrawer.mainDrawer).Clear();
        base.StopDrawingGesture();
    }

    struct Joint
    {
        public string name;
        public Vector3 position;

        public Joint(string name, Vector3 position)
        {
            this.name = name;
            this.position = position;
        }
    }

    List<Joint> GetJointPositions(out Hand hand, out Vector3[] positions)
    {
        Frame frame = controller.Frame();
        List<Joint> joints = new List<Joint>();
        List<Vector3> positionsList = new List<Vector3>();
        hand = null;

        if (frame.Hands.Count > 0)
        {
            hand = frame.Hands[0];
            int index = 0;
            foreach (Finger finger in hand.Fingers)
                foreach (Bone bone in finger.bones)
                {
                    Leap.Vector joint = bone.PrevJoint;
                    // TODO something is flipped here...
                    Vector3 joinPosition = new Vector3(joint.x, joint.y, joint.z);
                    positionsList.Add(joinPosition);
                    joints.Add(new Joint($"{finger.Type}_{bone.Type}", joinPosition));
                    index++;
                }
        }

        positions = positionsList.ToArray();
        return joints;
    }

    void VisualizeHand(List<Joint> joints, Hand hand, out Vector3[] positions)
    {
        List<Vector3> positionsList = new List<Vector3>();

        // create enough visualizers for the joints list
        while (joints.Count - 1 > jointVisualizers.Count)
            jointVisualizers.Add(GameObject.Instantiate(jointVisualizer, visualizerTransform));
        if (jointVisualizers.Count > joints.Count)
            // disable all unused visualizers
            for (int i = joints.Count; i < jointVisualizers.Count - 1; i++)
                jointVisualizers[i].SetActive(false);

        for (int i = 0; i < joints.Count - 1; i++)
        {
            GameObject visualizer = jointVisualizers[i];
            visualizer.SetActive(true);

            Vector originLeap = hand.WristPosition;

            // use this to negate hand rotations
            //Quaternion rotationPalm = Quaternion.FromToRotation(Vector3.right, LeapToUnityVector(hand.PalmNormal));
            //Quaternion rotationHand = Quaternion.FromToRotation(Vector3.up, LeapToUnityVector(hand.Direction));

            Vector3 origin = LeapToUnityVector(originLeap);
            Joint joint = joints[i];
            Vector3 position = (joint.position - origin) * visualizationScale;
            //position = Quaternion.Inverse(rotationPalm) * position;
            positionsList.Add(position);
            visualizer.transform.localPosition = position;
            visualizer.name = joint.name;
        }

        positions = positionsList.ToArray();
    }

    Vector3 LeapToUnityVector(Vector leapVector) => new Vector3(leapVector.x, leapVector.y, leapVector.z);
}
