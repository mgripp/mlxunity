﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawerSingle : LineDrawer
{
    [SerializeField] new protected LineRenderer renderer;
    public LineRenderer Renderer => renderer;

    bool destroyAfterFadeOut = false;

    private void Awake() => Initialize();

    override protected void Initialize()
    {
        base.Initialize();
        material = Instantiate(renderer.material);
        renderer.material = material;
    }

    override public void DrawLines(Vector3[] newPositions, float lineWidth = .1f)
    {
        renderer.startWidth = lineWidth;
        foreach (Vector3 position in newPositions)
            DrawLine(position);
    }

    override public void DrawLine(Vector3 newPosition, float lineWidth = .1f)
    {
        // TODO: Fails when rapidly repeating drawing of trajectories
        renderer.startWidth = lineWidth;
        renderer.positionCount++;
        renderer.SetPosition(renderer.positionCount - 1, newPosition);
    }

    override public void SetPositions(Vector3[] positions)
    {
        renderer.positionCount = positions.Length;
        renderer.SetPositions(positions);
    }

    override public void Clear()
    {
        renderer.positionCount = 0;
        base.Clear();
    }

    override public void FadeOutIndipendently(float time)
    {
        LineDrawerSingle fadingLine = (LineDrawerSingle)DeepCopy();
        fadingLine.destroyAfterFadeOut = true;

        fadingLine.FadeOut(time);

        Clear();
    }

    override protected IEnumerator FadeOutRoutine(float time)
    {
        float currentTime = 0;
        float alphaStart = material.color.a;

        while (currentTime < time)
        {
            SetAlpha(alphaStart * (1 - currentTime / time));

            currentTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        if (destroyAfterFadeOut)
        {
            GameObject.Destroy(gameObject);
        }
        else
        {
            Clear();
            SetAlpha(1f);
        }
    }

    override protected LineDrawer DeepCopy()
    {
        LineDrawerSingle clone = GameObject.Instantiate(gameObject).GetComponent<LineDrawerSingle>();
        clone.transform.parent = transform.parent;
        clone.transform.position = transform.position;

        Vector3[] positions = new Vector3[renderer.positionCount];
        renderer.GetPositions(positions);

        clone.Initialize();
        clone.DrawLines(positions);

        return clone;
    }
}
