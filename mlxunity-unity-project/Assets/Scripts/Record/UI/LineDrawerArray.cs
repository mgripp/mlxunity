﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawerArray : LineDrawer
{
    [SerializeField] GameObject lineDrawerPrefab;
    List<LineDrawerSingle> lineDrawers = new List<LineDrawerSingle>();

    private void Awake() => Initialize();

    public override void DrawLine(Vector3 newPosition, float lineWidth = .1f) => DrawLines(new Vector3[] { newPosition }, lineWidth);
    public override void DrawLines(Vector3[] newPositions, float lineWidth = .1f)
    {
        int diff = newPositions.Length - lineDrawers.Count;
        for (int i = 0; i < diff; i++)
        {
            GameObject g = Instantiate(lineDrawerPrefab, transform);
            LineDrawerSingle drawer = g.GetComponent<LineDrawerSingle>();
            drawer.DrawLine(newPositions[i], lineWidth);
            lineDrawers.Add(drawer);
        }

        for (int i = 0; i < lineDrawers.Count; i++)
            lineDrawers[i].DrawLine(newPositions[i], lineWidth);
    }

    public void FitToCamera(Camera camera)
    {
        Bounds boundsGesture = new Bounds(transform.position, Vector3.one);
        foreach (LineDrawerSingle drawer in lineDrawers)
            boundsGesture.Encapsulate(drawer.Renderer.bounds);
        float heightCamera = 2f * camera.orthographicSize;
        float widthCamera = heightCamera * camera.aspect;

        // scale so that it fits within the display area
        float scale;
        float scaleX = widthCamera / boundsGesture.size.x;
        float scaleY = heightCamera / boundsGesture.size.y;
        scale = Mathf.Min(scaleX, scaleY);
        transform.localScale = Vector3.one * scale;

        // Center line array on rec
        boundsGesture = new Bounds(transform.position, Vector3.one);
        foreach (LineDrawerSingle drawer in lineDrawers)
            boundsGesture.Encapsulate(drawer.Renderer.bounds);
        Vector3 offset = camera.transform.position - boundsGesture.center + Vector3.forward * boundsGesture.size.z;
        transform.position += offset;
    }

    public override void Clear()
    {
        foreach (LineDrawerSingle drawer in lineDrawers)
            drawer.Clear();
        base.Clear();
        transform.localScale = Vector3.one;
    }

    public override void SetPositions(Vector3[] positions)
    {
        throw new System.NotImplementedException();
    }

    public override void FadeOutIndipendently(float time)
    {
        throw new System.NotImplementedException();
    }

    protected override IEnumerator FadeOutRoutine(float time)
    {
        throw new System.NotImplementedException();
    }
}
