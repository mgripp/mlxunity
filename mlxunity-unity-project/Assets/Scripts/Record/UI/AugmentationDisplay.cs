﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AugmentationDisplay : LineDisplay
{
    [SerializeField] TMP_Text typeText;
    [SerializeField] TMP_Text numberText;
    [SerializeField] TMP_Text classText;
    [SerializeField] Image cwIndicator;
    [SerializeField] Image ccwIndicator;

    LabeledInputData gesture;
    int augmentationNumber = 0;
    float lineWidth;

    public void Initialize(LabeledInputData gesture, float lineWidth = .1f)
    {
        this.gesture = gesture;
        this.lineWidth = lineWidth;
        DisplayGesture(lineWidth);
    }

    void DisplayGesture(float lineWidth = .1f)
    {
        if (gesture.Augmentations.Count < 1)
            return;
        LabeledInputData augmentation = gesture.Augmentations[augmentationNumber];
        DisplayGesture(augmentation, lineWidth);
    }

    public override void DisplayGesture(LabeledInputData gestureDisplay, float lineWidth = .1f)
    {
        base.DisplayGesture(gestureDisplay, lineWidth);

        // ----- DisplayGesture(augmentation);
        typeText.text = ((IAugmentation)gestureDisplay).LastAugmentation;
        numberText.text = string.Format("{0}/{1}", (augmentationNumber + 1).ToString(), gesture.AugmentationCount);
        classText.text = gestureDisplay.ClassName;

        // TODO: implement universal handling of direction display
        //cwIndicator.gameObject.SetActive(false);
        //ccwIndicator.gameObject.SetActive(false);
        //if (augmentation.Direction == MouseGestureAugmentation.DrawDirection.None)
        //{
        //    cwIndicator.gameObject.SetActive(false);
        //    ccwIndicator.gameObject.SetActive(false);
        //}
        //else if (augmentation.Direction == MouseGestureAugmentation.DrawDirection.CW)
        //{
        //    cwIndicator.gameObject.SetActive(true);
        //    ccwIndicator.gameObject.SetActive(false);
        //}
        //else if (augmentation.Direction == MouseGestureAugmentation.DrawDirection.CCW)
        //{
        //    cwIndicator.gameObject.SetActive(false);
        //    ccwIndicator.gameObject.SetActive(true);
        //}
    }

    public void IncrementNumber()
    {
        if (augmentationNumber < gesture.AugmentationCount - 1)
        {
            augmentationNumber++;
            DisplayGesture(lineWidth);
        }
    }

    public void DecrementNumber()
    {
        if (augmentationNumber > 0)
        {
            augmentationNumber--;
            DisplayGesture(lineWidth);
        }
    }
}
