﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RecordingPanelArray : MonoBehaviour
{
    public static float panelsMargin = 10f;

    [SerializeField] GameObject panelPrefab;
    [SerializeField] GameObject background;
    [SerializeField] GameObject backgroundDetails;
    [SerializeField] GameObject detailsOnButton;
    [SerializeField] GameObject detailsOffButton;
    [SerializeField] RectTransform panelsContainer;
    [SerializeField] RectTransform panelsContainerBackground;
    [SerializeField] Scrollbar scrollbar;
    [SerializeField] TMP_Text gestureCountText;
    [SerializeField] float lineWidth;
    [SerializeField] float lineWidthAugmentation;

    List<RecordingPanel> panels = new List<RecordingPanel>();
    float panelsContainerHeight = 0;
    bool detailedViewActive = false;
    bool scrollBarActive = false;

    void Start()
    {
        SetDetailedView(detailedViewActive);
    }

    public void Add(LabeledInputData gesture)
    {
        RecordingPanel panel = Instantiate(panelPrefab, panelsContainer, false).GetComponent<RecordingPanel>();

        panel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, panelsContainer.rect.width);
        panel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, panelsContainer.rect.height);
        panel.transform.localPosition = Vector3.zero;

        float newPanelHeight = panel.rectTransform.rect.height + panelsMargin;

        // move all other panels one step down
        foreach (RecordingPanel otherPanel in panels)
            otherPanel.MoveDown(newPanelHeight);

        // if detailed view is not active, hide previously shown gesture panel
        if (panels.Count > 0)
            panels[panels.Count - 1].gameObject.SetActive(detailedViewActive);

        panel.Initialize(gesture, panels.Count, lineWidth, lineWidthAugmentation);
        panels.Add(panel);
        panelsContainerHeight += newPanelHeight;

        // reset scrollbar
        scrollbar.value = 1f;

        UpdateScrollBarEnabled();
        UpdateGestureCountText();
    }

    public void Remove(LabeledInputData gesture)
    {
        RecordingPanel removePanel = null;

        foreach (RecordingPanel panel in panels)
        {
            if (panel.BelongsToGesture(gesture))
            {
                removePanel = panel;
                break;
            }
        }

        if (removePanel == null)
        {
            Debug.LogWarning(string.Format("The gesture to be removed has not been found in panel array: {0}", gesture));
            return;
        }

        // if removePanel is not the top one (index < max. index)
        if (removePanel.Index < panels.Count - 1)
        {
            for (int i = removePanel.Index + 1; i < panels.Count; i++)
            {
                panels[i].DecrementIndex();
            }
        }

        // height of the removed panel with margin
        float removeHeight = removePanel.rectTransform.rect.height + panelsMargin;

        for (int i = removePanel.Index - 1; i > -1; i--)
        {
            panels[i].MoveUp(removeHeight);
        }

        panels.Remove(removePanel);
        panelsContainerHeight -= removeHeight;

        UpdateGestureCountText();
        Destroy(removePanel.gameObject);

        if (!detailedViewActive && panels.Count > 0)
            panels[panels.Count - 1].gameObject.SetActive(true);
    }

    public void RemoveAllGestures()
    {
        foreach(RecordingPanel panel in panels)
        {
            Destroy(panel.gameObject);
        }

        panels = new List<RecordingPanel>();
        panelsContainerHeight = 0;
    }

    public void ClearContainer()
    {
        RecordingsContainer.Instance.Clear();
        UpdateGestureCountText();
    }

    public void UpdateGestureCountText()
    {
        gestureCountText.text = string.Format("{0} (Total with Augmentations: {1})",
            RecordingsContainer.Instance.Count, RecordingsContainer.Instance.TotalCount
        );
    }

    void UpdateScrollBarEnabled()
    {
        if (!scrollBarActive && detailedViewActive && panelsContainerHeight > panelsContainerBackground.rect.height)
        {
            scrollBarActive = true;
            scrollbar.gameObject.SetActive(scrollBarActive);
        }
    }

    public void ScrollBarValueChanged(float value)
    {
        float y = (1f - value) * (panelsContainerHeight - panelsContainerBackground.rect.height);

        panelsContainer.localPosition = new Vector3(panelsContainer.localPosition.x, y, panelsContainer.localPosition.z);
    }

    public void ToggleDetailedView()
    {
        SetDetailedView(!detailedViewActive);
    }

    public void SetDetailedView(bool active)
    {
        detailedViewActive = active;

        UpdateScrollBarEnabled();
        background.SetActive(!detailedViewActive);
        backgroundDetails.SetActive(detailedViewActive);
        detailsOnButton.SetActive(!detailedViewActive);
        detailsOffButton.SetActive(detailedViewActive);
        scrollbar.gameObject.SetActive(detailedViewActive && scrollBarActive);
        SetPreviousPanelsActive(detailedViewActive);
    }

    void SetPreviousPanelsActive(bool active)
    {
        // do we even havre other panels than that of a single recorded gesture?
        if (panels.Count < 2)
            return;

        foreach (RecordingPanel panel in panels)
        {
            if (!panel.Equals(panels[panels.Count - 1]))
                panel.gameObject.SetActive(active);
        }
    }

    public void NormalizeAll()
    {
        foreach(RecordingPanel panel in panels)
        {
            panel.Normalize();
        }
    }

    public void DenormalizeAll()
    {
        foreach (RecordingPanel panel in panels)
        {
            panel.Denormalize();
        }
    }

    public void ScaleAll()
    {
        foreach (RecordingPanel panel in panels)
        {
            panel.ScaleTrajectory();
        }
    }
}
