﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RecordingPanel : MonoBehaviour
{
    [SerializeField]
    public RectTransform rectTransform;

    [SerializeField]
    LineDisplay gestureDispay;
    [SerializeField]
    TMP_Dropdown recordGestureDropdown;
    [SerializeField]
    AugmentationDisplay augmentationDisplay;
    [SerializeField]
    Toggle normalizeVectorsToggle;
    [SerializeField]
    Button scaleTrajectoryButton;
    [SerializeField]
    Image scaleTrajectoryButtonImage;
    [SerializeField]
    TMP_Text vectorCountText;

    public int Index => index;

    LabeledInputData gesture;
    int index;
    bool initialized = false;
    float lineWidth;
    float lineWidthAugmenation;

    public void Initialize(LabeledInputData gesture, int index, float lineWidth = .1f, float lineWidthAugmenation = .1f)
    {
        this.gesture = gesture;
        this.index = index;
        this.lineWidth = lineWidth;
        this.lineWidthAugmenation = lineWidthAugmenation;

        // gesture class dropdown init
        recordGestureDropdown.AddOptions(RecordPanel.Instance.gestureClassOptions);
        recordGestureDropdown.value = this.gesture.ClassIndex;

        UpdateNormalized();
        Display();
        initialized = true;
    }

    public void SelectClass(int index)
    {
        if (!initialized)
            return;

        gesture.SetClass(index);
    }

    void DisplayDenormalized()
    {
        LabeledInputData displayGesture = gesture.Clone();
        displayGesture.Denormalize();

        gestureDispay.Clear();
        gestureDispay.DisplayGesture(displayGesture, lineWidth);
        augmentationDisplay.Initialize(displayGesture, lineWidthAugmenation);
    }

    void DisplayOriginal()
    {
        gestureDispay.Clear();
        gestureDispay.DisplayGesture(gesture, lineWidth);
        augmentationDisplay.Initialize(gesture, lineWidthAugmenation);
    }

    void Display()
    {
        vectorCountText.text = gesture.inputData.Data[0].Count.ToString();

        // denormalize for correct drawing, but keep original in 'this.gesture'
        if (gesture.Normalized)
            DisplayDenormalized();
        else
            DisplayOriginal();
    }

    public void ToggleNormalize(bool normalize)
    {
        if (!initialized)
            return;

        if (normalize)
            Normalize();
        else
            Denormalize();
    }

    public void Normalize()
    {
        if (gesture.Normalized)
            return;

        gesture.Normalize();
        UpdateNormalized();
        DisplayDenormalized();
    }

    public void Denormalize()
    {
        if (!gesture.Normalized)
            return;

        gesture.Denormalize();
        UpdateNormalized();
        DisplayOriginal();
    }

    void UpdateNormalized()
    {
        if (normalizeVectorsToggle.isOn != gesture.Normalized)
            normalizeVectorsToggle.isOn = gesture.Normalized;
    }

    public void ScaleTrajectory()
    {
        gesture.inputData.ScaleToLength(RecordConfig.Instance.TrajectoryCountMax);
        Display();
    }


    void DisableScaleTrajectoryButton()
    {
        scaleTrajectoryButtonImage.color = new Color(
            scaleTrajectoryButtonImage.color.r,
            scaleTrajectoryButtonImage.color.g,
            scaleTrajectoryButtonImage.color.b,
            0.4f
        );

        scaleTrajectoryButton.enabled = false;
    }

    public void DecrementIndex() => index--;
    public void Drop() => RecordingsContainer.Instance.Drop(this.gesture);
    public bool BelongsToGesture(LabeledInputData gesture) => this.gesture == gesture;
    public void MoveUp(float amount) => transform.localPosition += new Vector3(0, amount, 0);
    public void MoveDown(float amount) => MoveUp(-amount);
}
