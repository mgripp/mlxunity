﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LineDrawer : MonoBehaviour
{
    static public LineDrawer mainDrawer;

    protected float initialZPosition = 0;
    protected Material material;

    protected virtual void Initialize()
    {
        mainDrawer = mainDrawer ?? this;
        initialZPosition = transform.localPosition.z;
        Clear();
    }

    public void SetColor(Color color) => material.color = color;

    protected virtual void SetAlpha(float alpha)
    {
        Color color = new Color(
            material.color.r,
            material.color.g,
            material.color.b,
            alpha
        );

        SetColor(color);
    }

    public abstract void DrawLine(Vector3 newPosition, float lineWidth = .1f);
    public abstract void DrawLines(Vector3[] newPositions, float lineWidth = .1f);
    public abstract void SetPositions(Vector3[] positions);
    public virtual void Clear() => transform.localPosition = new Vector3(0, 0, initialZPosition);
    public abstract void FadeOutIndipendently(float time);
    public void FadeOut(float time) => StartCoroutine(FadeOutRoutine(time));
    protected abstract IEnumerator FadeOutRoutine(float time);

    protected virtual LineDrawer DeepCopy()
    {
        LineDrawer clone = GameObject.Instantiate(gameObject).GetComponent<LineDrawer>();
        clone.transform.parent = transform.parent;
        clone.transform.position = transform.position;

        return clone;
    }
}
