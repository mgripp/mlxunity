﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineDisplay : MonoBehaviour
{
    static Vector3 nextPosition = new Vector3(100f, 0, 0);

    [SerializeField] protected LineDrawerArray lineDrawerArray;
    [SerializeField] protected RawImage image;
    [SerializeField] protected RenderTexture renderTexture;
    [SerializeField] protected Transform objects;
    [SerializeField] new protected Camera camera;

    private void Awake()
    {
        RenderTexture renderTextureClone = new RenderTexture(renderTexture);
        image.texture = renderTextureClone;
        camera.targetTexture = renderTextureClone;
        objects.position = nextPosition;
        nextPosition += new Vector3(100f, 0, 0);
    }

    public virtual void DisplayGesture(LabeledInputData gestureDisplay, float lineWidth = .1f)
    {
        Clear();
        int linesCount = gestureDisplay.inputData.Count;
        int positionsCount = gestureDisplay.inputData.Data[0].Count;
        Vector3[] currentPositions = new Vector3[linesCount];

        for (int i = 0; i < linesCount; i++)
            currentPositions[i] = Vector3.zero;

        for (int iLine = 0; iLine < linesCount; iLine++)
        {
            List<Vector3> input;
            if (gestureDisplay.inputData.DataType == DataTypes.DataType.Vector2)
            {
                input = new List<Vector3>();
                foreach (Vector2 v in gestureDisplay.inputData.Data[iLine].Data)
                    input.Add(v);
            }
            else
                input = (List<Vector3>)gestureDisplay.inputData.Data[iLine].Data;
            for (int iPosition = 0; iPosition < positionsCount; iPosition++)
            {
                currentPositions[iLine] += input[iPosition];
                lineDrawerArray.DrawLines(currentPositions, lineWidth);
            }
        }

        lineDrawerArray.FitToCamera(camera);
    }

    public void Clear() => lineDrawerArray.Clear();
}
