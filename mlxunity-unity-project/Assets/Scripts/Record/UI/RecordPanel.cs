﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RecordPanel : TrainStateObservingPanel
{
    static public RecordPanel Instance;

    // Gesture Class Selection
    public List<TMP_Dropdown.OptionData> gestureClassOptions { get; private set; }
    [SerializeField] TMP_Dropdown recordGestureDropdown;

    // Gesture Name Display
    [SerializeField] TMP_Text gestureNameText;

    // Start/Stop Button
    [SerializeField] Button recordButton;
    [SerializeField] TMP_Text recordButtonText;
    [SerializeField] Color recordingColor = Color.red;
    string recordButtonDefaultText = "Start Recording";
    Color notRecordingColor;

    // Normalization Settings
    [SerializeField] Toggle normalizeVectorsToggle;
    [SerializeField] Toggle scaleTrajectoriesToggle;

    // Maximum Length of Vector:
    [SerializeField] UnityEngine.UI.Slider minMovementDistanceSlider;
    [SerializeField] UnityEngine.UI.Slider maxTrajectoryVectorMagnitudeSlider;
    [SerializeField] UnityEngine.UI.Slider minTrajectoryCountSlider;
    [SerializeField] UnityEngine.UI.Slider maxTrajectoryCountSlider;

    [SerializeField] RecordingPanelArray gesturePanelArray;

    private void Start()
    {
        Instance = Instance ?? this;

        notRecordingColor = recordButton.image.color;
        recordButtonDefaultText = recordButtonText.text;

        gestureClassOptions = new List<TMP_Dropdown.OptionData>();

        foreach (Class c in ClassesConfig.Instance.Classes)
        {
            TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData();
            option.text = c.Name;
            gestureClassOptions.Add(option);
        }

        recordGestureDropdown.AddOptions(gestureClassOptions);
        InitializeGestureDropdown(RecordConfig.Instance.ClassIndex);
        minMovementDistanceSlider.value = RecordConfig.Instance.MinMovementDistance;
        maxTrajectoryVectorMagnitudeSlider.value = RecordConfig.Instance.MaxTrajectoryVectorMagnitude;
        minTrajectoryCountSlider.value = RecordConfig.Instance.TrajectoryCountMin;
        maxTrajectoryCountSlider.value = RecordConfig.Instance.TrajectoryCountMax;
        normalizeVectorsToggle.isOn = RecordConfig.Instance.NormalizeVectors;
        scaleTrajectoriesToggle.isOn = RecordConfig.Instance.ScaleTrajectories;

        UpdateGestureCountText();
        Initialize();
    }

    void InitializeGestureDropdown(int index)
    {
        if (RecordConfig.Instance.ClassIndex >= recordGestureDropdown.options.Count)
        {
            if (recordGestureDropdown.options.Count > 0)
                recordGestureDropdown.value = 0;
        }
        else
        {
            recordGestureDropdown.value = RecordConfig.Instance.ClassIndex;
        }
    }

    public override void OnTrainingStarted()
    {
        ShowGestureName("Training...\n(see Command Prompt)");

        if (Recorder.Instance.Recording)
            ToggleRecord();
    }

    public override void OnTrainingFinished() => ShowGestureName(RecordConfig.Instance.ClassName);
    public void Save() => RecordingsContainer.Instance.SaveGestures();
    public void Load() => RecordingsContainer.Instance.LoadGestures(RecordConfig.Instance.ClassIndex);
    public void UpdateGestureCountText() => gesturePanelArray.UpdateGestureCountText();
    public void SelectClass(int index) => RecordConfig.Instance.ClassIndex = index;

    public void ToggleRecord()
    {
        bool recording = Recorder.Instance.ToogleRecording();

        if (recording)
        {
            recordButton.image.color = recordingColor;
            recordButtonText.text = "Stop Recording";
        }
        else
        {
            recordButtonText.text = recordButtonDefaultText;
            recordButton.image.color = notRecordingColor;
        }
    }

    public void ToggleNormalizeVectors(bool normalize) => RecordConfig.Instance.NormalizeVectors = normalize;
    public void ToggleScaleTrajectories(bool scale) => RecordConfig.Instance.ScaleTrajectories = scale;
    public void ChangeMinMovementDistance(float value) => RecordConfig.Instance.MinMovementDistance = value;
    public void ChangeMaxTrajectoryVectorMagnitude(float value) => RecordConfig.Instance.MaxTrajectoryVectorMagnitude = value;

    public void ChangeTrajectoryCountMin(float value)
    {
        if (value >= RecordConfig.Instance.TrajectoryCountMax)
        {
            minTrajectoryCountSlider.value -= 1;
            return;
        }
        RecordConfig.Instance.TrajectoryCountMin = (int)value;
    }

    public void ChangeTrajectoryCountMax(float value)
    {
        if (value <= RecordConfig.Instance.TrajectoryCountMin)
        {
            maxTrajectoryCountSlider.value += 1;
            return;
        }
        RecordConfig.Instance.TrajectoryCountMax = (int)value;
    }

    public void ShowGestureName(string name) => gestureNameText.text = name;
    public void ClearGestureName() => ShowGestureName("");
    public void AddGesture(LabeledInputData gesture) => gesturePanelArray.Add(gesture);
    public void RemoveGesture(LabeledInputData gesture) => gesturePanelArray.Remove(gesture);
    public void RemoveAllGestures() => gesturePanelArray.RemoveAllGestures();
}
