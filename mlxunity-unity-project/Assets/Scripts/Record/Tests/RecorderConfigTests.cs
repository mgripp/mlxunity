﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class RecorderConfigTests
    {
        [Test]
        public void TestCopy()
        {
            RecordConfig clone = RecordConfig.Instance.Copy();

            Assert.AreEqual(clone, RecordConfig.Instance);
        }

        [Test]
        public void TestEqualsTrueAfterNewlyCreated()
        {
            RecordConfig config1 = new RecordConfig();
            RecordConfig config2 = new RecordConfig();

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsTrueAfterFloatParameterChanged()
        {
            RecordConfig config1 = new RecordConfig();
            RecordConfig config2 = new RecordConfig();

            config1.MinMovementDistance = 5.99999f;
            config2.MinMovementDistance = 5.99999f;

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsTrueWithFloatParameterApproximatelyTheSame()
        {
            RecordConfig config1 = new RecordConfig();
            RecordConfig config2 = new RecordConfig();

            config1.MinMovementDistance = 10f / 10f;
            config2.MinMovementDistance = 1f;

            Assert.AreEqual(config1, config2);
        }

        [Test]
        public void TestEqualsFalseAfterFloatParameterChanged()
        {
            RecordConfig config1 = new RecordConfig();
            RecordConfig config2 = new RecordConfig();

            config1.MinMovementDistance = 5.99999f;
            config2.MinMovementDistance = 5.99998f;

            Assert.AreNotEqual(config1, config2);
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
