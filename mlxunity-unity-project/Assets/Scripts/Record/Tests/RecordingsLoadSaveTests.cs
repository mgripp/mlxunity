﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using SimpleJSON;
using System.IO;

namespace Tests
{
    public class RecordingsLoadSaveTests
    {
        [Test]
        public void TestGetJSONPath()
        {
            Assert.AreEqual($"{GeneralConfig.Instance.ProjectPath}record_circle_ccw.json", ClassesConfig.Instance.Classes[0].RecordPath);
            Assert.AreEqual($"{GeneralConfig.Instance.ProjectPath}record_circle_cw.json", ClassesConfig.Instance.Classes[1].RecordPath);
            Assert.AreEqual($"{GeneralConfig.Instance.ProjectPath}record_erase.json", ClassesConfig.Instance.Classes[2].RecordPath);
            Assert.AreEqual($"{GeneralConfig.Instance.ProjectPath}record_s.json", ClassesConfig.Instance.Classes[3].RecordPath);
            Assert.AreEqual($"{GeneralConfig.Instance.ProjectPath}record_spiral_ccw.json", ClassesConfig.Instance.Classes[4].RecordPath);
            Assert.AreEqual($"{GeneralConfig.Instance.ProjectPath}record_spiral_cw.json", ClassesConfig.Instance.Classes[5].RecordPath);
            Assert.AreEqual($"{GeneralConfig.Instance.ProjectPath}record_swipe.json", ClassesConfig.Instance.Classes[6].RecordPath);
        }

        [Test]
        public void TestLoadFromJSON()
        {
            JSONNode jsonData;
            string jsonContent = string.Format("[{0},{1},{2},{3}]",
                TestExamples.gestureJSONExample1,
                TestExamples.gestureJSONExample2,
                TestExamples.gestureJSONExample3,
                TestExamples.gestureJSONExample1Augmentations
            );
            File.WriteAllText(TestUtils.TestRecordPath, jsonContent);

            List<LabeledInputData> gestures = RecordingsLoadSave.Instance.LoadFromJSON(out jsonData, TestUtils.TestRecordPath);

            Assert.AreEqual(4, gestures.Count);

            MouseGesture gesture1 = (MouseGesture)gestures[0];
            MouseGesture gesture2 = (MouseGesture)gestures[1];
            MouseGesture gesture3 = (MouseGesture)gestures[2];
            MouseGesture gesture4 = (MouseGesture)gestures[3];

            // gestureJSONExample1
            Assert.AreEqual(0, gesture1.ClassIndex);
            Assert.AreEqual("Circle CCW", gesture1.ClassName);
            Assert.IsFalse(gesture1.Normalized);
            Assert.IsTrue(gesture1.Scaled);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1, gesture1.Trajectory);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1.Size, gesture1.Trajectory.Size);
            Assert.IsNull(gesture1.Augmentations);

            // gestureJSONExample2
            Assert.AreEqual(6, gesture2.ClassIndex);
            Assert.AreEqual("Swipe", gesture2.ClassName);
            Assert.IsTrue(gesture2.Normalized);
            Assert.IsFalse(gesture2.Scaled);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample2, gesture2.Trajectory);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample2.Size, gesture2.Trajectory.Size);
            Assert.IsNull(gesture2.Augmentations);

            // gestureJSONExample3
            Assert.AreEqual(6, gesture3.ClassIndex);
            Assert.AreEqual("Swipe", gesture3.ClassName);
            Assert.IsFalse(gesture3.Normalized);
            Assert.IsTrue(gesture3.Scaled);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1, gesture3.Trajectory);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1.Size, gesture3.Trajectory.Size);
            Assert.IsNull(gesture3.Augmentations);

            // gestureJSONExample1Augmentations
            Assert.AreEqual(6, gesture4.ClassIndex);
            Assert.AreEqual("Swipe", gesture4.ClassName);
            Assert.IsFalse(gesture4.Normalized);
            Assert.IsTrue(gesture4.Scaled);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1, gesture4.Trajectory);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample1.Size, gesture4.Trajectory.Size);
            Assert.IsNotNull(gesture4.Augmentations);
            Assert.AreEqual(1, gesture4.Augmentations.Count);

            MouseGestureAugmentation augmentation = (MouseGestureAugmentation)gesture4.Augmentations[0];
            Debug.Log(augmentation);

            Assert.AreEqual(6, augmentation.ClassIndex);
            Assert.AreEqual("Swipe", augmentation.ClassName);

            // Scaled and Normalized always point to corresponding properties of the original gesture
            Assert.IsFalse(augmentation.Normalized);
            Assert.IsTrue(augmentation.Scaled);

            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample2, augmentation.Trajectory);
            Assert.AreEqual(TestExamples.vector2DataArrayContainerExample2.Size, augmentation.Trajectory.Size);
            Assert.IsNull(augmentation.Augmentations);
        }

        [Test]
        public void TestLoadFromJSONEmpty()
        {
            JSONNode jsonData;
            List<LabeledInputData> gestures = RecordingsLoadSave.Instance.LoadFromJSON(out jsonData, TestUtils.TestRecordPath);
            string jsonContent = File.ReadAllText(TestUtils.TestRecordPath);

            Assert.AreEqual(0, gestures.Count);
            Assert.AreEqual("[]", jsonContent);
        }

        [Test]
        public void TestRemoveJSON()
        {
            string jsonContent = string.Format("[{0}]", TestExamples.gestureJSONExample1);
            File.WriteAllText(TestUtils.TestRecordPath, jsonContent);

            string jsonContentRead = File.ReadAllText(TestUtils.TestRecordPath);
            Assert.AreEqual(jsonContent, jsonContentRead);

            RecordingsLoadSave.Instance.InitializeJSON(TestUtils.TestRecordPath);

            jsonContentRead = File.ReadAllText(TestUtils.TestRecordPath);
            Assert.AreEqual("[]", jsonContentRead);
        }

        [Test]
        public void TestOverwriteJSON()
        {
            string jsonContent = string.Format("[{0}]", TestExamples.gestureJSONExample1);
            File.WriteAllText(TestUtils.TestRecordPath, jsonContent);

            List<LabeledInputData> gestures = new List<LabeledInputData>();
            gestures.Add(new MouseGesture(new List<Vector2>()));
            RecordingsLoadSave.Instance.OverwriteJSON(gestures, TestUtils.TestRecordPath);
            string gestureJSON = string.Format("[{0}]", gestures[0].ToJSON());

            string jsonContentRead = File.ReadAllText(TestUtils.TestRecordPath);
            Assert.AreEqual(gestureJSON, jsonContentRead);
            Assert.AreNotEqual(gestureJSON, jsonContent);
        }

        [Test]
        public void TestWriteJSON()
        {
            List<LabeledInputData> gestures = new List<LabeledInputData>();
            gestures.Add(new MouseGesture(new List<Vector2>()));
            RecordingsLoadSave.Instance.WriteJSON(gestures, TestUtils.TestRecordPath);
            string gestureJSON = string.Format("[{0}]", gestures[0].ToJSON());

            string jsonContentRead = File.ReadAllText(TestUtils.TestRecordPath);
            Assert.AreEqual(gestureJSON, jsonContentRead);
        }

        [SetUp] public void BeforeTest() => TestUtils.Initialize<MouseGesture>();
        [TearDown] public void AfterTest() => TestUtils.CleanUp();
    }
}
