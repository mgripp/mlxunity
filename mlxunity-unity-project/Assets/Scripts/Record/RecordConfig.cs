﻿using UnityEngine;
using SimpleJSON;
using System;

public class RecordConfig : Configurable
{
    public static RecordConfig Instance;

    public Type InputDataType { get; protected set; }

    int classIndex = 0;
    // TODO: seperation from classIndex needed?
    public int ClassIndex
    {
        get { return classIndex; }
        set { classIndex = value; }
    }
    public string ClassName
    {
        get
        {
            if (classIndex >= 0 && classIndex < ClassesConfig.Instance.ClassCount)
            {
                return ClassesConfig.Instance.Classes[classIndex].Name;
            }
            else
            {
                Debug.LogWarning(string.Format("Class Index {0} is not valid, since there are currently {1} classes defined.",
                    classIndex, ClassesConfig.Instance.ClassCount));
                return "<Undefined>";
            }
        }
    }

    public override string ConfigKey => "record";

    public int TrajectoryCountMax
    {
        get => trajectoryCountMax;
        set => trajectoryCountMax = value;
    }
    public bool NormalizeVectors = true;
    public bool ScaleTrajectories = true;
    public float MinMovementDistance = 5f;
    public float MaxTrajectoryVectorMagnitude = 5f;
    public int TrajectoryCountMin = 10;
    protected int trajectoryCountMax = 30;

    public RecordConfig() : base() => Instance = Instance ?? this;

    public override void LoadFromJSON(JSONNode configJSON)
    {
        ClassIndex = configJSON["class_index"];
        MinMovementDistance = configJSON["min_movement_distance"];
        MaxTrajectoryVectorMagnitude = configJSON["max_trajectory_vector_magnitude"];
        TrajectoryCountMin = configJSON["trajectory_count_min"];
        TrajectoryCountMax = configJSON["trajectory_count_max"];
        NormalizeVectors = configJSON["normalize_vectors"];
        ScaleTrajectories = configJSON["scale_trajectories"];
    }

    public override JSONNode ToJSON()
    {
        JSONObject configJSON = new JSONObject();

        configJSON.Add("class_index", ClassIndex);
        configJSON.Add("min_movement_distance", MinMovementDistance);
        configJSON.Add("max_trajectory_vector_magnitude", MaxTrajectoryVectorMagnitude);
        configJSON.Add("trajectory_count_min", TrajectoryCountMin);
        configJSON.Add("trajectory_count_max", TrajectoryCountMax);
        configJSON.Add("normalize_vectors", NormalizeVectors);
        configJSON.Add("scale_trajectories", ScaleTrajectories);

        return configJSON;
    }

    public RecordConfig Copy()
    {
        RecordConfig clone = new RecordConfig();

        clone.ClassIndex = this.ClassIndex;
        clone.MinMovementDistance = this.MinMovementDistance;
        clone.MaxTrajectoryVectorMagnitude = this.MaxTrajectoryVectorMagnitude;
        clone.TrajectoryCountMin = this.TrajectoryCountMin;
        clone.trajectoryCountMax = this.trajectoryCountMax;
        clone.NormalizeVectors = this.NormalizeVectors;
        clone.ScaleTrajectories = this.ScaleTrajectories;

        return clone;
    }

    public override bool Equals(object obj)
    {
        RecordConfig other = (RecordConfig)obj;

        bool equal = (
            this.ClassIndex == other.ClassIndex &&
            this.ClassName == other.ClassName &&
            this.MinMovementDistance == other.MinMovementDistance &&
            this.MaxTrajectoryVectorMagnitude == other.MaxTrajectoryVectorMagnitude &&
            this.TrajectoryCountMin == other.TrajectoryCountMin &&
            this.TrajectoryCountMax == other.TrajectoryCountMax &&
            this.NormalizeVectors == other.NormalizeVectors &&
            this.ScaleTrajectories == other.ScaleTrajectories
        );

        return equal;
    }
}

public class RecordConfig<InputDataType> : RecordConfig where InputDataType : LabeledInputData
{
    public RecordConfig() : base() => this.InputDataType = typeof(InputDataType);
}
