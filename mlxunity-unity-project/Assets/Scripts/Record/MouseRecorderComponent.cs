﻿using UnityEngine;

public class MouseRecorderComponent : RecorderComponent
{
    RecordConfig<MouseGesture> config;
    Vector2 mousePositionBefore;

    void Start()
    {
        config = new RecordConfig<MouseGesture>();
        recorder = new MouseRecorder();
        recordingsContainer = new RecordingsContainer();
        recordingsLoadSave = new RecordingsLoadSave();
        mousePositionBefore = Input.mousePosition;
    }

    void Update()
    {
        // if MLB is pressed, draw/record mouse movement
        if (Input.GetMouseButtonDown(0) && !MainPanel.Instance.MouseIsOverUI())
            StartDrawingGesture();
        else if (Input.GetMouseButtonUp(0))
            StopDrawingGesture();

        // watch mouse movement and draw/record it when recording is active
        Vector2 mousePosition = Input.mousePosition;
        Vector2 movement = mousePosition - mousePositionBefore;

        if (movement.magnitude > RecordConfig.Instance.MinMovementDistance)
        {
            if (gestureActive)
            {
                ((MouseRecorder)recorder).AddMovement(movement);
                LineDrawer.mainDrawer.DrawLine(Camera.main.ScreenToWorldPoint(mousePosition));
            }

            mousePositionBefore = mousePosition;
        }
    }
}
