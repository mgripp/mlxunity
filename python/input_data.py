import json
import random
import numpy as np
import logging

# TODO: make configurable
TRAIN_TEST_RATIO = .9

def data_to_ndarray(
        data_list,
        x_ndarray,
        y_ndarray,
    ):
    i = 0
    for data_set in data_list:
        y_ndarray[i] = data_set[1]
        for j in range(len(data_set[0])):
            x_ndarray[i][j] = data_set[0][j]
        i += 1

def serialize_data(data_list, include_augmentations=True):
    """
    Takes in recorded data with its augmentations included as attribute, then serializes all original
    and augmented gestures into an array. Thus the array will have the length:
    (<number of gestures> + <number of all augmentations of all gestures> * <number of fields per float array>
    Every entry will be a tuple: (<input float arrays (flattened)>, <output class>)
    """
    data_serialized = []
    augmentation_data = []
    for labeled_data in data_list:
        input_floats = []
        for vector_array in labeled_data["input_data"]["data"]:
            for vector in vector_array:
                input_floats.extend(vector)
        data_serialized.append((input_floats, labeled_data["class"]))

        if include_augmentations:
            augmentation_data.extend(labeled_data["augmentations"])

    if include_augmentations:
        if len(augmentation_data) > 0:
            data_serialized.extend(serialize_data(augmentation_data, include_augmentations=False))

    return data_serialized

def get_data_from_json(json_path):
    try:
        with open(json_path, 'r') as f:
            data = json.load(f)
    except (FileNotFoundError, IOError):
        logging.warn(("file {} not found".format(json_path)))
        return []

    return serialize_data(data)

def split_into_test_and_training_data(data):
    border = int(len(data) * TRAIN_TEST_RATIO)
    train_data = data[:border]
    test_data = data[border:]

    return train_data, test_data

def get_train_and_test_data(classes_config):
    data = []

    for class_config in classes_config:
        json_data = get_data_from_json(class_config["input_json_path"])
        if len(json_data) > 0:
            data.extend(json_data)

    # get shape from first dataset in list
    shape = np.array(data[0][0]).shape
    # check that all other datasets have the same shape
    for data_set in data:
        current_shape = np.array(data_set[0]).shape
        if current_shape != shape:
            raise ValueError((
                "Shapes of datasets are not the same for all datasets." +
                "Shape of first one is {}, but there is at least one with shape {}").format(
                    shape, current_shape
                ))
    random.shuffle(data)
    print("Data Count:", len(data), "Shape:", shape)

    train_data, test_data = split_into_test_and_training_data(data)

    train_data_len = len(train_data)
    test_data_len = len(test_data)
    print("Train Data Count:", train_data_len)
    print("Test Data Count:", test_data_len)

    # shape of inputs and outputs
    shape_train_x = (train_data_len, *shape)
    shape_train_y = (train_data_len)
    shape_test_x = (test_data_len, *shape)
    shape_test_y = (test_data_len)

    train_input = np.ndarray(shape=shape_train_x, dtype="float32")
    train_labels = np.ndarray(shape=shape_train_y, dtype="float32")

    test_input = np.ndarray(shape=shape_test_x, dtype="float32")
    test_labels = np.ndarray(shape=shape_test_y, dtype="float32")

    data_to_ndarray(train_data, train_input, train_labels)
    data_to_ndarray(test_data, test_input, test_labels)

    return train_input, train_labels, test_input, test_labels
