import tensorflow as tf2
import inspect
import os


class NeuralNetwork:
    class Layer:
        layer_name_to_tf_layer = {
            "flatten" : {
                "tf2" : {
                    "create_function" : tf2.keras.layers.Flatten,
                    "takes_paramaters" : False,
                }
            },
            "dense" : {
                "tf2" : {
                    "create_function" : tf2.keras.layers.Dense,
                    "takes_paramaters" : True,
                }
            },
        }

        def __init__(self, layer_type, name, units=None, activation=None):
            self.layer_type = layer_type
            self.name = name
            self.units = units
            self.activation = activation
            self.tf2_variant = None

            tf2_layer_config = self.layer_name_to_tf_layer[self.layer_type]["tf2"]
            tf2_create_function = tf2_layer_config["create_function"]

            if tf2_layer_config["takes_paramaters"]:
                self.tf2 = tf2_create_function(self.units, activation=self.activation)
            else:
                self.tf2 = tf2_create_function()
    # class Layer END

    # optimizers
    optimizers = {}
    optimizers["adam"] = tf2.optimizers.Adam
    optimizers["adamax"] = tf2.optimizers.Adamax
    optimizers["nadam"] = tf2.optimizers.Nadam
    optimizers["adadelta"] = tf2.optimizers.Adadelta
    optimizers["adagrad"] = tf2.optimizers.Adagrad
    optimizers["rmsprop"] = tf2.optimizers.RMSprop
    optimizers["ftrl"] = tf2.optimizers.Ftrl
    optimizers["sgd"] = tf2.optimizers.SGD

    def add_layers_from_config(self, layers_config, output_length):
        for layer_config in layers_config:
            layer_type = layer_config["type"]
            name = "{}_{}".format(layer_type, len(self.layers)) # TODO: make configurable
            units = layer_config["units"]
            activation = layer_config["activation"]
            layer = self.Layer(
                layer_type=layer_type,
                name=name,
                units=units,
                activation=activation
            )
            self.layers.append(layer)

        output_layer = self.Layer(
                layer_type="dense",
                name="output",
                units=output_length,
                activation="softmax"
            )
        self.layers.append(output_layer)

    def get_input_length(self):
        return self.tf2_variant.inputs[0].shape[1]

    def get_output_length(self):
        return self.tf2_variant.outputs[0].shape[1]

    def get_tf2_layers(self):
        return [layer.tf2 for layer in self.layers]

    def __init__(self, network_config, output_length):
        self.layers = []
        self.add_layers_from_config(network_config["layers"], output_length)

        self.tf2_variant = tf2.keras.models.Sequential(self.get_tf2_layers())

    def train(self, train_config, train_data, train_labels, test_data, test_labels):
        optimizer_name = train_config["optimizer"]
        optimizer_function = self.optimizers[optimizer_name]
        optimizers_config = train_config["optimizers"][optimizer_name]

        parameter_keys = inspect.getfullargspec(optimizer_function).args
        parameter_values = []
        for key in parameter_keys:
            if key == "self" or key == "name":
                continue
            parameter_values.append(optimizers_config[key])

        optimizer = optimizer_function(*parameter_values)
        
        self.tf2_variant.compile(optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

        self.tf2_variant.fit(train_data, train_labels, batch_size=train_config["batch_size"], epochs=train_config["epochs"])

        test_loss, self.accuracy = self.tf2_variant.evaluate(test_data, test_labels)
        print ('Test loss: {}, Test accuracy: {}'.format(test_loss, self.accuracy * 100))

        self.tf2_variant.summary()

        # now that training is done, input and output length of NN are known
        self.input_length = self.get_input_length()
        self.output_length = self.get_output_length()

        # point layers to their respective tf2 layers
        index = 0
        for tf2_layer in self.tf2_variant.layers:
            self.layers[index].tf2_variant = tf2_layer
            index += 1

    def save(self, pb_path, barracuda_path):
        print("save tf2")

        tf2.saved_model.save(self.tf2_variant, pb_path)
        os.system("python -m tf2onnx.convert --saved-model {} --output {}".format(
            pb_path,
            barracuda_path,
        ))
        print("Model Accuracy: {:.2f}%".format(self.accuracy * 100))