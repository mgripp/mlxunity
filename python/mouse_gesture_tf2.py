# https://codelabs.developers.google.com/codelabs/tensorflow-lab4-cnns/#4

import tensorflow as tf
import json
import numpy as np
import random
from pathlib import Path
import ntpath
import posixpath

"""
    Classes:
    0 = None
    1 = Circle CW
    2 = Circle CCW
    3 = Sprial CW
    4 = Sprial CCW
    5 = S
    6 = Swipe
    7 = Erase
"""

TRAJECTORY_LEN = 60

DATA_PATH = str(Path(__file__).parent.absolute()).replace(ntpath.sep, posixpath.sep)
DATA_PATH += '/../mlxunity-unity-project/Assets/MouseGesture/circle_ccw.json'

def data_to_ndarray(data_list, x_ndarray, y_ndarray):
    index = 0

    for data_set in data_list:
        trajectory = data_set[0]

        assert(len(trajectory) == TRAJECTORY_LEN)

        y_ndarray[index] = data_set[1]
        for vectorIndex in range(TRAJECTORY_LEN):
            x_ndarray[index][vectorIndex][0] = trajectory[vectorIndex][0]
            x_ndarray[index][vectorIndex][1] = trajectory[vectorIndex][1]

        assert(data_set[1] > -1 and data_set[1] < 8)

        index += 1


def serialize_data(data_list):
    """
    Takes in recorded data with its augmentations included as attribute, then serializes all original
    and augmented gestures into an array. Thus the array will have the length:
    <number of gestures> + <number of all augmentations of all gestures>
    Every entry will be a tuple: (<input float array>, <output class>)
    """

    data_serialized = []
    for data_set in data_list:
        data_serialized.append((data_set["trajectory"], data_set["class"]))

        augmentations = data_set["augmentations"]

        if augmentations is not None:
            for augmentation in augmentations:
                data_serialized.append((augmentation["trajectory"], augmentation["class"]))

    return data_serialized

def main():
    # load recorded mouse gestures
    with open(DATA_PATH, 'r') as f:
        data = json.load(f)

    data_serialized = serialize_data(data)

    random.shuffle(data_serialized)

    border = int(len(data_serialized) * 2 / 3)
    train_data = data_serialized[:border]
    train_data_len = len(train_data)
    test_data = data_serialized[border:]
    test_data_len = len(test_data)


    train_gestures = np.ndarray(shape=(train_data_len, TRAJECTORY_LEN, 2), dtype="float32")
    train_labels = np.ndarray(shape=(train_data_len, 1), dtype="float32")
    test_gestures = np.ndarray(shape=(test_data_len, TRAJECTORY_LEN, 2), dtype="float32")
    test_labels = np.ndarray(shape=(test_data_len, 1), dtype="float32")

    data_to_ndarray(train_data, train_gestures, train_labels)
    data_to_ndarray(test_data, test_gestures, test_labels)

    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(256, activation='relu'),
        tf.keras.layers.Dense(256, activation='relu'),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(7, activation='softmax'),
    ])

    model.compile(optimizer = tf.optimizers.Adam(learning_rate=.0001), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    model.fit(train_gestures, train_labels, batch_size=32, epochs=15)

    test_loss, test_accuracy = model.evaluate(test_gestures, test_labels)
    print ('Test loss: {}, Test accuracy: {}'.format(test_loss, test_accuracy*100))

    model.summary()

    # save
    model.save('mouse_gesture.h5') 

if __name__ == "__main__":
    main()