import json
import click
import model
import input_data


@click.command()
@click.option(
    "--config_path", "-c",
    default="./config.json",
    help="Path to config file from unity application."
)
@click.option(
    "--pb_path", "-p",
    default="./model.pb",
    help="Desired path under which the protocol buffer model will be saved"
)
@click.option(
    "--barracuda_path", "-b",
    default="./model.onnx",
    help="Desired path under which the onnx model will be saved"
)
def main(config_path, pb_path, barracuda_path):
    # script_path = str(Path(__file__).parent.absolute()).replace(ntpath.sep, posixpath.sep)

    with open(config_path, 'r') as f:
        config = json.load(f)

    # load neuronal network
    network = model.NeuralNetwork(config["train"]["network"], len(config["classes"]))

    # load training and test data
    # TODO: trajectory_count_max is not a reliable source for the input array length (clamp/pad arrays?)
    # TODO: get input length from other value, might differ
    train_data, train_labels, test_data, test_labels = input_data.get_train_and_test_data(config["classes"])

    # start training
    # TODO: pass config["train"] instead of None
    network.train(config["train"], train_data, train_labels, test_data, test_labels)

    network.save(pb_path, barracuda_path)


if __name__ == '__main__':
    main()