# MLxUnity

![Recording gestures in hand.mlx](docs/images/handgesture.png)

The goal of "MlxUnity", short MLX, is to create an environment in which one can easily work on machine learning projects. At its current state, the framework allows to record, edit and augment data from mouse and hand gestures. The records can then be used to train a neural network that classifies input gestures.


## Installation

Works only on Windows.

### Unity

Tested only in Unity 2019.4.9f1 (newer versions should work as well).

You will need to install the following packages from the Unity Package Manager:
- Barracuda preview – 2.1.0
- Test Framework 1.1.16
- Textmesh Pro 2.0.1
- XR Plugin Management (optinal, needed for Leap Motion Controller)

In case the Leap Motion Controller is being used you need to install its software "Leap Motion Core":
> https://www2.leapmotion.com/downloads/unity-modules-4.6.0

### Python

Needed python version: Python 3.6.8

To install all requirements open a terminal window. Install virtualenv:
> python -m pip install virtualenv

Now navigate to the project directory and create a new virtual environment:
> python -m virtualenv ve

Then activate it:
> ve\Scripts\activate.bat (for CMD)
>
> ve\Scripts\activate.ps1 (for Power Shell)

Now install all python requirements into the environment:
> python -m pip install -r requirements.txt

You can now use MLX.
